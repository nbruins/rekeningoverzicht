-- CreateTable
CREATE TABLE "Bankaccount" (
    "id" SERIAL NOT NULL,
    "accountNumber" TEXT NOT NULL,
    "balance" DOUBLE PRECISION NOT NULL,
    "balanceDate" TIMESTAMP(3) NOT NULL,
    "description" TEXT,

    CONSTRAINT "Bankaccount_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Bankaccount_accountNumber_key" ON "Bankaccount"("accountNumber");

INSERT INTO "Bankaccount" ("accountNumber", "balance", "balanceDate", "description")
SELECT DISTINCT "accountNumber", 0.0, now(), 'migrated, balance not set' from "Transaction" t;
