DELETE FROM budget where year = 2023;

-- sqllite:
-- INSERT INTO budget (year, postId, month, budget)
-- 	SELECT '2025' as year,
-- 		postId,
-- 		strftime('%m', DATETIME(ROUND(interestDate / 1000), 'unixepoch')) - 1 as month,
-- 		ROUND(SUM(amount), 2) as budget
-- 	FROM "Transaction"
--     WHERE DATETIME(ROUND(interestDate / 1000), 'unixepoch') >= '2023-01-01'
-- 	GROUP BY strftime('%m', DATETIME(ROUND(interestDate / 1000), 'unixepoch')) - 1, postId;

-- postgres
INSERT INTO "Budget" (year, "postId", month, budget)
  SELECT '2025' as year,
  Tr."postId",
  EXTRACT(month FROM Tr."interestDate") -1 as month,
  SUM(Tr.amount) as budget
  FROM "Transaction" Tr
  WHERE Tr."interestDate" >= '2024-01-01'
  group by EXTRACT(month FROM Tr."interestDate") -1, Tr."postId";