import compression from 'compression'
import cors from 'cors'
import express, { Application, Response, Request, NextFunction, urlencoded } from 'express'
import logger from 'morgan'
import path from 'path'
import swaggerUi, { JsonObject } from 'swagger-ui-express'
import { addHelmet, corsOptions, methodBlocker } from './middleware'
import { RegisterRoutes } from './tsoa/routes'
import swaggerJson from './tsoa/swagger.json'
import { ValidateError } from "tsoa"

export const app: Application = express()

// Use body parser to read sent json payloads
app.use(
  urlencoded({
    extended: true,
  })
)

app.use(express.json({ limit: '50mb' }))

// Middleware
app.disable('x-powered-by')
app.disable('etag')

addHelmet(app)

app.use(methodBlocker())

app.use(cors(corsOptions))
app.options('*', cors<express.Request>())

app.use(compression())

app.use(logger('dev'))

const options = {
  swaggerOptions: {
    url: '/api/docs/swagger.json',
  }
}
app.get('/api/docs/swagger.json', (_req, res) => { res.json(swaggerJson as JsonObject) })
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
app.use('/api/docs', swaggerUi.serveFiles(undefined, options), swaggerUi.setup(undefined, options))


RegisterRoutes(app)

app.use(express.static(path.join(__dirname, '../client')))
app.get('/', (_req, res) => {
  res.sendFile(path.join(__dirname, '../client', 'index.html'))
})

app.use((err: unknown, req: Request, res: Response, next: NextFunction): void => {
  if (err instanceof ValidateError) {
    console.warn(`Caught Validation Error for ${req.path}:`, err.fields)
    res.status(422).json({
      message: "Validation Failed",
      details: err?.fields,
    })
  } else if (err instanceof Error) {
    res.status(500).json({
      message: "Internal Server Error",
    })
  } else {

    next()
  }
})

// All unknown GET routes, route to index.html else a 418
app.use((req, res) => {
  if (req.method === 'GET') {
    res.sendFile(path.join(__dirname, '../client', 'index.html'))
  } else {

    res.status(418).json({
      status: 'error',
      message: 'Er heeft zich een onbekende fout voorgedaan.',
    })
  }
})
