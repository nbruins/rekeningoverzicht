export interface Post {
  id: number
  name: string
  regExp: string | null
  categoryId: number
}

export interface Category {
  id: number
  name: string
  icon: string | null
  type: string
  gemmiddeldbedragNibud: number | null
  backgroundColor: string | null
  borderColor: string | null
  color: string | null
  post?: Post[]
}

export interface Transaction {
  id: number
  sequenceNumber: number
  accountNumber: string | null
  currency: string | null
  amount: number
  balance: number
  entryDate: Date
  interestDate: Date
  toFromAccountNumber: string | null
  toFromName: string | null
  description: string | null
  note: string | null
  error: boolean
  errorDescription: string | null
  postId: number | null
}

export interface Budget {
  id: number
  year: number
  month: number
  postId: number
  budget: number
}

interface PostWithCategory extends Post {
  category: Omit<Category, 'post'> | null
}
export interface TransactionWithPostAndCategory extends Transaction {
  post: PostWithCategory | null
}