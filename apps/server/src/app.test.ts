
import supertest from 'supertest'
import { app } from './app'

// eslint-disable-next-line @typescript-eslint/no-misused-promises
const requestWithSupertest = supertest(app)

describe('Block unknown routes and methods', () => {
  test('GET /unknown should 200 response and index.html', async () => {
    const res = await requestWithSupertest.get('/unknown')
    expect(res.status).toEqual(200)
    expect(res.type).toEqual('text/html')
  })

  test('PUT / should get a 418 response', async () => {
    const res = await requestWithSupertest.put('/')
    expect(res.status).toEqual(418)
    expect(res.type).toEqual(expect.stringContaining('json'))
    expect(res.body).toEqual({ status: 'error', message: 'Er heeft zich een onbekende fout voorgedaan.' })
  })

  test('DELETE / should get a 418 response', async () => {
    const res = await requestWithSupertest.delete('/')
    expect(res.status).toEqual(418)
    expect(res.type).toEqual(expect.stringContaining('json'))
    expect(res.body).toEqual({ status: 'error', message: 'Er heeft zich een onbekende fout voorgedaan.' })
  })


})

