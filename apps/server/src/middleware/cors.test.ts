import { corsOptions } from './cors'

const expectedCors = 'Origin, X-Requested-With, Content-Type, Accept, ' +
  'Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Origin, access-control-allow-origin'

test('Check if cors options are not changed', () => {

  expect(corsOptions.origin).toBeTruthy()

  const expectedMethods = ['DELETE', 'GET', 'OPTIONS', 'PATCH', 'POST', 'PUT']
  const methods = corsOptions.methods.split(',').map(item => item.trim()).sort()
  expect(methods).toMatchObject(expectedMethods)

  expect(corsOptions.allowedHeaders)
    .toBe(expectedCors)

  expect(corsOptions.credentials).toBeTruthy()


})
