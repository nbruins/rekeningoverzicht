export const corsOptions = {
  origin: true,
  methods: 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Credentials, ' +
    'Access-Control-Allow-Headers, Access-Control-Allow-Origin, access-control-allow-origin',
  credentials: true
}
