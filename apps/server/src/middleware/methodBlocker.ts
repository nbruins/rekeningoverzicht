import { Request, Response, NextFunction } from 'express'

const defaultMethodsToAllow = ['GET', 'POST', 'PUT', 'OPTIONS']

export const methodBlocker = () =>
  (req: Request, res: Response, next: NextFunction): void => {
    if (!defaultMethodsToAllow.includes(req.method)) {
      res.status(418).json({
        status: 'error',
        message: 'Er heeft zich een onbekende fout voorgedaan.',
      })
    } else {
      next()
    }
  }
