import helmet from 'helmet'
import { Application } from 'express'

export const addHelmet = (app: Application): void => {
  app.use(helmet())
  app.use(helmet.ieNoOpen()) // Sets "X-Download-Options: noopen".
  app.use(helmet.noSniff()) // Sets "X-Content-Type-Options: nosniff".
  app.use(helmet.xssFilter()) // Sets "X-XSS-Protection: 1; mode=block".
  app.use(helmet.referrerPolicy({ policy: 'no-referrer' })) // Sets "Referrer-Policy: no-referrer".
  app.use(helmet.dnsPrefetchControl()) // Sets "X-DNS-Prefetch-Control: off".
  app.use(helmet.frameguard({ action: 'deny' }))
}
