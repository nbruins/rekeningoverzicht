import { Body, Controller, Get, Path, Post, Put, Route, SuccessResponse, Tags } from 'tsoa'
import { Category, Post as PostType } from '../types'
import prisma from './client'

@Route('api/categories')
@Tags('Category')
export class CategoriesController extends Controller {

  @Get()
  public async getCategories(): Promise<Category[]> {

    return prisma.category.findMany({ include: { post: true } })
  }

  @SuccessResponse('204', 'No Content')
  @Put('{id}')
  public async updateCategory(
    @Path() id: number,
    @Body() requestBody: Omit<Category, 'id' | 'post'>
  ): Promise<void> {

    this.setStatus(204)

    await prisma.category.update({
      where: { id },
      data: { ...requestBody }
    })
    return Promise.resolve()
  }

  @Post()
  public async createCategory(
    @Body() requestBody: Omit<Category, 'id' | 'post'>
  ): Promise<Category> {
    this.setStatus(201)
    return prisma.category.create({ data: requestBody })
  }

  @SuccessResponse('204', 'No Content')
  @Put('/posts/{id}')
  public async updatePost(
    @Path() id: number,
    @Body() requestBody: Omit<PostType, 'id'>
  ): Promise<void> {

    this.setStatus(204)
    await prisma.post.update({
      where: { id },
      data: { ...requestBody }
    })
    return Promise.resolve()
  }

  @Post('/posts')
  public async createPost(
    @Body() requestBody: Omit<PostType, 'id'>
  ): Promise<PostType> {
    this.setStatus(201)
    return prisma.post.create({ data: requestBody })
  }

}
