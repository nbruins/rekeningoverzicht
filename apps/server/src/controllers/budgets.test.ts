import { prismaMock } from './singelton'
import { allPostsWithoutBudget, budgetDBMock, expectedResponseFromController } from './budget.mock'
import { BudgetsController } from './budgets'

describe('Test budget controller', () => {
  test('Get budget for year 2023', async () => {

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    prismaMock.budget.findMany.mockResolvedValue(budgetDBMock)

    const budget = new BudgetsController()
    const response = await budget.getBudgets(2023)

    expect(response).toEqual(expectedResponseFromController)
  })


  test('Get al posts without budget', async () => {

    prismaMock.$queryRaw.mockResolvedValue(allPostsWithoutBudget)

    const budget = new BudgetsController()
    const response = await budget.getAllPostsWithoutBudgetFor(2023)

    expect(response).toEqual(allPostsWithoutBudget)
  })

  test('Create new budget', async () => {

    prismaMock.$queryRaw.mockResolvedValue(allPostsWithoutBudget)

    const budget = new BudgetsController()
    const response = await budget.createBudget({ budget: 10, month: 1, postId: 72, year: 2023 })

    expect(response).toBeUndefined()

  })
  test('Update budget', async () => {

    prismaMock.budget.upsert.mockResolvedValue({ id: 1, budget: 10, month: 1, postId: 72, year: 2023 })

    const budget = new BudgetsController()
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    const response = await budget.updateBudget({ budgetPerMonth: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10], postId: 72, year: 2023 })

    expect(response).toBeUndefined()

  })
})