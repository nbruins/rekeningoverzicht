import { Body, Controller, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { Transaction, TransactionWithPostAndCategory } from '../types'
import prisma from './client'

interface TransactionUpdatePostAndNote {
  transactionIds: number[]
  note: string
  postId: number
}

const include = {
  post: {
    include: {
      category: true
    }
  }
}


@Route('api/transactions')
@Tags('Transaction')
export class TransactionsController extends Controller {

  @Get()
  public async getTransactions(
    @Query() interestDateGte?: Date,
    @Query() interestDateLte?: Date,
    @Query() postIds?: number[],
    @Query() accountNumbers?: string[]
  ): Promise<TransactionWithPostAndCategory[]> {

    const whereDateIs = []
    if (interestDateGte)
      whereDateIs.push({ interestDate: { gte: interestDateGte } })
    if (interestDateGte)
      whereDateIs.push({ interestDate: { lte: interestDateLte } })

    return prisma.transaction.findMany({
      include,
      where: {
        ...(whereDateIs.length > 0 ? { AND: whereDateIs } : {}),
        ...(postIds ? { postId: { in: postIds } } : {}),
        ...(accountNumbers ? { accountNumber: { in: accountNumbers } } : {})
      },
      orderBy: { interestDate: 'desc' }
    })
  }

  @Post()
  public async createManyTransactions(
    @Body() requestBody: Omit<Transaction, 'id'>[]
  ): Promise<Transaction[]> {

    if (!requestBody || requestBody.length <= 0)
      return []

    const sequenceNumbers = requestBody.map(transaction => (transaction.sequenceNumber))

    return await prisma.transaction.findMany({ where: { sequenceNumber: { in: sequenceNumbers } }, select: { sequenceNumber: true } })
      .then(async (existingSequenceNumberRaw: { sequenceNumber: number }[]) => {

        const existingSequence = existingSequenceNumberRaw.map(nr => nr.sequenceNumber)
        const insertList: Partial<Transaction>[] = []
        const doublesList: Partial<Transaction>[] = []
        requestBody.forEach((tr, index) => {
          if (tr && !existingSequence?.includes(tr.sequenceNumber)) {
            insertList.push({ ...tr, id: index + 1 })
          } else {
            doublesList.push({ ...tr, id: index + 1, error: true, errorDescription: 'double' })
          }
        })
        if (insertList.length > 0)
          await prisma.$transaction(
            insertList.map((tr => prisma.transaction.create({ data: { ...(tr as Transaction), id: undefined } })))
          )
        return [...insertList, ...doublesList] as Transaction[]
      })
  }


  @Put()
  public async updateNoteAndPostForMultipleTransactions(
    @Body() requestBody: TransactionUpdatePostAndNote): Promise<TransactionWithPostAndCategory[]> {

    const { transactionIds, postId, note } = requestBody
    const where = { id: { in: transactionIds } }

    return prisma.transaction.updateMany({ data: { postId, note }, where })
      .then(() => (
        prisma.transaction.findMany({ include, where, orderBy: { sequenceNumber: 'desc' } })
      ))
  }
}
