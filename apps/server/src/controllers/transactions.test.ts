
import { prismaMock } from './singelton'
import { TransactionsController } from './transactions'

const transaction1 = {
  id: 2,
  sequenceNumber: 1,
  accountNumber: 'NL66RABO0396868320',
  currency: 'EUR',
  amount: -66.3,
  balance: 1668,
  entryDate: new Date('2020-12-31T00:00:00.000Z'),
  interestDate: new Date('2021-01-01T00:00:00.000Z'),
  toFromAccountNumber: '1391942248',
  toFromName: null,
  description: 'RENTE LENING ',
  note: null,
  error: false,
  errorDescription: null,
  postId: 91
}
const transaction2 = {
  id: 3,
  sequenceNumber: 2,
  accountNumber: 'NL66RABO0396868320',
  currency: 'EUR',
  amount: -354.24,
  balance: 1313.76,
  entryDate: new Date('2020-12-31T00:00:00.000Z'),
  interestDate: new Date('2021-01-01T00:00:00.000Z'),
  toFromAccountNumber: '1391942442',
  toFromName: null,
  description: 'RENTE + AFLOSSING LENING ',
  note: null,
  error: false,
  errorDescription: null,
  postId: 106
}
const transaction3 = {
  id: 4,
  sequenceNumber: 3,
  accountNumber: 'NL66RABO0396868320',
  currency: 'EUR',
  amount: -444.67,
  balance: 869.09,
  entryDate: new Date('2020-12-31T00:00:00.000Z'),
  interestDate: new Date('2021-01-01T00:00:00.000Z'),
  toFromAccountNumber: '1391942221',
  toFromName: null,
  description: 'RENTE LENING ',
  note: null,
  error: false,
  errorDescription: null,
  postId: 91
}
const transactionsMock = [transaction1, transaction2, transaction3]

describe('Test transactions', () => {
  test('get all transactions', async () => {

    prismaMock.transaction.findMany.mockResolvedValue(transactionsMock)

    const transactions = new TransactionsController()
    const response = await transactions.getTransactions()
    expect(response).toEqual(transactionsMock)
  })

  test('get all transactions', async () => {

    prismaMock.transaction.findMany.mockResolvedValue(transactionsMock)

    const transactions = new TransactionsController()
    const response = await transactions.getTransactions(new Date(-10), new Date(), [1, 2])
    expect(response).toEqual(transactionsMock)
  })

  test('get all transactions with 0 found', async () => {

    prismaMock.transaction.findMany.mockResolvedValue([])

    const transactions = new TransactionsController()
    const response = await transactions.getTransactions()
    expect(response).toEqual([])
  })

  test('create transactions', async () => {

    prismaMock.transaction.findMany.mockResolvedValue([transaction1])

    const transactions = new TransactionsController()
    const response = await transactions.createManyTransactions([transaction1, transaction2, transaction3])
    expect(response).toHaveLength(3)
    expect(response.filter(transaction => transaction.error)).toHaveLength(1)
  })

  test('create transactions, but add empty list', async () => {

    prismaMock.transaction.findMany.mockResolvedValue([transaction1])

    const transactions = new TransactionsController()
    const response = await transactions.createManyTransactions([])
    expect(response).toHaveLength(0)
    expect(response.filter(transaction => transaction.error)).toHaveLength(0)
  })


  test('Update post and note of transactions', async () => {

    prismaMock.transaction.findMany.mockResolvedValue([transaction1])
    prismaMock.transaction.updateMany.mockResolvedValue({ count: 1 })

    const transactions = new TransactionsController()
    const response = await transactions.updateNoteAndPostForMultipleTransactions({ transactionIds: [1, 2], postId: 1, note: '' })
    expect(response).toHaveLength(1)

  })
})