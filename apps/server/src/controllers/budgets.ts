import { Body, Controller, Get, Path, Post, Put, Route, SuccessResponse, Tags } from 'tsoa'
import { Budget } from '../types'
import prisma from './client'

type Months = 'jan' | 'feb' | 'mar' | 'apr' | 'may' | 'jun' | 'jul' | 'aug' | 'sep' | 'oct' | 'nov' | 'dec'
const months: Months[] = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
const monthsObj: Record<Months, number | null> = {
  jan: null,
  feb: null,
  mar: null,
  apr: null,
  may: null,
  jun: null,
  jul: null,
  aug: null,
  sep: null,
  oct: null,
  nov: null,
  dec: null
}

interface BudgetResponse extends Record<Months, number | null> {
  id: string
  parentId?: string
  category: string
  postId?: number
  postName?: string
}

interface PostsWithOutBudgetResponse {
  category: string
  postId: number
  post: string
}
interface BudgetUpdateRequestBody {
  year: number
  postId: number
  budgetPerMonth: number[]
}

@Route('api/budgets')
@Tags('Budget')
export class BudgetsController extends Controller {

  @Get('{year}')
  public async getBudgets(@Path() year: number): Promise<BudgetResponse[]> {

    return prisma.budget.findMany({
      where: { year },
      orderBy: [
        { post: { category: { name: 'asc' } } },
        { post: { name: 'asc' } }
      ],
      select: {
        id: true,
        year: true,
        month: true,
        budget: true,
        post: {
          select: {
            id: true,
            name: true,
            category: {
              select: {
                name: true
              },
            }
          }
        }
      }
    }).then((result) => {
      const categoryMap = new Map<string, Map<string, BudgetResponse>>()
      result.forEach(item => {
        const categoryName = item.post.category.name
        const postName = item.post.name
        if (!categoryMap.has(categoryName) || !categoryMap.get(categoryName)?.has(postName)) {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          const postMap = categoryMap.has(categoryName) ? categoryMap.get(categoryName)! : new Map<string, BudgetResponse>()
          postMap.set(postName, {
            id: postName,
            category: '',
            postId: item.post.id,
            postName: item.post.name,
            parentId: categoryName,
            ...monthsObj
          })
          categoryMap.set(categoryName, postMap)
        }
        const updatePostMap = categoryMap.get(categoryName)?.get(postName)
        if (!updatePostMap) {
          console.error('Unable to find updatedPostMap')
          return
        }
        updatePostMap[months[item.month]] = item.budget
      })

      const budgetTable: BudgetResponse[] = []
      categoryMap.forEach((category, name) => {
        budgetTable.push({
          id: name,
          category: name,
          postName: '',
          ...monthsObj
        })
        category.forEach(postmap => {
          budgetTable.push(postmap)
        })
      })

      return budgetTable
    })
  }

  @Get('posts/{year}')
  public async getAllPostsWithoutBudgetFor(@Path() year: number): Promise<PostsWithOutBudgetResponse[]> {

    return await prisma.$queryRaw`
      SELECT c.name as category, p.id as postId, p.name as post FROM "Post" p 
      JOIN "Category" c 
      ON p."categoryId" = c.id
      WHERE p.id NOT IN (
        SELECT b."postId" FROM "Budget" b
          WHERE b.year = ${year}
          GROUP BY b."postId"
      )
      ORDER BY c.name, p.name
    `
  }

  @Post()
  public async createBudget(
    @Body() requestBody: Omit<Budget, 'id'>
  ): Promise<Budget> {
    this.setStatus(201)
    return prisma.budget.create({ data: requestBody })
  }

  @SuccessResponse('204', 'No Content')
  @Put()
  public async updateBudget(@Body() requestBody: BudgetUpdateRequestBody): Promise<void> {

    this.setStatus(204)
    const { year, postId, budgetPerMonth } = requestBody
    budgetPerMonth.map((budget, index) => {

      const where = { budgetIdentifier: { year, postId, month: index } }
      const update = { budget }
      const create = { year, postId, month: index, budget }
      prisma.budget.upsert({ where, update, create })
        .catch((err: unknown) => { console.error(err) })
    })
    return Promise.resolve()
  }
}
