import { prismaMock } from './singelton'
import { CategoriesController } from './categoriesAndPosts'

const category1 = {
  id: 2,
  name: 'Energie en lokale lasten',
  icon: 'power',
  type: 'UITGAVEN',
  gemmiddeldbedragNibud: 305,
  backgroundColor: '#6b5b95',
  borderColor: '#878f99',
  color: '#ffffff',
  Post: [
    {
      id: 73,
      name: 'Belasting gemeente',
      regExp: 'GBT ONTVANGSTEN|ENSCHEDE BELASTINGEN|Gem.Belastingen|Gemeentelijke Belastingen',
      categoryId: 2
    },
    {
      id: 75,
      name: 'Belasting waterschap',
      regExp: 'GBLT',
      categoryId: 2
    },
    {
      id: 82,
      name: 'Energie',
      regExp: 'BUDGETENERGIE|Essent',
      categoryId: 2
    },
    {
      id: 127,
      name: 'Water',
      regExp: 'Lococensus|VITENS|Waterschap Regge Dinkel',
      categoryId: 2
    }
  ]
}


describe('Test categories controller', () => {
  test('Get all categories', async () => {

    prismaMock.category.findMany.mockResolvedValue([category1])

    const cat = new CategoriesController()
    const response = await cat.getCategories()
    expect(response).toEqual([category1])
  })

  test('Create a category', async () => {

    void prismaMock.category.create({ data: category1 })

    const cat = new CategoriesController()
    const response = await cat.createCategory(category1)
    expect(response).toBeUndefined()
  })


})