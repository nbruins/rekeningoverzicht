import { Controller, Get, Route, Tags } from 'tsoa'
import prisma from './client'

@Route('api/health')
@Tags('Health')
export class HealtController extends Controller {

  @Get()
  public async getHealth(): Promise<{ status: string }> {

    return prisma.$queryRaw`SELECT 1 FROM "Category"`
      .then(() => ({ status: 'ok' }))
      .catch((e: unknown) => {
        console.log(e)
        return ({
          status: 'error connecting to database'
        })
      })
  }
}
