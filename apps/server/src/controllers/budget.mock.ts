export const budgetDBMock = [
  {
    id: 2548,
    year: 2023,
    month: 0,
    budget: 0,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2546,
    year: 2023,
    month: 1,
    budget: 0,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2547,
    year: 2023,
    month: 2,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2115,
    year: 2023,
    month: 3,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2156,
    year: 2023,
    month: 4,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2194,
    year: 2023,
    month: 5,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2236,
    year: 2023,
    month: 6,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2275,
    year: 2023,
    month: 7,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2308,
    year: 2023,
    month: 8,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2346,
    year: 2023,
    month: 9,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2384,
    year: 2023,
    month: 10,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2555,
    year: 2023,
    month: 11,
    budget: -110,
    post: {
      id: 73,
      name: 'Belasting gemeente',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 1999,
    year: 2023,
    month: 0,
    budget: -41,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2040,
    year: 2023,
    month: 1,
    budget: -41,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2076,
    year: 2023,
    month: 2,
    budget: -41,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2117,
    year: 2023,
    month: 3,
    budget: -41,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2560,
    year: 2023,
    month: 4,
    budget: 0,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2563,
    year: 2023,
    month: 5,
    budget: 0,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2237,
    year: 2023,
    month: 6,
    budget: -48,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2276,
    year: 2023,
    month: 7,
    budget: -48,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2309,
    year: 2023,
    month: 8,
    budget: -48,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2347,
    year: 2023,
    month: 9,
    budget: -48,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2385,
    year: 2023,
    month: 10,
    budget: -48,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2567,
    year: 2023,
    month: 11,
    budget: -48,
    post: {
      id: 75,
      name: 'Belasting waterschap',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2003,
    year: 2023,
    month: 0,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2044,
    year: 2023,
    month: 1,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2081,
    year: 2023,
    month: 2,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2122,
    year: 2023,
    month: 3,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2161,
    year: 2023,
    month: 4,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2199,
    year: 2023,
    month: 5,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2241,
    year: 2023,
    month: 6,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2280,
    year: 2023,
    month: 7,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2577,
    year: 2023,
    month: 8,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2352,
    year: 2023,
    month: 9,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2390,
    year: 2023,
    month: 10,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2424,
    year: 2023,
    month: 11,
    budget: -180,
    post: {
      id: 82,
      name: 'Energie',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2032,
    year: 2023,
    month: 0,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2067,
    year: 2023,
    month: 1,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2109,
    year: 2023,
    month: 2,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2150,
    year: 2023,
    month: 3,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2187,
    year: 2023,
    month: 4,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2227,
    year: 2023,
    month: 5,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2588,
    year: 2023,
    month: 6,
    budget: 0,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2587,
    year: 2023,
    month: 7,
    budget: 0,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2341,
    year: 2023,
    month: 8,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2379,
    year: 2023,
    month: 9,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2416,
    year: 2023,
    month: 10,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  },
  {
    id: 2447,
    year: 2023,
    month: 11,
    budget: -21.55,
    post: {
      id: 127,
      name: 'Water',
      category: {
        name: 'Energie en lokale lasten'
      }
    }
  }
]

export const expectedResponseFromController = [
  {
    id: 'Energie en lokale lasten',
    category: 'Energie en lokale lasten',
    postName: '',
    jan: null,
    feb: null,
    mar: null,
    apr: null,
    may: null,
    jun: null,
    jul: null,
    aug: null,
    sep: null,
    oct: null,
    nov: null,
    dec: null
  },
  {
    id: 'Belasting gemeente',
    category: '',
    postId: 73,
    postName: 'Belasting gemeente',
    parentId: 'Energie en lokale lasten',
    jan: 0,
    feb: 0,
    mar: -110,
    apr: -110,
    may: -110,
    jun: -110,
    jul: -110,
    aug: -110,
    sep: -110,
    oct: -110,
    nov: -110,
    dec: -110
  },
  {
    id: 'Belasting waterschap',
    category: '',
    postId: 75,
    postName: 'Belasting waterschap',
    parentId: 'Energie en lokale lasten',
    jan: -41,
    feb: -41,
    mar: -41,
    apr: -41,
    may: 0,
    jun: 0,
    jul: -48,
    aug: -48,
    sep: -48,
    oct: -48,
    nov: -48,
    dec: -48
  },
  {
    id: 'Energie',
    category: '',
    postId: 82,
    postName: 'Energie',
    parentId: 'Energie en lokale lasten',
    jan: -180,
    feb: -180,
    mar: -180,
    apr: -180,
    may: -180,
    jun: -180,
    jul: -180,
    aug: -180,
    sep: -180,
    oct: -180,
    nov: -180,
    dec: -180
  },
  {
    id: 'Water',
    category: '',
    postId: 127,
    postName: 'Water',
    parentId: 'Energie en lokale lasten',
    jan: -21.55,
    feb: -21.55,
    mar: -21.55,
    apr: -21.55,
    may: -21.55,
    jun: -21.55,
    jul: 0,
    aug: 0,
    sep: -21.55,
    oct: -21.55,
    nov: -21.55,
    dec: -21.55
  }
]

export const allPostsWithoutBudget = [
  {
    category: 'Abonnementen en telecom',
    postId: 80,
    post: 'Cursus/Opleiding'
  },
  {
    category: 'Niet ingedeeld',
    postId: 106,
    post: 'Onbekend'
  },
  {
    category: 'Niet ingedeeld',
    postId: 111,
    post: 'Overige'
  },
  {
    category: 'Overige vaste lasten',
    postId: 90,
    post: 'Hypotheek Keuzeplus'
  },
  {
    category: 'Overige vaste lasten',
    postId: 131,
    post: 'Zwemles'
  },
  {
    category: 'Vervoer',
    postId: 71,
    post: 'Bekeuring'
  },
  {
    category: 'Vrijetijdsuitgaven',
    postId: 77,
    post: 'Boeken/Tijdschriften/DVD'
  }
]