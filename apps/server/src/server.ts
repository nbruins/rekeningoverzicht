import http from 'http'
import { app } from './app'

const port = 3002
app.set('port', port)

// eslint-disable-next-line @typescript-eslint/no-misused-promises
const server = http.createServer(app)

// Start server
server.listen(port, '0.0.0.0', () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log('\n')
    console.log('✓🚀  server is running')
    console.log(`     address:  http://localhost:${String(port)}`)
    console.log('\nPress CTRL-C to stop\n')
  }
})

