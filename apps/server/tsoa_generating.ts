import {
  generateRoutes,
  generateSpec,
  ExtendedRoutesConfig,
  ExtendedSpecConfig,
} from "tsoa";

(async () => {
  const specOptions: ExtendedSpecConfig = {
    basePath: "/api",
    entryFile: "./api/server.ts",
    specVersion: 3,
    outputDirectory: "./api/dist",
    controllerPathGlobs: ["./**/controllers/*.ts"],
    noImplicitAdditionalProperties: 'throw-on-extras',
  }

  const routeOptions: ExtendedRoutesConfig = {
    basePath: "/api",
    entryFile: "./api/server.ts",
    routesDir: "./api",
    noImplicitAdditionalProperties: 'throw-on-extras',
    bodyCoercion: false
  }

  await generateSpec(specOptions)

  await generateRoutes(routeOptions)
})()