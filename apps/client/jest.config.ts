import { defaults as tsjPreset } from "ts-jest/presets"
import type { JestConfigWithTsJest } from 'ts-jest'

/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */

const jestConfig: JestConfigWithTsJest = {
  ...tsjPreset,
  clearMocks: true,

  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{ts,tsx}',
    '!**/node_modules/**',
    '!**/types/**',
    '!**/*.d.ts',
    '!**/*.config.ts',
    '!**/index.ts',
    '!**/openapi-fetch/**',
    '!src/App.tsx',
    '!src/index.tsx',
    '!src/i18n.ts',
    '!src/reportWebVitals.ts'
  ],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '<rootDir>/apps/client/src/types/*.ts'
  ],
  coverageProvider: 'v8',
  coverageReporters: [
    'json',
    'text',
    'lcov',
    'clover',
    'cobertura'
  ],
  coverageThreshold: undefined,

  preset: 'ts-jest',

  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
  testEnvironment: 'jsdom',

  // transform: {
  //   '^.+\\.(j|t)sx?$': 'ts-jest',
  // },
  // transformIgnorePatterns: ['node_modules/(?!plotly.js)'],
  transformIgnorePatterns: ['node_modules/(?!\@azure\/msal-react)'],
  transform: {
    '^.+\\.[tj]s?$': ['ts-jest', { tsconfig: { allowJs: true } }]
  },

  verbose: true,
  workerIdleMemoryLimit: '1600MB'
}

export default jestConfig