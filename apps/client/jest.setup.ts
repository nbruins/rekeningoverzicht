import '@testing-library/jest-dom/jest-globals'
import { TextEncoder } from 'util'

global.TextEncoder = TextEncoder

jest.mock('plotly.js', () => ({
  newPlot: jest.fn(() => Promise.resolve())
}))
