import { useState, useEffect } from 'react'

interface ReturnDimensions {
  width: number
  height: number
}
const getWindowDimensions = (): ReturnDimensions => {
  const { innerWidth: width, innerHeight: height } = window
  return {
    width,
    height
  }
}

export const  useWindowDimensions = (): ReturnDimensions => {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions())

  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(getWindowDimensions())
    }

    window.addEventListener('resize', handleResize)
    return () => { window.removeEventListener('resize', handleResize); }
  }, [])

  return windowDimensions
}
