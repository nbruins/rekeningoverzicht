import { useEffect } from 'react'
import { useStoreActions, useStoreState } from '../state/typedHooks'
import { dateService } from '../utils'

// reset date range to year or month
export const useSetDateRange = (to: 'year' | 'month' = 'year'): void => {

  const filters = useStoreState((state) => state.transactions.filters)
  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)

  const date = filters.dateBeforeEqual


  useEffect(() => {
    setFilters({
      ...filters,
      dateAfterEqual: to === 'month' ? dateService.firstDateOfMonth(date) : dateService.firstDateOfYear(date),
      dateBeforeEqual: to === 'month' ? dateService.lastDateOfMonth(date) : dateService.lastDateOfYear(date)
    })
  }, [])
}
