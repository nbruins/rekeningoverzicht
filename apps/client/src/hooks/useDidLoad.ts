import {useEffect, useRef} from 'react'

// Return true when is loading is done
export const useDidLoad = (isLoading = false): boolean => {

  const loadingStarted = useRef(false)
  const didLoad = useRef(false)
  useEffect(() => {
    if (isLoading) {
      loadingStarted.current = true
      didLoad.current = false
      return
    }
    if (!isLoading && loadingStarted.current) {
      loadingStarted.current = false
      didLoad.current = true
    }
  }, [isLoading])

  return didLoad.current
}
