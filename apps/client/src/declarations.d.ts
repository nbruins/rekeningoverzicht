// declare module '*.scss';

declare module '*.scss' {
  const styles: Record<string, string>
  export default styles
}
