import { action, Action, thunk, Thunk } from 'easy-peasy'
import {
  Category,
  PickCategoryExcludeKeyofCategoryIdOrPost,
  PickCategoryExcludeKeyofCategoryIdOrPostFromJSON,
  PickPostTypeExcludeKeyofPostTypeId,
  PickPostTypeExcludeKeyofPostTypeIdFromJSON,
  Post
} from '../openapi-fetch'
import { Injections } from './configureStore'

export interface CategoryAndPostModel {
  loading: boolean
  setLoading: Action<CategoryAndPostModel, boolean>
  fetchFinished: boolean
  setFetchFinished: Action<CategoryAndPostModel, boolean>
  categoriesWithPosts: Category[]
  getCategoriesWithPosts: Thunk<CategoryAndPostModel, undefined, Injections>
  setCategoriesWithPosts: Action<CategoryAndPostModel, Category[]>
  updateCategory: Thunk<CategoryAndPostModel, Category, Injections>
  addCategory: Thunk<CategoryAndPostModel, PickCategoryExcludeKeyofCategoryIdOrPost, Injections>
  updatePost: Thunk<CategoryAndPostModel, Post, Injections>
  addPost: Thunk<CategoryAndPostModel, PickPostTypeExcludeKeyofPostTypeId, Injections>
}

export const categoryAndPostModel: CategoryAndPostModel = {
  loading: false,
  setLoading: action((state, payload) => { state.loading = payload }),
  fetchFinished: false,
  setFetchFinished: action((state, payload) => { state.fetchFinished = payload }),
  categoriesWithPosts: [],
  getCategoriesWithPosts: thunk((actions, payload, helpers) => {
    if (!helpers.getState().fetchFinished) {
      actions.setLoading(true)
      helpers.injections.categoryApi.getCategories()
        .then(response => { actions.setCategoriesWithPosts(response) })
        .finally(() => {
          actions.setLoading(false)
          actions.setFetchFinished(true)
        })
    }
  }),
  setCategoriesWithPosts: action((state, payload) => { state.categoriesWithPosts = payload }),
  updateCategory: thunk((actions, category, helpers) => {

    actions.setLoading(true)
    const body = PickCategoryExcludeKeyofCategoryIdOrPostFromJSON(category)

    helpers.injections.categoryApi.updateCategory({ id: category.id, body })
      .then(() => {
        const categories = helpers.getState().categoriesWithPosts
        const index = categories.findIndex(item => item.id === category.id)
        categories.splice(index, 1, category)
        actions.setCategoriesWithPosts(categories)
      })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
  addCategory: thunk((actions, category, helpers) => {

    actions.setLoading(true)
    const body = PickCategoryExcludeKeyofCategoryIdOrPostFromJSON(category)

    helpers.injections.categoryApi.createCategory({ body })
      .then((response) => {
        const categories = helpers.getState().categoriesWithPosts
        categories.push(response)
        actions.setCategoriesWithPosts(categories)
      })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
  updatePost: thunk((actions, post, helpers) => {

    actions.setLoading(true)
    const body = PickPostTypeExcludeKeyofPostTypeIdFromJSON(post)

    helpers.injections.categoryApi.updatePost({ id: post.id, body })
      .then(() => {
        const categories = helpers.getState().categoriesWithPosts
        categories.forEach(cat => {
          if (cat.post) {
            const postIndex = cat.post.findIndex(postInCat => postInCat.id === post.id) ?? -1
            if (postIndex > -1) {
              cat.post.splice(postIndex, 1, post)
            }
          }
        })
        actions.setCategoriesWithPosts(categories)
      })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
  addPost: thunk((actions, post, helpers) => {

    actions.setLoading(true)
    const body = PickPostTypeExcludeKeyofPostTypeIdFromJSON(post)

    helpers.injections.categoryApi.createPost({ body })
      .then((response) => {
        const categories = helpers.getState().categoriesWithPosts
        categories.forEach(category => {
          if (category.id === body.categoryId) {
            category?.post?.push(response as Post)
            return
          }
        })
        actions.setCategoriesWithPosts(categories)
      })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
}
