import { createStore } from 'easy-peasy'
import { enableMapSet } from 'immer'
import { BudgetApi, CategoryApi, Configuration, TransactionApi } from '../openapi-fetch'
import { StoreMock } from '../types'
import { BudgetModel, budgetModel } from './budgetModel'
import { CategoryAndPostModel, categoryAndPostModel } from './categoryAndPostModel'
import { configModel, ConfigModel } from './configModel'
import { transactionModel, TransactionModel } from './transactionModel'

enableMapSet()

export interface AppState {
  config: ConfigModel
  transactions: TransactionModel
  categories: CategoryAndPostModel
  budget: BudgetModel
}

export interface Injections {
  budgetApi: BudgetApi
  categoryApi: CategoryApi
  transactionApi: TransactionApi
}

const port = (location.port && location.port !== '80') ? ':3002' : ''
const configuration = new Configuration({ basePath: `${location.protocol}//${location.hostname}${port}` })
export const transactionApi = new TransactionApi(configuration)

export const configureStore = ({ mock }: { mock?: StoreMock } = {}) => {

  const isTest = process.env.NODE_ENV === 'test'

  const injections: Injections = {
    budgetApi: new BudgetApi(configuration),
    categoryApi: new CategoryApi(configuration),
    transactionApi
  }

  const testInjections = {
    budgetApi: {
      getBudgets: () => Promise.resolve(mock?.budgets?.get ?? []),
      getAllPostsWithoutBudgetFor: () => Promise.resolve(mock?.budgets?.getPosts ?? []),
    },
    transactionApi: {
      getTransactions: () => Promise.resolve(mock?.transactions?.get ?? []),
      updateNoteAndPostForMultipleTransactions: () => Promise.resolve()
    }
  }

  return createStore(
    {
      config: configModel,
      transactions: transactionModel,
      categories: categoryAndPostModel,
      budget: budgetModel
    },
    {
      name: 'Rekeningoverzicht',
      devTools: process.env.NODE_ENV !== 'production',
      injections: isTest ? testInjections : injections
    }
  )
}
