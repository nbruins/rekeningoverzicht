import { dateService, setTestStore } from '../utils'
import fetchMock from 'fetch-mock-jest'

describe('transactionModel test', () => {

  fetchMock.get('path:/api/transactions', [])

  const dateAfter = dateService.localDateAsUTC(new Date(2021, 2, 1))
  const dateBefore = dateService.localDateAsUTC(new Date(2021, 2, 28))
  const accountNumbers: string[] = []
  const postIds = [1, 2, 3]

  it('should set the dates and postIds', () => {

    const store = setTestStore()

    store.getActions().transactions.setFilters({
      dateAfterEqual: dateAfter,
      dateBeforeEqual: dateBefore,
      accountNumbers,
      postIds
    })

    expect(store.getState().transactions.filters.dateAfterEqual).toBe(dateAfter)
    expect(store.getState().transactions.filters.dateBeforeEqual).toBe(dateBefore)
    expect(store.getState().transactions.filters.postIds).toBe(postIds)

    fetchMock.mockReset()
  })

})
