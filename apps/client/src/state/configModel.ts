import {action, Action, computed, Computed} from 'easy-peasy'
import {ThemeOptions} from '@mui/material/styles'

export interface ConfigModel {
  themeOptions: Computed<ConfigModel, ThemeOptions>
  setThemeOptions: Action<ConfigModel, ThemeOptions>
}

const defaultTheme: ThemeOptions = {
  palette: {
    mode: 'light',
    primary: {
      light: '#f05545',
      main: '#b71c1c',
      dark: '#7f0000',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
  },
}

export const configModel: ConfigModel = {
  themeOptions: computed(() => {
    const storedTheme = localStorage.getItem('theme')
    return storedTheme? JSON.parse(storedTheme) as ThemeOptions : defaultTheme
  }),
  setThemeOptions: action((state, newThemeOptions) => {
    state.themeOptions = newThemeOptions
    localStorage.setItem('theme', JSON.stringify(newThemeOptions))
  }),
}
