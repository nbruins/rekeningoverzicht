import { action, Action, thunk, Thunk, thunkOn, ThunkOn } from 'easy-peasy'
import {
  FetchError, GetTransactionsRequest, RequiredError,
  ResponseError, TransactionUpdatePostAndNote, TransactionWithPostAndCategory
} from '../openapi-fetch'
import { TransactionFilters } from '../types'
import { dateService } from '../utils/dateService'
import { AppState, Injections } from './configureStore'

export interface TransactionModel {
  transactions: TransactionWithPostAndCategory[]
  loading: boolean
  error: string | null
  filters: TransactionFilters
  selectedTransactionIds: Set<number>
  lastTransactionRequest: GetTransactionsRequest
  setTransactions: Action<TransactionModel, TransactionWithPostAndCategory[]>
  setLoading: Action<TransactionModel, boolean>
  setError: Action<TransactionModel, string | null>
  setFilters: Action<TransactionModel, TransactionFilters>
  setSelectedTransactionIds: Action<TransactionModel, number>
  resetSelectedTransactionIds: Action<TransactionModel, boolean>
  setLastTransactionRequest: Action<TransactionModel, GetTransactionsRequest>
  getTransactions: ThunkOn<TransactionModel, Injections, AppState>
  updateNoteAndPostForMultipleTransactions: Thunk<TransactionModel, TransactionUpdatePostAndNote, Injections>
}

const defaultFilters: TransactionFilters = {
  dateAfterEqual: dateService.firstDateOfYear(),
  dateBeforeEqual: dateService.lastDateOfMonth(),
  postIds: [],
  accountNumbers: ['NL66RABO0129637718']
}

export const transactionModel: TransactionModel = {
  transactions: [],
  loading: false,
  error: null,
  filters: defaultFilters,
  selectedTransactionIds: new Set(),
  lastTransactionRequest: {},
  setTransactions: action((state, payload) => { state.transactions = payload }),
  setFilters: action((state, filters) => { state.filters = filters }),
  setLoading: action((state, payload) => { state.loading = payload }),
  setError: action((state, payload) => { state.error = payload }),
  setSelectedTransactionIds: action((state, payload) => {
    if (state.selectedTransactionIds.has(payload)) {
      state.selectedTransactionIds.delete(payload)
    } else {
      state.selectedTransactionIds.add(payload)
    }
  }),
  resetSelectedTransactionIds: action((state, reset) => {
    if (reset)
      state.selectedTransactionIds = new Set()
  }),
  setLastTransactionRequest: action((state, payload) => { state.lastTransactionRequest = payload }),
  getTransactions: thunkOn(
    (actions, storeActions) => storeActions.transactions.setFilters,
    (actions, target, helpers) => {
      const lastRequest = helpers.getState().lastTransactionRequest
      const postIds = target.payload.postIds.length > 0 ? target.payload.postIds : undefined
      if (
        lastRequest.interestDateGte !== target.payload.dateAfterEqual
        || lastRequest.interestDateLte !== target.payload.dateBeforeEqual
        || lastRequest.postIds !== postIds
        || lastRequest.accountNumbers !== target.payload.accountNumbers
      ) {
        actions.setLoading(true)
        const requestParameters = {
          interestDateGte: target.payload.dateAfterEqual,
          interestDateLte: target.payload.dateBeforeEqual,
          postIds,
          accountNumbers: target.payload.accountNumbers
        }
        helpers.injections.transactionApi.getTransactions(requestParameters)
          .then(response => {
            actions.setLastTransactionRequest(requestParameters)
            actions.setTransactions(response)
            actions.setError(null)
          })
          // eslint-disable-next-line @typescript-eslint/use-unknown-in-catch-callback-variable
          .catch((error: ResponseError | FetchError | RequiredError) => {
            actions.setError(error.message)
            actions.setTransactions([])
          })
          .finally(() => {
            actions.setLoading(false)
          })
      }
    }),
  updateNoteAndPostForMultipleTransactions: thunk((actions, payload, helpers) => {
    void helpers.injections.transactionApi.updateNoteAndPostForMultipleTransactions({ transactionUpdatePostAndNote: payload })
      .then((updatedTransactions) => {
        const updatedTransactionsMap = new Map(updatedTransactions.map((transaction => [transaction.id, transaction])))
        const transactionsFromState = helpers.getState().transactions.map(transaction => {
          if (updatedTransactionsMap.has(transaction.id))
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            return updatedTransactionsMap.get(transaction.id)!
          return transaction
        })

        actions.setTransactions(transactionsFromState)
      })
  })
}
