import { createTypedHooks } from 'easy-peasy'
import { AppState } from './configureStore'

const typedHooks = createTypedHooks<AppState>()

export const useStoreActions = typedHooks.useStoreActions
export const useStoreDispatch = typedHooks.useStoreDispatch
export const useStoreState = typedHooks.useStoreState