import { action, Action, thunk, Thunk } from 'easy-peasy'
import {
  BudgetResponse,
  BudgetUpdateRequestBody,
  PickBudgetExcludeKeyofBudgetId,
  PostsWithOutBudgetResponse
} from '../openapi-fetch'
import { Injections } from './configureStore'

export interface BudgetModel {
  loading: boolean
  setLoading: Action<BudgetModel, boolean>
  isLoaded: boolean
  setIsLoaded: Action<BudgetModel, boolean>
  budget: BudgetResponse[]
  postsWithoutBudget: PostsWithOutBudgetResponse[]
  getBudget: Thunk<BudgetModel, number, Injections>
  getAllPostsWithoutBudgetForYear: Thunk<BudgetModel, number, Injections>
  updateBudget: Thunk<BudgetModel, BudgetUpdateRequestBody, Injections>
  setBudget: Action<BudgetModel, BudgetResponse[]>
  setPostsWithoutBudget: Action<BudgetModel, PostsWithOutBudgetResponse[]>
  createBudgetForOneMonth: Thunk<BudgetModel, PickBudgetExcludeKeyofBudgetId, Injections>
}

export const budgetModel: BudgetModel = {
  loading: false,
  setLoading: action((state, payload) => { state.loading = payload }),
  isLoaded: false,
  setIsLoaded: action((state, payload) => { state.isLoaded = payload }),
  budget: [],
  postsWithoutBudget: [],
  getBudget: thunk((actions, year, helpers) => {
    actions.setLoading(true)
    helpers.injections.budgetApi.getBudgets({ year })
      .then(response => { actions.setBudget(response); })
      .finally(() => {
        actions.setLoading(false)
        actions.setIsLoaded(true)
      })
  }),
  getAllPostsWithoutBudgetForYear: thunk((actions, year, helpers) => {
    actions.setLoading(true)
    helpers.injections.budgetApi.getAllPostsWithoutBudgetFor({ year })
      .then(response => { actions.setPostsWithoutBudget(response); })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
  updateBudget: thunk((actions, budgetUpdateRequestBody, helpers) => {
    actions.setLoading(true)
    helpers.injections.budgetApi.updateBudget({ budgetUpdateRequestBody })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
  setBudget: action((state, payload) => { state.budget = payload }),
  setPostsWithoutBudget: action((state, payload) => { state.postsWithoutBudget = payload }),
  createBudgetForOneMonth: thunk((actions, body, helpers) => {
    actions.setLoading(true)
    helpers.injections.budgetApi.createBudget({ body })
      .finally(() => {
        actions.setLoading(false)
      })
  }),
}
