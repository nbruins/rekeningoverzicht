import { ThemeOptions } from '@mui/material/styles'
import { setTestStore } from '../utils'

describe('configModel test', () => {

  const theme: ThemeOptions = { palette: { mode: 'dark' } }

  it('should load an empty config and then change the theme', () => {

    const store = setTestStore()

    expect(store.getState().config.themeOptions).not.toBe(theme)

    store.getActions().config.setThemeOptions(theme)
    expect(store.getState().config.themeOptions).toStrictEqual(theme)
  })

  it('should load the standard config', () => {

    const store2 = setTestStore()

    expect(store2.getState().config.themeOptions).toEqual(theme)
  })
})
