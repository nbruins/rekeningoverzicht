import { setTestStore } from '../utils'

describe('configureStore test', () => {

  it('should load the standard modules', () => {

    const store = setTestStore()

    expect(store.getState().config).toBeDefined()
    expect(store.getState().transactions).toBeDefined()
  })
})
