import { BudgetResponse, Category, PostsWithOutBudgetResponse, Transaction, TransactionWithPostAndCategory } from '../openapi-fetch'

export interface TransactionsAndCategories {
  transactions: TransactionWithPostAndCategory[],
  categoriesWithPosts: Category[]
}

// "IBAN/BBAN","Munt","BIC","Volgnr","Datum","Rentedatum","Bedrag","Saldo na trn","Tegenrekening IBAN/BBAN","Naam tegenpartij",
// "Naam uiteindelijke partij","Naam initiërende partij","BIC tegenpartij","Code","Batch ID","Transactiereferentie",
// "Machtigingskenmerk","Incassant ID","Betalingskenmerk","Omschrijving-1","Omschrijving-2","Omschrijving-3",
// "Reden retour","Oorspr bedrag","Oorspr munt","Koers"
export interface CSVRabobank {
  'IBAN/BBAN': string
  Munt: string
  BIC: string
  Volgnr: string
  Datum: string
  Rentedatum: string
  Bedrag: string
  'Saldo na trn': string
  'Tegenrekening IBAN/BBAN': string
  'Naam tegenpartij': string
  'Omschrijving-1': string
  'Omschrijving-2': string
  'Omschrijving-3': string
}

export type Months = 'jan' | 'feb' | 'mar' | 'apr' | 'may' | 'jun' | 'jul' | 'aug' | 'sep' | 'oct' | 'nov' | 'dec'

export interface TransactionFilters {
  dateAfterEqual: Date
  dateBeforeEqual: Date
  postIds: number[]
  accountNumbers: string[]
}

export interface StoreMock {
  transactions?: { get: Transaction[] },
  budgets?: {
    get?: BudgetResponse[]
    getPosts?: PostsWithOutBudgetResponse[]
  }
}