import React, { useEffect } from 'react'
import { Container } from '@mui/material'
import { MainPage } from '../components'
import { useTranslation } from 'react-i18next'

export const NotFoundPage = () => {

  const { t } = useTranslation()
  const title = t('titles.page not found')
  useEffect(() => { document.title = title })

  return (
    <MainPage title={title}>
      <Container maxWidth="lg" className={'container'}>
        <h1>{t('titles.page not found')}</h1>
      </Container>
    </MainPage>
  )
}
