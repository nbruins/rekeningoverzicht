import styled from '@emotion/styled'
import { CircularProgress, TextField } from '@mui/material'
import Grid from '@mui/material/Grid2'
import { Chart } from 'chart.js'
import React, { ChangeEvent, FC, useEffect, useRef, useState } from 'react'
import { AutoCompletePost, Option } from '../../../components'
import { Category } from '../../../openapi-fetch'
import { useStoreActions, useStoreState } from '../../../state/typedHooks'
import { dateService } from '../../../utils'
import { trendsChartConfig } from './trendsChartConfig'

const dateBefore = new Date((dateService.getYear()), 0, 1)

const getDateAfter = (noOfYears: number) => new Date((dateBefore.getFullYear() - noOfYears), 0, 1)

const getDefaultPosts = (categories: Category[]) => {
  const defaultPosts = categories
    .filter(category => category.type === 'INKOMSTEN' && category.post !== undefined)
    .flatMap(category => (category.post?.map(post => ({ post: post.name, postId: post.id, category: category.name })) ?? []))

  return defaultPosts
}

const Div = styled.div`
  max-width: 80%;
`
const FormFields = styled.div`
  display: flex;
  direction: row;
`

export const Trends: FC = () => {

  const trendsContainer = useRef<HTMLCanvasElement>(null)

  const filters = useStoreState((state) => state.transactions.filters)
  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)

  const [trendsChart, setTrendsChart] = useState<Chart | null>(null)
  const [noOfYears, setNoOfYears] = useState(5)

  const [defaultValueAutoComplete, setDefaultValueAutoComplete] = useState<Option[] | null>(null)
  const [defaultPostsAreSet, setDefaultPostsAreSet] = useState(false)

  const categories = useStoreState((state) => state.categories.categoriesWithPosts)

  const loading = useStoreState((state) => state.transactions.loading)
  const transactions = useStoreState((state) => state.transactions.transactions)
  const transactionsError = useStoreState((state) => state.transactions.error)

  useEffect(() => {
    const updatedFilter = { ...filters, dateAfterEqual: getDateAfter(noOfYears), dateBeforeEqual: dateBefore }

    if (categories.length > 0 && !defaultPostsAreSet) {

      const defaultPosts = getDefaultPosts(categories)


      updatedFilter.postIds = defaultPosts.map(post => post.postId ?? 0)

      setDefaultValueAutoComplete(defaultPosts)
      setDefaultPostsAreSet(true)
    }
    setFilters(updatedFilter)
  }, [categories, noOfYears])

  useEffect(() => {
    if (!trendsContainer.current || !transactions)
      return

    const yearAfter = getDateAfter(noOfYears).getFullYear()
    const config = trendsChartConfig({ yearAfter, noOfYears, transactions })
    !!trendsChart && trendsChart.destroy()

    setTrendsChart(new Chart(trendsContainer.current, (config)))


  }, [transactions])

  const handleSelectPost = (event: ChangeEvent<unknown>, value: Option | readonly Option[] | null) => {
    if (value) {
      if (Array.isArray(value)) {
        const options = value as Option[]
        setFilters({ ...filters, postIds: options.map(item => item.postId) })
      } else {
        const option = value as Option
        setFilters({ ...filters, postIds: [option.postId] })
      }
    }


  }

  const handleChangeNoOfYears = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {

    setNoOfYears(Number(event.target.value ?? 5))
  }

  if (transactionsError)
    return <p>{String(transactionsError)}</p>

  return (
    <Grid container justifyContent="center" spacing={2}>
      <Grid size={{ xs: 12 }}>
        <h1>Trends</h1>
      </Grid>
      <Grid size={{ xs: 12 }}>
        <FormFields>
          {defaultValueAutoComplete &&
            <AutoCompletePost handleSelectPost={handleSelectPost} multiple defaultValue={defaultValueAutoComplete} />
          }
          <TextField
            sx={{ minWidth: '50px', maxWidth: '80px' }}
            type="number"
            slotProps={{ htmlInput: { inputMode: 'numeric', pattern: '[0-9]*' } }}
            value={noOfYears}
            onChange={handleChangeNoOfYears}
          />
        </FormFields>
      </Grid>
      <Grid size={{ xs: 12 }}>
        {loading && <CircularProgress />}
        <Div>
          <canvas ref={trendsContainer} />
        </Div>
      </Grid>
    </Grid>
  )
}