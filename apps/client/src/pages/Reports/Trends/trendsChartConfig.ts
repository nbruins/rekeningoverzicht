import { ChartConfiguration } from 'chart.js'
import regression from 'regression'
import { TransactionWithPostAndCategory } from '../../../openapi-fetch'
import { dateService } from '../../../utils'


interface Props {
  yearAfter: number
  noOfYears: number
  transactions: TransactionWithPostAndCategory[]
}

export const trendsChartConfig = ({ yearAfter, noOfYears, transactions }: Props): ChartConfiguration => {

  const labels: number[] = []
  const perYearNew: Record<number, number> = {}


  for (let step = 0; step < noOfYears; step++) {
    labels.push((yearAfter + step))
    perYearNew[(yearAfter + step)] = 0
  }

  transactions.forEach(transation => {
    const year = new Date(String(transation.interestDate)).getFullYear()
    const invert = transation.post?.category?.type !== 'INKOMSTEN'
    perYearNew[year] += invert ? transation.amount * -1 : transation.amount
  })


  const linearRegression = regression.linear(labels.map(label => [label, perYearNew[label]]))
  const trendline = linearRegression.points.map(point => point[1])
  const currentYear = dateService.getYear()
  for (let year = currentYear; year < (currentYear + 2); year++) {
    trendline.push(linearRegression.equation[0] * year + linearRegression.equation[1])
    labels.push(year)
  }

  return {
    type: 'line',
    data: {
      labels,
      datasets: [
        {
          data: Object.values(perYearNew),
          label: 'Geselecteerde posten',
          borderColor: '#80bfff',
          backgroundColor: '#80bfff'
        },
        {
          label: 'Trendlijn',
          data: trendline
        }
      ]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  }
}