import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFnsV3'
import { render, screen, waitFor } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { StoreMock } from '../../../../types'
import { setTestStore, transactions } from '../../../../utils'
import { ReportPerYear } from '../ReportPerYear'

describe('ReportPerYear tests', () => {


  it.skip('should render the ReportPerYear without transactions', async () => {

    const store = setTestStore({ setDefault: ['categories', 'filters'] })

    render(
      <StoreProvider store={store}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <ReportPerYear />
        </LocalizationProvider>
      </StoreProvider>
    )

    await waitFor(() => { expect(screen.getByText('Geen gegevens gevonden')).toBeDefined() })
  })

  it('should render the ReportPerYear', async () => {

    const mock: StoreMock = { transactions: { get: transactions } }
    const store = setTestStore({ setDefault: ['categories', 'filters'], mock })

    render(
      <StoreProvider store={store}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <BrowserRouter>
            <ReportPerYear />
          </BrowserRouter>
        </LocalizationProvider>
      </StoreProvider>
    )

    expect(await screen.findByText('income')).toBeInTheDocument()
    expect(await screen.findByText('€ 43,99')).toBeVisible()

  })

})
