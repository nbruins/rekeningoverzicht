import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { ChartDataset, ChartTypeRegistry } from 'chart.js'
import { RecursiveActions } from 'easy-peasy'
import React, { FC, Fragment, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { NavigateFunction, useNavigate } from 'react-router-dom'
import { Category } from '../../../openapi-fetch'
import { TransactionModel } from '../../../state'
import { useStoreActions, useStoreState } from '../../../state/typedHooks'
import { PostTotals, autoFormatValue, dateService } from '../../../utils'

interface TotalsPerYearProps {
  noOfMonths: number,
  noOfWeeks: number,
  totals: (ChartDataset<keyof ChartTypeRegistry, number[]> & PostTotals)[]
  categories?: Category[]
}

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  }
}))

const printTableRowTotals = (data: number[], noOfMonths: number, noOfWeeks: number) => {

  const total = data.reduce((accumulator, currentValue) => accumulator + currentValue, 0)
  const cells = []
  cells.push(<TableCell align="right" key={data.length + 1}>{autoFormatValue(total)}</TableCell>)
  cells.push(<TableCell align="right" key={data.length + 2}>{autoFormatValue(total / noOfMonths)}</TableCell>)
  cells.push(<TableCell align="right" key={data.length + 3}>{autoFormatValue(total / noOfWeeks)}</TableCell>)

  return cells
}

const printTableCellForEachMonth = (
  data: number[],
  categoryName: string,
  transactionActions: RecursiveActions<TransactionModel>,
  dateBeforeEqual: Date,
  accountNumbers: string[],
  navigate: NavigateFunction,
  categories?: Category[]
) => {

  const handleClickOnAmount = (cellIndex: number) => {

    const posts = categories?.find(category => category.name === categoryName)?.post

    if (posts) {
      const selectedYear = dateService.getYear(dateBeforeEqual)
      const dateAfter = dateService.localDateAsUTC(new Date(selectedYear, cellIndex, 1))
      transactionActions.setFilters({
        dateAfterEqual: dateAfter,
        dateBeforeEqual: dateService.lastDateOfMonth(dateAfter),
        accountNumbers,
        postIds: posts.map(post => post.id)
      })
      navigate('/')
    }
  }

  return data.map((amount, index) => (
    <TableCell key={index} align="right" onClick={() => { handleClickOnAmount(index); }}>{autoFormatValue(amount)}</TableCell>
  ))
}

export const TotalsPerYearTable: FC<TotalsPerYearProps> = ({ totals, categories, noOfMonths, noOfWeeks }) => {
  const { t } = useTranslation()
  const transactionActions: RecursiveActions<TransactionModel> = useStoreActions((actions) => actions.transactions)
  const dateBeforeEqual = useStoreState((state) => state.transactions.filters.dateBeforeEqual)
  const accountNumbers = useStoreState((state) => state.transactions.filters.accountNumbers)
  const navigate = useNavigate()
  const [expandCategory, setExpandCategory] = useState<string>('')

  return (
    <TableContainer typeof={'Paper'}>
      <Table size="small" aria-label={String(t('amounts per category'))}>
        <TableHead>
          <TableRow>
            <TableCell />
            {dateService.months().map((month) =>
              <TableCell key={month} align={'right'}>{month}</TableCell>
            )}
            <TableCell align={'right'}>{t('total')}</TableCell>
            <TableCell align={'right'}>{t('monthly')}</TableCell>
            <TableCell align={'right'}>{t('weekly')}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {totals.map(total => total.data && total.label && (
            <Fragment key={total.label}>
              <StyledTableRow>
                <TableCell
                  onClick={() => { setExpandCategory((total.label && total.label !== expandCategory) ? total.label : ''); }}
                >
                  {total.label}
                </TableCell>
                {printTableCellForEachMonth(total.data, total.label, transactionActions,
                  dateBeforeEqual, accountNumbers, navigate, categories)}
                {printTableRowTotals(total.data, noOfMonths, noOfWeeks)}
              </StyledTableRow>
              {total.label === expandCategory && Object.keys(total.postTotals).map(post => (
                <StyledTableRow key={post}>
                  <TableCell> - {post}</TableCell>
                  {printTableCellForEachMonth(total.postTotals[post], post, transactionActions,
                    dateBeforeEqual, accountNumbers, navigate, categories)}
                  {printTableRowTotals(total.postTotals[post], noOfMonths, noOfWeeks)}
                </StyledTableRow>
              ))}
            </Fragment>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}