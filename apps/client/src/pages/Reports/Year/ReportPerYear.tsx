import { CircularProgress } from '@mui/material'
import Grid from '@mui/material/Grid2'
import { ChartDataset, ChartTypeRegistry } from 'chart.js'
import React, { FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { MonthYearFilter } from '../../../components'
import { useDidLoad, useSetDateRange } from '../../../hooks'
import { useStoreState } from '../../../state/typedHooks'
import { PostTotals, dateService, sumTotalsPerCategoryPerMonth } from '../../../utils'
import { TotalsPerYearTable } from './TotalsPerYearTable'

export const ReportPerYear: FC = () => {

  const { t } = useTranslation()
  const loading = useStoreState((state) => state.transactions.loading)
  const transactions = useStoreState((state) => state.transactions.transactions)
  const transactionsError = useStoreState((state) => state.transactions.error)

  const categories = useStoreState((state) => state.categories.categoriesWithPosts)

  const didLoad = useDidLoad(loading)
  useSetDateRange()

  const [totals, setTotals] = useState<(ChartDataset<keyof ChartTypeRegistry, number[]> & PostTotals)[]>([])
  const [noOfMonths, setNoOfMonths] = useState(1)
  const [noOfWeeks, setNoOfWeeks] = useState(1)

  useEffect(() => {
    if (transactions) {

      const totalesPerMonth = sumTotalsPerCategoryPerMonth(transactions)

      setTotals(totalesPerMonth.totals)
      setNoOfMonths(dateService.getMonth(totalesPerMonth.lastTransactionDate) + 1)
      setNoOfWeeks(dateService.getISOWeek(totalesPerMonth.lastTransactionDate))
    }
  }, [transactions])

  return (
    <Grid container justifyContent="center" spacing={2}>
      <Grid size={{ xs: 12 }}>
        <MonthYearFilter views={['year']} openTo='year' />
      </Grid>
      {transactionsError && <Grid><p>Error</p></Grid>}
      {!loading && didLoad && totals.length <= 0 && <Grid><p>Geen gegevens gevonden</p></Grid>}
      {totals.length > 0 &&
        <>
          <Grid size={{ xs: 12 }}>
            <p>{t('income')}</p>
            <TotalsPerYearTable categories={categories} totals={totals.filter(total => total.stack === 'INKOMSTEN')}
              noOfMonths={noOfMonths} noOfWeeks={noOfWeeks} />
          </Grid>
          <Grid size={{ xs: 12 }}>
            <p>{t('savings')}</p>
            <TotalsPerYearTable categories={categories} totals={totals.filter(total => total.stack === 'SPAREN')}
              noOfMonths={noOfMonths} noOfWeeks={noOfWeeks} />
          </Grid>
          <Grid size={{ xs: 12 }}>
            <p>{t('expenses')}</p>
            <TotalsPerYearTable categories={categories} totals={totals.filter(total => total.stack === 'UITGAVEN')}
              noOfMonths={noOfMonths} noOfWeeks={noOfWeeks} />
          </Grid>
        </>
      }
      {loading && <CircularProgress />}
    </Grid>
  )
}
