import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { StoreMock } from '../../../types'
import { budgets, setTestStore, transactions } from '../../../utils'
import ReportsPage from '../ReportsPage'

describe('ReportsPage test', () => {

  const mock: StoreMock = {
    budgets: { get: budgets },
    transactions: { get: transactions }
  }
  const store = setTestStore({ setDefault: ['categories', 'filters'], mock })

  it('should render the ReportsPage with no Errors', async () => {

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <ReportsPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(screen.getByText('Overzicht per maand')).toBeDefined()
    expect(await screen.findByText('expenses')).toBeInTheDocument()
  })

})
