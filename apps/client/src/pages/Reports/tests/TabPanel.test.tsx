import React from 'react'
import { render, screen } from '@testing-library/react'
import { TabPanel } from '../TabPanel'

describe('TabPanel tests', () => {

  it('should render the TabPanel and show its contents', () => {

    render(<TabPanel value={0} index={0}>
      <p>child text</p>
    </TabPanel>
    )
    expect(screen.getByText('child text')).toBeDefined()
  })

  it('should render the TabPanel and hide its contents', () => {

    render(<TabPanel value={0} index={1}>
      <p>child text</p>
    </TabPanel>
    )
    expect(screen.queryByText('child text')).toBeNull()
  })
})
