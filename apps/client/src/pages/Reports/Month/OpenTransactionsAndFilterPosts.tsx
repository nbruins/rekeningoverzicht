import { Button } from '@mui/material'
import React, { FC, ReactNode } from 'react'
import { useNavigate } from 'react-router-dom'
import { useStoreActions, useStoreState } from '../../../state/typedHooks'

interface Props {
  postIds?: number[]
  children: ReactNode
}
export const OpenTransactionsAndFilterPosts: FC<Props> = ({ postIds, children }) => {

  const filters = useStoreState((state) => state.transactions.filters)
  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)
  const navigate = useNavigate()

  if (!postIds) return <>{children}</>

  const onClick = () => {
    setFilters({ ...filters, postIds })
    navigate('/')
  }

  return <Button sx={{ border: 'none', background: 'none' }} onClick={onClick}>
    {children}
  </Button>
}
