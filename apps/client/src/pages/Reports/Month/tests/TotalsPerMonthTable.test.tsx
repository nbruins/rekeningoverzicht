import React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import { TotalsPerMonthTable } from '../TotalsPerMonthTable'
import { CategoryTotals } from '../../../../utils'
import userEvent from '@testing-library/user-event'
import { createTheme, ThemeProvider } from '@mui/material/styles'

describe('TotalsPerMonthTable tests', () => {

  const totals = (type: string): CategoryTotals => ({
    caterory1: {
      id: 1, budget: 100, actual: 120, type, name: 'category1', backgroundColor: '#FFF',
      color: '#000', icon: 'euro', borderColor: '#123', gemmiddeldbedragNibud: 126,
      posts: {
        post1: { id: 1, name: 'post1', actual: 20, budget: 50 },
        post2: { id: 2, name: 'post2', actual: 100, budget: 50 }
      }
    },
    caterory2: {
      id: 2, budget: 200, actual: 190, type, name: 'category2', backgroundColor: '#FFF',
      color: '#000', icon: 'euro', borderColor: '#123', gemmiddeldbedragNibud: 126,
      posts: {
        post3: { id: 3, name: 'post3', actual: 90, budget: 100 },
        post4: { id: 4, name: 'post4', actual: 100, budget: 100 }
      }
    }
  })

  const theme = createTheme()

  it('should render the TotalsPerMonthTable for UITGAVEN', () => {

    render(
      <ThemeProvider theme={theme}>
        <TotalsPerMonthTable type={'UITGAVEN'} totals={totals('UITGAVEN')} />
      </ThemeProvider>
    )
    expect(screen.getByText('category1')).toBeDefined()
    expect(screen.getByText('category2')).toBeDefined()
  })

  it('should render the TotalsPerMonthTable for INKOMSTEN', () => {

    render(
      <ThemeProvider theme={theme}>
        <TotalsPerMonthTable type={'INKOMSTEN'} totals={totals('INKOMSTEN')} />
      </ThemeProvider>
    )
    expect(screen.getByText('category1')).toBeDefined()
    expect(screen.getByText('category2')).toBeDefined()
  })

  it('should render the TotalsPerMonthTable for SPAREN', () => {

    render(
      <ThemeProvider theme={theme}>
        <TotalsPerMonthTable type={'SPAREN'} totals={totals('SPAREN')} />
      </ThemeProvider>
    )
    expect(screen.getByText('category1')).toBeDefined()
    expect(screen.getByText('category2')).toBeDefined()
  })

  it('should handle click on category', async () => {

    render(
      <ThemeProvider theme={theme}>
        <TotalsPerMonthTable type={'UITGAVEN'} totals={totals('UITGAVEN')} />
      </ThemeProvider>
    )

    userEvent.click(screen.getAllByRole('button')[0])
    await waitFor(() => { expect(screen.getByText('post1')).toBeDefined() })
  })
})
