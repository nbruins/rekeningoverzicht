import { createTheme, ThemeProvider } from '@mui/material/styles'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFnsV3'
import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { StoreMock } from '../../../../types'
import { budgets, setTestStore, transactions } from '../../../../utils'
import { ReportPerMonth } from '../ReportPerMonth'

describe('ReportPerMonth tests', () => {

  const theme = createTheme()
  const mock: StoreMock = {
    budgets: { get: budgets },
    transactions: { get: transactions }
  }
  const store = setTestStore({ setDefault: ['categories', 'filters'], mock })

  it('should render the ReportPerMonth', async () => {

    render(
      <StoreProvider store={store}>
        <ThemeProvider theme={theme}>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <BrowserRouter>
              <ReportPerMonth />
            </BrowserRouter>
          </LocalizationProvider>
        </ThemeProvider>
      </StoreProvider>
    )

    expect(await screen.findByText('expenses')).toBeInTheDocument()
  })

})
