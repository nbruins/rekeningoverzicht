import { CircularProgress } from '@mui/material'
import Grid from '@mui/material/Grid2'
import Plotly, { Layout } from 'plotly.js'
import React, { FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { MonthYearFilter } from '../../../components/MonthYearFilter'
import { Category, TransactionWithPostAndCategory } from '../../../openapi-fetch'
import { useStoreActions, useStoreState } from '../../../state/typedHooks'
import { Months } from '../../../types'
import { CategoryTotals, dateService, sumTotalsForTable, sumTotalsForTreemap } from '../../../utils'
import { OpenTransactionsAndFilterPosts } from './OpenTransactionsAndFilterPosts'
import { TotalsPerMonthTable } from './TotalsPerMonthTable'

interface PlotTreemap {
  transactions: TransactionWithPostAndCategory[]
  categoriesWithPosts: Category[]
}
const plotTreemap = ({ transactions, categoriesWithPosts }: PlotTreemap) => {
  const layout: Partial<Layout> = {
    margin: { l: 0, r: 0, t: 0, b: 0 }
  }
  const treemapData = sumTotalsForTreemap({ transactions, categoriesWithPosts })
  void Plotly.newPlot('treemap', treemapData, layout).catch(console.error)
}

export const ReportPerMonth: FC = () => {

  const { t } = useTranslation()
  const loading = useStoreState((state) => state.transactions.loading)
  const transactions = useStoreState((state) => state.transactions.transactions)
  const transactionsError = useStoreState((state) => state.transactions.error)

  const categoriesLoading = useStoreState((state) => state.categories.loading)
  const categoriesWithPosts = useStoreState((state) => state.categories.categoriesWithPosts)
  const budgetLoading = useStoreState((state) => state.budget.loading)
  const budgetIsLoaded = useStoreState((state) => state.budget.isLoaded)
  const budgets = useStoreState((state) => state.budget.budget)

  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)
  const getBudget = useStoreActions((actions) => actions.budget.getBudget)

  const dateBeforeEqual = useStoreState((state) => state.transactions.filters.dateBeforeEqual)
  const accountNumbers = useStoreState((state) => state.transactions.filters.accountNumbers)
  const [totals, setTotals] = useState<CategoryTotals | null>(null)
  const [filterIsSet, setFilterIsSet] = useState(false)

  useEffect(() => {
    setFilters({
      dateAfterEqual: dateService.firstDateOfMonth(),
      dateBeforeEqual: dateService.lastDateOfMonth(),
      accountNumbers,
      postIds: []
    })
    setFilterIsSet(!filterIsSet)
  }, [])

  useEffect(() => {
    filterIsSet && getBudget(dateService.getYear(dateBeforeEqual))
  }, [dateBeforeEqual, filterIsSet])

  useEffect(() => {
    if (filterIsSet && transactions && categoriesWithPosts && budgetIsLoaded) {

      const months: Months[] = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
      const month = months[dateService.getMonth(dateBeforeEqual)]

      setTotals(sumTotalsForTable({ transactions, categoriesWithPosts, budgets, month }))
      plotTreemap({ transactions, categoriesWithPosts })
    }
  }, [transactions, categoriesWithPosts, budgetIsLoaded, filterIsSet])

  const handleClickOnAmount = (amount: string, postIds?: number[]) =>
    <OpenTransactionsAndFilterPosts postIds={postIds}>{amount}</OpenTransactionsAndFilterPosts>

  return (
    <Grid container justifyContent="center" rowSpacing={1} columnSpacing={{ xs: 0, sm: 2 }}>
      <Grid size={{ xs: 12 }}>
        <MonthYearFilter />
      </Grid>
      <Grid size={{ xs: 12 }}>
        <div id='treemap' />
      </Grid>
      {(transactionsError) && <Grid size={{ xs: 12 }}><p>Error</p></Grid>}
      {(budgetLoading || loading || categoriesLoading) &&
        <CircularProgress />
      }
      {totals &&
        <>
          <Grid size={{ xs: 4 }}>
            <p>{t('expenses')}</p>
            <TotalsPerMonthTable totals={totals} type={'UITGAVEN'} handleClickOnAmount={handleClickOnAmount} />
          </Grid>
          <Grid size={{ xs: 4 }}>
            <p>{t('income')}</p>
            <TotalsPerMonthTable totals={totals} type={'INKOMSTEN'} handleClickOnAmount={handleClickOnAmount} />
          </Grid>
          <Grid size={{ xs: 4 }}>
            <p>{t('savings')}</p>
            <TotalsPerMonthTable totals={totals} type={'SPAREN'} handleClickOnAmount={handleClickOnAmount} />
          </Grid>
        </>
      }
    </Grid>
  )
}
