import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
import React, { FC, MouseEvent, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useWindowDimensions } from '../../../hooks'
import { bedragMin, bedragPlus } from '../../../styles'
import { CategoryTotal, CategoryTotals, formatValue } from '../../../utils'
import { SpendingVsBudget } from './SpendingVsBudget'

interface RowProps {
  category?: CategoryTotal
  type?: string
  name: string
  actual: number
  budget: number
  width: number
  handleClickOnCategory?: (event: MouseEvent<HTMLButtonElement>) => void
  handleClickOnAmount?: (amount: string, postIds?: number[]) => React.ReactNode
  postIds?: number[]
}
const Row: FC<RowProps> = ({ category, type, name, actual, budget, width, handleClickOnCategory, handleClickOnAmount, postIds }) => (
  <TableRow>
    <TableCell>
      {type === 'UITGAVEN' && category ?
        <SpendingVsBudget
          category={category}
          actual={actual}
          budget={budget}
          onClick={handleClickOnCategory}
        />
        : <>{name}</>}
    </TableCell>
    <TableCell align="right" sx={(actual < 0) ? bedragMin : bedragPlus}>
      {handleClickOnAmount ?
        handleClickOnAmount(formatValue(actual, 'currency'), postIds) : formatValue(actual, 'currency')}
    </TableCell>
    {width >= 560 &&
      <>
        <TableCell align="right" sx={(budget < 0) ? bedragMin : bedragPlus}>
          {formatValue(budget, 'currency')}
        </TableCell>
        <TableCell align="right" sx={((actual - budget) < 0) ? bedragMin : bedragPlus}>
          {formatValue(actual - budget, 'currency')}
        </TableCell>
      </>
    }
  </TableRow>
)

interface Props {
  totals: CategoryTotals
  /** type = INKOMSTEN, UITGAVEN, SPAREN */
  type: string
  handleClickOnAmount?: (amount: string, postIds?: number[]) => React.ReactNode
}

export const TotalsPerMonthTable: FC<Props> = ({ totals, type, handleClickOnAmount }) => {

  const { t } = useTranslation()
  const { width } = useWindowDimensions()

  const [showPosts, setShowPosts] = useState<Record<string, boolean>>({})
  useEffect(() => {
    if (totals)
      setShowPosts(Object.keys(totals).reduce((acc, curr) => ({ ...acc, [curr]: false }), {}))
  }, [totals])

  const handleClickOnCategory = (event: MouseEvent<HTMLButtonElement>) => {
    const categoryName = (event.target as HTMLInputElement).value
    const showHide = showPosts[categoryName]
    setShowPosts({ ...showPosts, [categoryName]: !showHide })
  }

  let actualTotal = 0
  let budgetTotal = 0
  Object.values(totals).filter(tot => tot.type === type)
    .forEach(total => {
      actualTotal += total.actual
      budgetTotal += total.budget
    })

  return (
    <Paper sx={{ maxWidth: '560px' }}>
      <TableContainer>
        <Table size="small" aria-label={'Bedragen per caqtegorie'} sx={{ maxWidth: '560px' }}>
          <TableHead>
            <TableRow>
              <TableCell>{t('category')}</TableCell>
              <TableCell align="right">{t('actual')}</TableCell>
              {width >= 560 &&
                <>
                  <TableCell align="right">{t('budgeted')}</TableCell>
                  <TableCell align="right">{t('difference')}</TableCell>
                </>
              }
            </TableRow>
          </TableHead>
          <TableBody>
            {Object.values(totals).filter(category => category.type === type)
              .filter(category => (!(category.actual === 0 && category.budget === 0)))
              .map(category => (
                <React.Fragment key={category.id}>
                  <Row
                    category={category}
                    type={type}
                    name={category.name}
                    actual={category.actual}
                    budget={category.budget}
                    width={width}
                    handleClickOnCategory={handleClickOnCategory}
                    handleClickOnAmount={handleClickOnAmount}
                    postIds={Object.values(category.posts).map(post => (post.id))}
                  />
                  {showPosts[category.name] && Object.values(category.posts).map((post => (
                    <Row key={post.name} name={post.name} actual={post.actual} budget={post.budget}
                      width={width} postIds={[post.id]}
                    />
                  )))}
                </React.Fragment>
              ))}
            {/*  Row with totals */}
            {type === 'UITGAVEN' &&
              <TableRow>
                <TableCell align={'right'}>Totaal</TableCell>
                <TableCell align="right" sx={(actualTotal < 0) ? bedragMin : bedragPlus}>
                  {formatValue(actualTotal, 'currency')}
                </TableCell>
                {width >= 560 &&
                  <>
                    <TableCell align="right" sx={(budgetTotal < 0) ? bedragMin : bedragPlus}>
                      {formatValue(budgetTotal, 'currency')}
                    </TableCell>
                    <TableCell align="right" sx={((actualTotal - budgetTotal) < 0) ? bedragMin : bedragPlus}>
                      {formatValue(actualTotal - budgetTotal, 'currency')}</TableCell>
                  </>
                }
              </TableRow>
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  )
}
