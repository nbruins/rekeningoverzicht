import styled from '@emotion/styled'
import React, { FC } from 'react'

const Div = styled.div`
  box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.25);
  position: relative;
  flex: 1;
  min-width: 100px;
  height: 16px;
  border-radius: 8px;
  background-color: white;
  overflow: hidden;
  margin: 0 0 4px 0
`

interface SpanProps {percentage: number}

const Span = styled.span<SpanProps>`
  width: ${props => props.percentage}%;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  transition: width 0.2s ease-out;
  background-color: ${props => props.percentage > 100 ? '#CC3333' : '#339933'};
  background-image: ${props => (props.percentage > 90 && props.percentage < 100) && 'linear-gradient(to right, #339933, #FF9900)'}
`

interface Props {
  amount: number
  budget?: number
}

export const Meter: FC<Props> = ({amount, budget}) => (
  <Div>
    <Span percentage={budget ? amount * 100 / budget : amount ? 100 : 0}/>
  </Div>
)
