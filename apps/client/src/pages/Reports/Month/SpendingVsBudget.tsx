import { Box, Button, SxProps } from '@mui/material'
import React, { FC, MouseEvent } from 'react'
import { CategoryIcon } from '../../../components'
import { Meter } from './Meter'
import { Category } from '../../../openapi-fetch'

const spendingsVsBudgetStyle: SxProps = {
  pointerEvents: 'none',
  fontSize: '0.85em',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  maxWidth: '560px'
}

const progressStyle: SxProps = {
  flexGrow: 1,
  marginLeft: '10px',
  width: '130px',
  textAlign: 'start',
  div: {
    margin: '6px 0 0 0',
  }
}

interface Props {
  category: Category
  actual: number
  budget: number
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void
}
export const SpendingVsBudget: FC<Props> = ({ category, actual, budget, onClick }) => (
  <Button sx={{ border: 'none', background: 'none', }} onClick={onClick} value={category.name}>
    <Box sx={spendingsVsBudgetStyle}>
      <CategoryIcon category={category} />
      <Box sx={progressStyle}>
        <span>{category.name}</span>
        <Meter amount={actual} budget={budget} />
      </Box>
    </Box>
  </Button>
)

