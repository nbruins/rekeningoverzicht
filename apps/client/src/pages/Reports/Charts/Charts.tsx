import { CircularProgress } from '@mui/material'
import Grid from '@mui/material/Grid2'
import React, { FC, useEffect, useState } from 'react'
import { MonthYearFilter } from '../../../components'
import { useDidLoad, useSetDateRange } from '../../../hooks'
import { TransactionWithPostAndCategory } from '../../../openapi-fetch'
import { useStoreState } from '../../../state/typedHooks'
import { TotalsPerCategoryPerMonth, dateService, sumTotalsPerCategoryPerMonth } from '../../../utils'
import { BalanceChart } from './BalanceChart'
import { ExpensesPerCategory } from './ExpensesPerCategory'
import { ExpensesPerCategoryDoughnut } from './ExpensesPerCategoryDoughnut'
import { ExpensesPerMonthChart } from './ExpensesPerMonthChart'

export const Charts: FC = () => {

  const loading = useStoreState((state) => state.transactions.loading)
  const transactions = useStoreState((state) => state.transactions.transactions)
  const transactionsError = useStoreState((state) => state.transactions.error)

  const dateAfterEqual = useStoreState((state) => state.transactions.filters.dateAfterEqual)
  const dateBeforeEqual = useStoreState((state) => state.transactions.filters.dateBeforeEqual)

  const [savingsToAndFrom, setSavingsToAndFrom] = useState<Partial<TransactionWithPostAndCategory>[]>([])
  const [totalsPerCategoryPerMonth, setTotalsPerCategoryPerMonth] =
    useState<TotalsPerCategoryPerMonth>({ lastTransactionDate: new Date(), totals: [] })

  const didLoad = useDidLoad(loading)
  useSetDateRange()

  useEffect(() => {
    if (transactions)
      setTotalsPerCategoryPerMonth(sumTotalsPerCategoryPerMonth(transactions))
    const savings = transactions.filter(transaction => transaction.post?.category?.type === 'SPAREN') ?? []
    const data = new Map<string, { entryDate: Date, balance: number }>()
    data.set(dateService.formatYYYYMMDD(dateAfterEqual), { entryDate: dateAfterEqual, balance: 0 })

    let balance = 0
    savings.sort((a, b) => (a.entryDate === b.entryDate ? 0 : a.entryDate > b.entryDate ? 1 : -1))
      .forEach(transaction => {

        const entryDate = dateService.formatYYYYMMDD(transaction.entryDate)
        const amount = transaction.amount * -1

        balance = balance + amount

        const value = data.has(entryDate) ? (data.get(entryDate)?.balance ?? 0) + amount : balance
        data.set(entryDate, { entryDate: transaction.entryDate, balance: value })

      })
    if (dateBeforeEqual > dateService.currentDate()) {
      data.set(dateService.formatYYYYMMDD(), { entryDate: dateService.currentDate(), balance })
    } else {
      data.set(dateService.formatYYYYMMDD(dateBeforeEqual), { entryDate: dateBeforeEqual, balance })
    }
    setSavingsToAndFrom(Array.from(data.values()))
  }, [transactions])

  return (
    <div style={{ padding: 0 }}>
      {transactionsError && (
        <Grid>
          <p>Error</p>
        </Grid>
      )}
      {!loading && didLoad && !transactions && (
        <Grid>
          <p>Geen gegevens gevonden</p>
        </Grid>
      )}
      {loading && <CircularProgress />}
      {transactions && (
        <Grid container justifyContent="center" rowSpacing={1} columnSpacing={{ xs: 1, sm: 12 }}>
          <Grid size={{ xs: 12, sm: 12 }}>
            <MonthYearFilter views={['year']} openTo='year' />
          </Grid>
          <Grid size={{ xs: 12, sm: 6 }}>
            <ExpensesPerMonthChart totalsPerCategoryPerMonth={totalsPerCategoryPerMonth} />
          </Grid>
          <Grid size={{ xs: 12, sm: 6 }}>
            <BalanceChart transactions={transactions} />
          </Grid>
          <Grid size={{ xs: 12, sm: 6 }}>
            <ExpensesPerCategory totalsPerCategoryPerMonth={totalsPerCategoryPerMonth} />
          </Grid>
          <Grid size={{ xs: 12, sm: 6 }}>
            <BalanceChart transactions={savingsToAndFrom} />
          </Grid>
          <Grid size={{ xs: 12, sm: 12 }} />
          {[...Array(12).keys()].map(month => (
            <Grid key={month} size={{ xs: 12, sm: 2 }}>
              <ExpensesPerCategoryDoughnut totalsPerCategoryPerMonth={totalsPerCategoryPerMonth} monthIndex={month} />
            </Grid>
          ))}
        </Grid>
      )}
    </div>
  )
}
