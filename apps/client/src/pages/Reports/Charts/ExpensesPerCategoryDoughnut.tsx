import {
  ArcElement, Chart, ChartConfiguration, DoughnutController
} from 'chart.js'
import React, { FC, useEffect, useRef, useState } from 'react'
import { dateService, TotalsPerCategoryPerMonth } from '../../../utils'

Chart.register(DoughnutController, ArcElement)

interface Props {
  totalsPerCategoryPerMonth: TotalsPerCategoryPerMonth,
  monthIndex: number
}

export const expensesDoughnutChartConfig = ({ totalsPerCategoryPerMonth, monthIndex }: Props): ChartConfiguration => {

  const labels: string[] = []
  const data: number[] = []
  const backgroundColor: string[] = []

  totalsPerCategoryPerMonth.totals.forEach(total => {
    labels.push(total.label ?? 'Unknown')
    data.push(total.data[monthIndex] ?? 0)
    backgroundColor.push((total.backgroundColor as string) ?? '#CCC')
  })

  return ({
    type: 'doughnut',
    data: {
      labels,
      datasets: [
        {
          data,
          backgroundColor
        }
      ]
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: false,
          position: 'top',
        },
        title: {
          display: true,
          text: dateService.months()[monthIndex]
        }
      }
    }
  })
}

export const ExpensesPerCategoryDoughnut: FC<Props> = ({ totalsPerCategoryPerMonth, monthIndex }) => {

  const container = useRef<HTMLCanvasElement>(null)
  const [expensesPerMonthChart, setExpensesPerMonthChart] = useState<Chart | null>(null)

  useEffect(() => {
    if (container.current) {

      if (expensesPerMonthChart)
        expensesPerMonthChart.destroy()
      const config = expensesDoughnutChartConfig({ totalsPerCategoryPerMonth, monthIndex })
      setExpensesPerMonthChart(
        new Chart(
          container.current,
          config
        )
      )
    }
  }, [totalsPerCategoryPerMonth, container])


  return <canvas ref={container} />
}
