import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFnsV3'
import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import fetchMock from 'fetch-mock-jest'
import React from 'react'
import { setTestStore, transactions } from '../../../../utils'
import { Charts } from '../Charts'


describe('Test the charts', () => {

  const store = setTestStore({ setDefault: ['categories', 'filters'] })

  it('should render the charts', async () => {

    window.ResizeObserver =
      window.ResizeObserver ||
      jest.fn().mockImplementation(() => ({
        disconnect: jest.fn(),
        observe: jest.fn(),
        unobserve: jest.fn(),
      }))

    fetchMock.get('path:/api/transactions', transactions)

    render(
      <StoreProvider store={store}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Charts />
        </LocalizationProvider>
      </StoreProvider>
    )

    expect(await screen.findAllByText('Jaar/Maand')).toHaveLength(2)

    fetchMock.mockReset()
  })
})
