import { ChartConfiguration } from 'chart.js'
import { TFunction } from 'i18next'
import { dateService } from '../../../utils'
import { TransactionWithPostAndCategory } from '../../../openapi-fetch'

export const balanceChartConfig = (transactions: Partial<TransactionWithPostAndCategory>[], t: TFunction): ChartConfiguration => {

  const uniqueDates: number[] = []
  const labels: string[] = []
  const data: number[] = []

  transactions.forEach((transaction) => {
    if (transaction.interestDate) {
      const interestDate = transaction.interestDate
      if (!uniqueDates.includes(interestDate.getTime())) {
        uniqueDates.push(interestDate.getTime())
        labels.unshift(dateService.formatYYYYMMDD(interestDate))
        data.unshift(transaction.balance ?? 0)
      }
    }
  })
  return {
    type: 'line',
    data: {
      labels,
      datasets: [
        {
          label: t('balance'),
          fill: false,
          borderColor: '#1F77B4',
          data,
          pointRadius: 0,
          borderWidth: 1
        },
      ],
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: t('balance'),
          align: 'start'
        },
        legend: { display: false }
      },
      scales: {
        x: {
          type: 'time',
          time: {
            tooltipFormat: 'dd t',
          },
          title: {
            display: true,
            text: t('date'),
          }
        },
        y: {
          title: {
            display: true,
            text: t('balance'),
          }
        }
      }
    }
  }
}
