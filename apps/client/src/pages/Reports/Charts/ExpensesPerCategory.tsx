import { MenuItem, Select } from '@mui/material'
import {
  BarController, BarElement, CategoryScale, Chart, Legend,
  LinearScale, PointElement, SubTitle, TimeScale, Title, Tooltip
} from 'chart.js'
import React, { FC, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useSetDateRange } from '../../../hooks'
import { useStoreState } from '../../../state/typedHooks'
import { TotalsPerCategoryPerMonth } from '../../../utils'
import { expensesBarChartConfig } from './expensesBarChartConfig'

Chart.register(
  BarController,
  BarElement,
  PointElement,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Title,
  Tooltip,
  SubTitle
)
const colorArray = [
  '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', '#E6B333', '#3366E6', '#999966', '#99FF99',
  '#B34D4D', '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', '#FF99E6', '#CCFF1A', '#FF1A66',
  '#E6331A', '#33FFCC', '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', '#66664D', '#991AFF',
  '#E666FF', '#4DB3FF', '#1AB399', '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', '#4D8066',
  '#809980', '#E6FF80', '#1AFF33', '#999933', '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
]

const filterCategoryAndMakeDatasetOfPosts = (totalsPerCategoryPerMonth: TotalsPerCategoryPerMonth, selectedCategory: string) => {
  const category = totalsPerCategoryPerMonth.totals.find(row => row.label === selectedCategory,)
  const postTotals = category?.postTotals ?? {}

  return Object.keys(postTotals).map(
    (key, index) => (
      {
        label: key,
        data: postTotals[key],
        backgroundColor: colorArray[index] ?? '#FF6633'
      }
    )
  )
}

export const ExpensesPerCategory: FC<{ totalsPerCategoryPerMonth: TotalsPerCategoryPerMonth }> = ({ totalsPerCategoryPerMonth }) => {
  const { t } = useTranslation()

  const categoriesWithPosts = useStoreState((state) => state.categories.categoriesWithPosts)

  useSetDateRange()

  const [selectedCategory, setSelectedCategory] = useState('')
  useEffect(() => { if (categoriesWithPosts.length > 0) setSelectedCategory(categoriesWithPosts[0].name) }, [categoriesWithPosts])

  const expensesPerCatPerMonthContainer = useRef<HTMLCanvasElement>(null)
  const [expensesPerCatPerMonthChart, setExpensesPerCatPerMonthChart] = useState<Chart | null>(null)

  useEffect(() => {
    if (expensesPerCatPerMonthContainer.current) {

      if (expensesPerCatPerMonthChart)
        expensesPerCatPerMonthChart.destroy()
      setExpensesPerCatPerMonthChart(
        new Chart(
          expensesPerCatPerMonthContainer.current,
          expensesBarChartConfig(filterCategoryAndMakeDatasetOfPosts(totalsPerCategoryPerMonth, selectedCategory), t)
        )
      )

    }
  }, [totalsPerCategoryPerMonth, selectedCategory])


  return (
    <>
      <Select
        size="small"
        variant="standard"
        labelId="select--category-label"
        id="demo-simple-select"
        value={selectedCategory}
        label="Category"
        onChange={(event) => { setSelectedCategory(event.target.value); }}
      >
        {categoriesWithPosts.map(category =>
          <MenuItem key={category.id} value={category.name}>{category.name}</MenuItem>
        )}
      </Select>
      <canvas ref={expensesPerCatPerMonthContainer} />
    </>
  )
}
