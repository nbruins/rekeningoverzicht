import {
  BarController,
  BarElement,
  CategoryScale,
  Chart, Legend,
  LinearScale,
  LineController,
  LineElement,
  PointElement,
  SubTitle,
  TimeScale,
  Title,
  Tooltip
} from 'chart.js'
import React, { FC, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { TotalsPerCategoryPerMonth } from '../../../utils'
import { expensesBarChartConfig } from './expensesBarChartConfig'

Chart.register(
  BarController,
  LineController,
  BarElement,
  LineElement,
  PointElement,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Title,
  Tooltip,
  SubTitle
)

export const ExpensesPerMonthChart: FC<{ totalsPerCategoryPerMonth: TotalsPerCategoryPerMonth }> = ({ totalsPerCategoryPerMonth }) => {

  const { t } = useTranslation()

  const expensesPerMonthContainer = useRef<HTMLCanvasElement>(null)
  const [expensesPerMonthChart, setExpensesPerMonthChart] = useState<Chart | null>(null)

  useEffect(() => {
    if (expensesPerMonthContainer.current) {

      if (expensesPerMonthChart)
        expensesPerMonthChart.destroy()

      setExpensesPerMonthChart(
        new Chart(
          expensesPerMonthContainer.current,
          expensesBarChartConfig(totalsPerCategoryPerMonth.totals.filter(row => row.stack !== 'SPAREN'), t)
        )
      )
    }
  }, [totalsPerCategoryPerMonth, expensesPerMonthContainer])


  return <canvas ref={expensesPerMonthContainer} />
}
