import { ChartConfiguration, ChartDataset, ChartTypeRegistry } from 'chart.js'
import { TFunction } from 'i18next'
import { dateService } from '../../../utils'

export const expensesBarChartConfig = (
  datasets: (ChartDataset<keyof ChartTypeRegistry, number[]>)[],
  t: TFunction
): ChartConfiguration => ({
  type: 'bar',
  data: {
    labels: dateService.months(),
    datasets,
  },
  options: {
    plugins: {
      legend: {
        display: false,
        position: 'top',
        align: 'start'
      },
      title: {
        display: true,
        text: t('expenses per month'),
        align: 'start'
      },
      tooltip: {
        mode: 'index',
        intersect: false,
      },
    },
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true,
      },
    },
  },
})