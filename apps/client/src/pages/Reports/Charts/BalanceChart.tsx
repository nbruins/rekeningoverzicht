import { CategoryScale, Chart, LineController, LineElement, PointElement, TimeScale, Title } from 'chart.js'
import 'chartjs-adapter-date-fns'
import React, { FC, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { balanceChartConfig } from './balanceChartConfig'
import { Transaction } from '../../../openapi-fetch/models/Transaction'


Chart.register(
  LineController,
  LineElement,
  PointElement,
  CategoryScale,
  TimeScale,
  Title
)

export const BalanceChart: FC<{ transactions: Partial<Transaction>[] }> = ({ transactions }) => {

  const { t } = useTranslation()
  const balanceContainer = useRef<HTMLCanvasElement>(null)
  const [balance, setBalance] = useState<Chart | null>(null)

  useEffect(() => {
    if (balanceContainer.current) {

      if (balance)
        balance.destroy()

      setBalance(new Chart(balanceContainer.current, balanceChartConfig(transactions, t)))
    }
  }, [transactions, balanceContainer])


  return <canvas ref={balanceContainer} />
}
