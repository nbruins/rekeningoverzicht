import { Box, Typography } from '@mui/material'
import React, { FC } from 'react'

interface TabPanelProps {
  index: number
  value: number
  children?: React.ReactNode
  [props: string]: unknown
}

export const TabPanel: FC<TabPanelProps> = ({ index, value, children, ...props }) => (
  <Typography
    component="div"
    role="tabpanel"
    hidden={value !== index}
    id={`scrollable-auto-tabpanel-${String(index)}`}
    aria-labelledby={`scrollable-auto-tab-${String(index)}`}
    {...props}
  >
    {value === index && <Box p={3}>{children}</Box>}
  </Typography>
)

export const a11yProps = (index: number): { id: string, 'aria-controls': string } => ({
  'id': `scrollable-auto-tab-${String(index)}`,
  'aria-controls': `scrollable-auto-tabpanel-${String(index)}`,
})
