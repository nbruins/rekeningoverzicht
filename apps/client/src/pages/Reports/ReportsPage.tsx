import { AppBar, Box, Tab, Tabs } from '@mui/material'
import React, { FC, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { MainPage } from '../../components'
import { useWindowDimensions } from '../../hooks'
import { Charts } from './Charts'
import { ReportPerMonth } from './Month'
import { TabPanel, a11yProps } from './TabPanel'
import { Trends } from './Trends'
import { ReportPerYear } from './Year'

const ReportsPage: FC = () => {

  const { t } = useTranslation()

  const { width } = useWindowDimensions()
  const title = 'Overzicht per maand'
  useEffect(() => { document.title = title })

  const [selectedTab, setSelectedTab] = React.useState(0)
  const handleTabChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
    setSelectedTab(newValue)
  }

  return (
    <MainPage title={title}>
      <main>
        <Box sx={{ flexGrow: 1, width: '100%', padding: '0px' }}>
          <AppBar position="static" color="default">
            <Tabs
              value={selectedTab}
              onChange={handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              variant={width < 620 ? 'fullWidth' : 'scrollable'}
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Tab label={t('month')} {...a11yProps(0)} />
              <Tab label={t('year')} {...a11yProps(1)} />
              <Tab label={t('graphs')} {...a11yProps(2)} />
              <Tab label={t('trends')} {...a11yProps(3)} />
            </Tabs>
          </AppBar>
          <TabPanel value={selectedTab} index={0}>
            <ReportPerMonth />
          </TabPanel>
          <TabPanel value={selectedTab} index={1}>
            <ReportPerYear />
          </TabPanel>
          <TabPanel value={selectedTab} index={2} sx={{ padding: '16px 6px' }}>
            <Charts />
          </TabPanel>
          <TabPanel value={selectedTab} index={3}>
            <Trends />
          </TabPanel>
        </Box>
      </main>
    </MainPage>
  )
}
export default ReportsPage
