import { Button, CircularProgress, Container } from '@mui/material'
import { ParseResult, parse } from 'papaparse'
import React, { ChangeEvent, FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { MainPage } from '../../components'
import { Category, PickTransactionExcludeKeyofTransactionId, Transaction } from '../../openapi-fetch'
import { transactionApi } from '../../state'
import { useStoreState } from '../../state/typedHooks'
import { CSVRabobank } from '../../types'
import { ImportResultTable } from './ImportResultTable'
import { dateService } from '../../utils'

interface GetPostProps {
  transaction?: PickTransactionExcludeKeyofTransactionId
  categoriesWithPosts: Category[]
}

interface GetPostResponse {
  connect: {
    id: number
  }
}

const getPost = ({ transaction, categoriesWithPosts }: GetPostProps): GetPostResponse | null => {

  // ToDo refactor
  if (!transaction) {
    let defaultId: number | null = null
    categoriesWithPosts.forEach(
      category => category.post?.forEach(
        pst => {
          if (pst.name === 'Onbekend')
            defaultId = pst.id
        }
      )
    )
    return defaultId ? { connect: { id: defaultId } } : null
  }

  const fields = `${transaction.toFromAccountNumber ?? ''}${transaction.toFromName ?? ''}${transaction.description ?? ''}`

  let post: GetPostResponse | null = null
  categoriesWithPosts.find(category =>
    category.post?.find(pst => {
      if (!pst.regExp) return false
      const regex = RegExp(pst.regExp)
      if (regex.test(fields)) {
        post = { connect: { id: pst.id } }
        return true
      }
      return false
    })
  )
  return post
}

interface ParseCSVToJson {
  data: CSVRabobank[]
  categoriesWithPosts: Category[]
}
const parseCSVToJson = ({ data, categoriesWithPosts }: ParseCSVToJson): PickTransactionExcludeKeyofTransactionId[] => {

  const defaultPost = getPost({ categoriesWithPosts })?.connect.id ?? null
  return data.filter((element: CSVRabobank) => element.Datum)
    .map((item: CSVRabobank) => {
      const description: string[] = [item['Omschrijving-1'], item['Omschrijving-2'], item['Omschrijving-3']]
      const transaction: PickTransactionExcludeKeyofTransactionId = {
        accountNumber: item['IBAN/BBAN'],
        sequenceNumber: Number(item.Volgnr),
        interestDate: dateService.convertDateStringToUTC(item.Rentedatum),
        entryDate: dateService.convertDateStringToUTC(item.Datum),
        currency: item.Munt,
        amount: item.Bedrag ? parseFloat(item.Bedrag.replace(',', '.')) : 0,
        balance: item['Saldo na trn'] ? parseFloat(item['Saldo na trn'].replace(',', '.')) : 0,
        toFromName: item['Naam tegenpartij'],
        toFromAccountNumber: item['Tegenrekening IBAN/BBAN'],
        description: description.map(desc => desc.replaceAll('^\'|\'$', '')).join(' ').trim(),
        note: null,
        error: false,
        errorDescription: null,
        postId: null
      }
      const post = getPost({ transaction, categoriesWithPosts })
      const postId = post?.connect.id ?? defaultPost
      return { ...transaction, postId }
    })
}

export const ImportPage: FC = () => {

  const { t } = useTranslation()
  const title = 'Importeer nieuwe transacties'
  useEffect(() => { document.title = title })

  const categoriesLoading = useStoreState((state) => state.categories.loading)
  const categoriesWithPosts = useStoreState((state) => state.categories.categoriesWithPosts)

  const [transactions, setTransactions] = useState<Transaction[]>([])
  const [imported, setImported] = useState(false)

  // callback function for parse, this is used to load transactions in state
  const complete = (result: ParseResult<unknown>) => {
    if (categoriesWithPosts) {
      const errors = result.errors.map(err => err.row)
      const data = result.data.filter((_undefined, index) => !errors.includes(index)) as CSVRabobank[]
      const newTransactions = parseCSVToJson({ data, categoriesWithPosts })

      transactionApi.createManyTransactions({ pickTransactionExcludeKeyofTransactionId: newTransactions })
        .then(response => {
          setTransactions(response)
          setImported(true)
        })
    }
  }

  // parse the file
  const onChangeRabo = (event: ChangeEvent<HTMLInputElement>) => {
    event.target.files?.[0] && parse(event.target.files[0], { header: true, complete })
  }
  const onChangeASN = (event: ChangeEvent<HTMLInputElement>) => {
    const columnNamesForHeader = [
      'Datum',
      'IBAN/BBAN',
      'Tegenrekening IBAN/BBAN',
      'Naam tegenpartij',
      'Adres',
      'Postcode',
      'Plaats',
      'Valutasoort rekening',
      'Saldo rekening voor mutatie',
      'Munt',
      'Bedrag',
      'Journaaldatum',
      'Rentedatum',
      'Interne transactiecode',
      'Globale transactiecode',
      'Volgnr',
      'Omschrijving-1',
      'Omschrijving-2',
      'Afschriftnummer'
    ]
    if (event.target.files?.[0]) {

      event.target.files[0].text().then(content => {
        parse(`${columnNamesForHeader.join(',')}\r\n${content}`, { header: true, complete })
      })
    }
  }
  return (
    <MainPage title={title}>
      <Container maxWidth="lg">
        {(categoriesLoading) && <CircularProgress />}
        {!imported && !categoriesLoading && transactions.length <= 0 &&
          <>
            <input
              style={{ display: 'none' }}
              accept=".csv"
              id="uploadRabobank"
              type="file"
              onChange={onChangeRabo}
            />
            <label htmlFor="uploadRabobank">
              <Button variant="contained" color="primary" component="span">
                Upload rabobank
              </Button>
            </label>
            <input
              style={{ display: 'none' }}
              accept=".csv"
              id="uploadASN"
              type="file"
              onChange={onChangeASN}
            />
            <label htmlFor="uploadASN">
              <Button variant="contained" color="primary" component="span">
                Upload ASN
              </Button>
            </label>
          </>
        }
        {imported &&
          <>
            <p>{t('imported transactions')}</p>
            <ImportResultTable transactions={transactions.filter(tr => !tr.error)} />
            <p>{t('double transactions')}</p>
            <ImportResultTable transactions={transactions.filter(tr => tr.error)} />
          </>
        }
      </Container>
    </MainPage>
  )
}
