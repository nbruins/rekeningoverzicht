import Paper from '@mui/material/Paper'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { PickTransactionExcludeKeyofTransactionId } from '../../openapi-fetch'
import { formatValue } from '../../utils'

interface Props {
  transactions: PickTransactionExcludeKeyofTransactionId[]
}
export const ImportResultTable = ({ transactions }: Props): React.JSX.Element => {
  const { t } = useTranslation()
  return (
    <TableContainer component={Paper}>
      <Table aria-label="table">
        <TableHead>
          <TableRow>
            <TableCell>{t('accountNumber')}</TableCell>
            <TableCell>{t('sequenceNumber')}</TableCell>
            <TableCell>{t('entryDate')}</TableCell>
            <TableCell>{t('amount')}</TableCell>
            <TableCell>{t('balance')}</TableCell>
            <TableCell>{t('toFromName')}</TableCell>
            <TableCell>{t('toFromAccountNumber')}</TableCell>
            <TableCell>{t('description')}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {transactions.map((transaction) => (
            <TableRow key={transaction.sequenceNumber} color={transaction.error ? 'red' : 'blue'}>
              <TableCell>{transaction.accountNumber}</TableCell>
              <TableCell>{transaction.sequenceNumber}</TableCell>
              <TableCell>{formatValue(transaction.entryDate, 'date')}</TableCell>
              <TableCell>{transaction.amount}</TableCell>
              <TableCell>{transaction.balance}</TableCell>
              <TableCell>{transaction.toFromName}</TableCell>
              <TableCell>{transaction.toFromAccountNumber}</TableCell>
              <TableCell>{transaction.description}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
