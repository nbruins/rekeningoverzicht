import React from 'react'
import { render, screen } from '@testing-library/react'
import { ImportResultTable } from '../ImportResultTable'
import { Transaction } from '../../../openapi-fetch'

describe('ImportResultTable tests', () => {

  const transactions: Transaction[] = [
    {
      id: 1,
      sequenceNumber: 1,
      accountNumber: 'NL66RABO0396868320',
      currency: 'EUR',
      amount: -66.3,
      balance: 1668,
      entryDate: new Date('2020-12-31T00:00:00.000Z'),
      interestDate: new Date('2021-01-01T00:00:00.000Z'),
      toFromAccountNumber: '1391942248',
      toFromName: '',
      description: 'RENTE LENING ',
      note: null,
      error: true,
      errorDescription: 'double',
      postId: null
    },
    {
      id: 2,
      sequenceNumber: 2,
      accountNumber: 'NL66RABO0396868320',
      currency: 'EUR',
      amount: -354.24,
      balance: 1313.76,
      entryDate: new Date('2020-12-31T00:00:00.000Z'),
      interestDate: new Date('2021-01-01T00:00:00.000Z'),
      toFromAccountNumber: '1391942442',
      toFromName: '',
      description: 'RENTE + AFLOSSING LENING ',
      note: null,
      error: true,
      errorDescription: 'double',
      postId: null
    },
    {
      id: 3,
      sequenceNumber: 3,
      accountNumber: 'NL66RABO0396868320',
      currency: 'EUR',
      amount: -444.67,
      balance: 869.09,
      entryDate: new Date('2020-12-31T00:00:00.000Z'),
      interestDate: new Date('2021-01-01T00:00:00.000Z'),
      toFromAccountNumber: '1391942221',
      toFromName: '',
      description: 'RENTE LENING ',
      note: null,
      error: false,
      errorDescription: null,
      postId: null
    },
    {
      id: 4,
      sequenceNumber: 4,
      accountNumber: 'NL66RABO0396868320',
      currency: 'EUR',
      amount: -5,
      balance: 864.09,
      entryDate: new Date('2020-12-31T00:00:00.000Z'),
      interestDate: new Date('2021-01-01T00:00:00.000Z'),
      toFromAccountNumber: 'NL84RABO03289212365',
      toFromName: 'J. Janssen',
      description: 'Cadeautje ',
      note: null,
      error: false,
      errorDescription: null,
      postId: null
    }
  ]

  it('should render the ImportResultTable', () => {

    render(<ImportResultTable transactions={transactions} />)
    expect(screen.getByText('accountNumber')).toBeInTheDocument()
    expect(screen.getByText('RENTE + AFLOSSING LENING')).toBeInTheDocument()
  })

})
