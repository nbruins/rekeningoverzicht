import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import fetchMock from 'fetch-mock-jest'
import * as fs from 'fs'
import path from 'path'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { setTestStore, transactions } from '../../../utils'
import { ImportPage } from '../ImportPage'

describe('ImportPage test', () => {

  const store = setTestStore()

  it('should render the ImportPage with no Errors', () => {

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <ImportPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(screen.getByText('Importeer nieuwe transacties')).toBeDefined()
  })

  it('should upload a import file', async () => {

    const importFile = fs.readFileSync(path.resolve(__dirname, '../../../../../server/example_to_import.csv'), { encoding: 'utf8' })
    const file = new File([importFile], 'import.csv', { type: 'text/csv' })

    fetchMock
      .post('path:/api/transactions', transactions)

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <ImportPage />
        </BrowserRouter>
      </StoreProvider >
    )

    await waitFor(() => { expect(screen.getByLabelText('Upload rabobank')).toBeDefined(); })

    const input = screen.getByLabelText('Upload rabobank')
    expect(input).toBeDefined()
    userEvent.upload(input, file)

    expect(await screen.findByText(/imported transactions/i)).toBeInTheDocument()

  })

})
