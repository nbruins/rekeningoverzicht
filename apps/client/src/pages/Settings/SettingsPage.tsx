import { CircularProgress } from '@mui/material'
import Grid from '@mui/material/Grid2'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { MainPage } from '../../components'
import { useStoreState } from '../../state/typedHooks'
import { AddNewCategory } from './AddNewCategory'
import { CategoryList } from './CategoryList'

export const SettingsPage = () => {

  const { t } = useTranslation()
  const title = t('titles.configuration')
  useEffect(() => { document.title = title })

  const loading = useStoreState((state) => state.categories.loading)
  const categories = useStoreState((state) => state.categories.categoriesWithPosts)

  return (
    <MainPage title={title}>
      <main>
        <Grid
          container
          direction="column"
          justifyContent="space-between"
          alignItems="flex-start"
        >
          {loading && <Grid><CircularProgress /></Grid>}
          {(!categories && !loading) && <Grid>{t('no categories found')}</Grid>}
          {categories &&
            <>
              <Grid>
                <AddNewCategory />
              </Grid>
              <Grid>
                <CategoryList categoriesAndPosts={categories} />
              </Grid>
            </>
          }
        </Grid>
      </main>
    </MainPage>
  )
}
