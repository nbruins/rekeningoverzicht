import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import TextField from '@mui/material/TextField'
import React, { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { PickPostTypeExcludeKeyofPostTypeId } from '../../openapi-fetch'
import { useStoreActions } from '../../state/typedHooks'
import { IconButton } from '@mui/material'
import { Add } from '@mui/icons-material'

const defaultNewPost: PickPostTypeExcludeKeyofPostTypeId = {
  name: '',
  categoryId: 1,
  regExp: ''
}

export const AddNewPost: FC<{ categoryId: number }> = ({ categoryId }) => {

  const { t } = useTranslation()

  const addPost = useStoreActions((actions) => actions.categories.addPost)

  const [open, setOpen] = React.useState(false)
  const toggleOpen = () => { setOpen(!open) }

  const [newPost, setNewPost] = useState(defaultNewPost)

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {

    const name = event.target.id
    const value = event.target.value

    setNewPost({ ...newPost, [name]: value })
  }

  const handleAddButton = () => {
    if (newPost.name.length > 0) {
      addPost({ ...newPost, categoryId })
      setNewPost(defaultNewPost)
      toggleOpen()
    }
  }

  return (
    <div>
      <IconButton edge="end" aria-label={t('add')} onClick={toggleOpen} value={categoryId} size="large">
        <Add />
      </IconButton>
      <Dialog open={open} onClose={toggleOpen} aria-labelledby="add-Post">
        <DialogTitle id="add-Post">{t('add Post')}</DialogTitle>
        <DialogContent>
          <TextField required autoFocus fullWidth margin="dense"
            id="name"
            label={t('Post')}
            value={newPost.name}
            onChange={handleOnChange} />
          <TextField required fullWidth margin="dense" type="number"
            id="regex"
            label={t('regex')}
            value={newPost.regExp}
            onChange={handleOnChange} />
        </DialogContent>
        <DialogActions>
          <Button onClick={toggleOpen} color="primary">Cancel</Button>
          <Button onClick={handleAddButton} color="primary">{t('add Post')}</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
