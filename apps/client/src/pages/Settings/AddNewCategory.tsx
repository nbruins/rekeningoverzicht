import { Box, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import TextField from '@mui/material/TextField'
import React, { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { CategoryIcon } from '../../components'
import { PickCategoryExcludeKeyofCategoryIdOrPost } from '../../openapi-fetch'
import { useStoreActions } from '../../state/typedHooks'
import { standardIcons } from '../../utils'

const defaultNewCategory: PickCategoryExcludeKeyofCategoryIdOrPost = {
  name: '',
  gemmiddeldbedragNibud: 0,
  backgroundColor: '#ffffff',
  color: '#000000',
  borderColor: '#000000',
  icon: '',
  type: 'UITGAVEN'
}

export const AddNewCategory: FC = () => {

  const { t } = useTranslation()

  const addCategory = useStoreActions((actions) => actions.categories.addCategory)

  const [open, setOpen] = React.useState(false)
  const toggleOpen = () => { setOpen(!open) }

  const [newCategory, setNewCategory] = useState(defaultNewCategory)

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {

    const name = event.target.id
    const value = (name === 'gemmiddeldbedragNibud') ? parseInt(event.target.value, 10) : event.target.value

    setNewCategory({ ...newCategory, [name]: value })
  }

  const handleChangeIcon = (event: SelectChangeEvent) => {
    setNewCategory({ ...newCategory, icon: event.target.value })
  }

  const handleAddButton = () => {
    if (newCategory.name.length > 0) {
      addCategory(newCategory)
      setNewCategory(defaultNewCategory)
      toggleOpen()
    }
  }

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={toggleOpen}>
        {t('add category')}
      </Button>
      <Dialog open={open} onClose={toggleOpen} aria-labelledby="add-category">
        <DialogTitle id="add-category">{t('add category')}</DialogTitle>
        <DialogContent>
          <TextField required autoFocus fullWidth margin="dense"
            id="name"
            label={t('category')}
            value={newCategory.name}
            onChange={handleOnChange} />
          <TextField required fullWidth margin="dense" type="number"
            id="gemmiddeldbedragNibud"
            label={t('average amount')}
            value={newCategory.gemmiddeldbedragNibud ?? 0}
            onChange={handleOnChange} />
          <TextField required fullWidth margin="dense" type="color"
            id="color"
            label={t('color')}
            value={newCategory.color}
            onChange={handleOnChange} />
          <TextField required fullWidth margin="dense" type="color"
            id="backgroundColor"
            label={t('backgroundColor')}
            value={newCategory.backgroundColor}
            onChange={handleOnChange} />
          <Box sx={{ display: 'flex', flexDirection: 'row' }}>
            <CategoryIcon category={newCategory} />
            <FormControl className={'formControlSelect'}>
              <InputLabel id="icon" shrink focused>Icon</InputLabel>
              <Select
                className={'selectFilter'}
                labelId="icon"
                id="icon"
                value={newCategory.icon ?? ''}
                onChange={handleChangeIcon}
              >
                {Object.keys(standardIcons).map((value) => (
                  <MenuItem key={value} value={value}>{value}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={toggleOpen} color="primary">Cancel</Button>
          <Button onClick={handleAddButton} color="primary">{t('add category')}</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
