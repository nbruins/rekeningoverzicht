import { ArrowRight, Cancel, Edit, Save } from '@mui/icons-material'
import { CircularProgress, IconButton, List, ListItemButton, ListItemIcon, ListItemText, TextField } from '@mui/material'
import React, { MouseEvent } from 'react'
import { Post } from '../../openapi-fetch'
import { useStoreActions, useStoreState } from '../../state/typedHooks'


interface PostListProps {
  posts: Post[]
}
export const PostList = ({ posts }: PostListProps) => {

  const [editPost, setEditPost] = React.useState<Post | undefined>()

  const loading = useStoreState((state) => state.categories.loading)
  const updatePost = useStoreActions((actions) => actions.categories.updatePost)

  const handleEditPostClick = (event: MouseEvent<{ value: string }>) => {
    const rawId = event.currentTarget.value
    if (rawId) {
      const id = parseInt(rawId, 10)
      setEditPost(posts.find(post => post.id === id))
    }
  }
  const handleChangePost = (event: React.ChangeEvent<{ value: string }>) =>
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    editPost && setEditPost({ ...editPost, name: event.target.value })

  const handleChangePostRegExp = (event: React.ChangeEvent<{ value: string }>) =>
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    editPost && setEditPost({ ...editPost, regExp: event.target.value })

  const savePost = () => {

    if (editPost) {
      void updatePost(editPost)
      setEditPost(undefined)
    }
  }

  return <>
    {loading && <CircularProgress />}
    <List component="div" disablePadding>
      {posts.map((post) => (
        <ListItemButton key={post.name}>
          <ListItemIcon>
            <ArrowRight />
          </ListItemIcon>
          {editPost && editPost.id === post.id ?
            <div style={{ width: '100%' }}>
              <TextField
                required
                fullWidth
                id="updatePost"
                label="Post"
                value={editPost.name}
                onChange={handleChangePost}
              />
              <TextField
                fullWidth
                id="updateRegExp"
                label="Zoekvraag"
                value={editPost.regExp}
                onChange={handleChangePostRegExp}
              />
            </div> :
            <ListItemText primary={post.name} secondary={post.regExp} />
          }
          <ListItemIcon>
            {editPost && editPost.id === post.id ?
              <>
                <IconButton edge="end" aria-label="save" onClick={savePost} size="large">
                  <Save />
                </IconButton>
                <IconButton
                  edge="end"
                  aria-label="cancel"
                  onClick={() => { setEditPost(undefined) }}
                  size="large">
                  <Cancel />
                </IconButton>
              </>
              :
              <IconButton
                value={post.id}
                name={post.name}
                edge="end"
                aria-label="edit"
                onClick={handleEditPostClick}
                size="large">
                <Edit />
              </IconButton>
            }
          </ListItemIcon>
        </ListItemButton>
      ))}
    </List>
  </>
}
