import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, TextField } from '@mui/material'
import React, { ChangeEvent } from 'react'
import { useTranslation } from 'react-i18next'
import { Category } from '../../openapi-fetch'
import { standardIcons } from '../../utils'

interface ECProps {
  editCategory?: Category,
  updateCategory: (category?: Category) => void
}
export const EditCategory = ({ editCategory, updateCategory }: ECProps): React.JSX.Element => {

  const { t } = useTranslation()

  const handleChangeCategory = (event: ChangeEvent<{ value: string }>) => {
    editCategory && updateCategory({ ...editCategory, name: event.target.value })
  }

  const handleChangeNibud = (event: React.ChangeEvent<{ value: string }>) => {
    editCategory && updateCategory({ ...editCategory, gemmiddeldbedragNibud: parseInt(event.target.value, 10) })
  }

  const handleChangeIcon = (event: SelectChangeEvent) => {
    editCategory && updateCategory({ ...editCategory, icon: event.target.value ?? '' })
  }

  return (
    <div style={{ width: '100%' }}>
      <TextField required fullWidth
        id="updateCategory"
        label={t('category')}
        value={editCategory?.name}
        onChange={handleChangeCategory}
      />
      <TextField required fullWidth
        id="updateNibud"
        label={t('average amount')}
        value={editCategory?.gemmiddeldbedragNibud}
        onChange={handleChangeNibud}
      />
      <FormControl className={'formControlSelect'}>
        <InputLabel id="icon" shrink focused>Icon</InputLabel>
        <Select
          className={'selectFilter'}
          labelId="icon"
          id="iconSelect"
          value={editCategory?.icon ?? ''}
          onChange={handleChangeIcon}
        >
          {Object.keys(standardIcons).map((value) => (
            <MenuItem key={value} value={value}>{value}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  )
}
