import { Cancel, Edit, ExpandLess, ExpandMore, Save } from '@mui/icons-material'
import {
  Collapse,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  Typography
} from '@mui/material'
import React, { MouseEvent, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { CategoryIcon } from '../../components'
import {
  Category
} from '../../openapi-fetch'
import { useStoreActions } from '../../state/typedHooks'
import { autoFormatValue } from '../../utils'
import { AddNewPost } from './AddNewPost'
import { EditCategory } from './EditCategory'
import { PostList } from './PostList'

type OpenedCategories = Record<string, boolean>

interface CategoryListProps {
  categoriesAndPosts: Category[]
}

export const CategoryList = ({ categoriesAndPosts }: CategoryListProps): React.JSX.Element => {

  const { t } = useTranslation()

  const updateCategory = useStoreActions((actions) => actions.categories.updateCategory)

  const [open, setOpen] = useState<OpenedCategories>({})
  const toggleOpenCategory = (name: string) => { setOpen({ ...open, [name]: !open[name] }) }

  const [editCategory, setEditCategory] = useState<Category | undefined>()

  const handleEditCategoryClick = (event: MouseEvent<{ value: string }>) => {
    const rawId = event.currentTarget.value
    if (rawId) {
      const id = parseInt(rawId, 10)
      setEditCategory(categoriesAndPosts.find(category => category.id === id))
    }
  }

  const cancelEdit = () => { setEditCategory(undefined) }
  const saveCategory = () => {
    if (editCategory) {
      updateCategory(editCategory)
      setEditCategory(undefined)
    }
  }
  return (
    <List disablePadding
      component="nav"
      aria-labelledby="category-list"
      subheader={
        <ListSubheader component="div" id="category-list">
          {t('categories and posts')}
        </ListSubheader>
      }
    >
      {categoriesAndPosts.map(category => (
        <div key={category.name}>
          <ListItemButton>
            <ListItemIcon><CategoryIcon category={category} /></ListItemIcon>
            {editCategory?.id === category.id ?
              <EditCategory editCategory={editCategory} updateCategory={setEditCategory} />
              :
              <ListItemText primary={category.name}
                secondary={
                  <Typography component="span" variant="body2" color="textPrimary">
                    {t('average amount')}: {autoFormatValue(category.gemmiddeldbedragNibud ?? undefined)} {t('type')}: {category.type}
                  </Typography>
                }
              />
            }
            <ListItemIcon>
              {editCategory?.id === category.id ?
                <>
                  <IconButton edge="end" aria-label={t('edit')} onClick={saveCategory} size="large"><Save /></IconButton>
                  <IconButton edge="end" aria-label={t('cancel')} onClick={cancelEdit} size="large"><Cancel /></IconButton>
                </>
                :
                <>
                  <IconButton edge="end" aria-label={t('edit')} onClick={handleEditCategoryClick} value={category.id} size="large"><Edit /></IconButton>
                  <AddNewPost categoryId={category.id} />
                </>
              }
            </ListItemIcon>
            {open[category.name] ?
              <ExpandLess onClick={() => { toggleOpenCategory(category.name) }} aria-label={`close ${category.name}`} />
              : <ExpandMore onClick={() => { toggleOpenCategory(category.name) }} aria-label={`open ${category.name}`} />
            }
          </ListItemButton>
          {category.post &&
            <Collapse in={open[category.name]} timeout="auto" unmountOnExit>
              <PostList posts={category.post} />
            </Collapse>
          }
        </div>
      ))}
    </List>
  )
}
