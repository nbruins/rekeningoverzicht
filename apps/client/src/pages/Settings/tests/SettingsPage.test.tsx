import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { SettingsPage } from '..'
import { setTestStore } from '../../../utils'

describe('ConfigurationPage test', () => {

  const store = setTestStore({ setDefault: ['categories'] })

  it('should render the ConfigurationPage with no Errors', async () => {

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <SettingsPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(await screen.findByText('titles.configuration')).toBeInTheDocument()
    expect(await screen.findByText(/Abonementen en telecom/)).toBeInTheDocument()
  })
})
