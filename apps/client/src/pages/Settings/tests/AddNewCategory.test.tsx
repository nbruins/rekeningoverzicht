import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { setTestStore } from '../../../utils'
import { AddNewCategory } from '../AddNewCategory'


describe('AddNewCategory tests', () => {

  const store = setTestStore({ setDefault: ['categories'] })

  it('should render the AddNewCategory', async () => {

    render(
      <StoreProvider store={store} >
        <AddNewCategory />
      </StoreProvider>
    )

    userEvent.click(screen.getByText('add category'))

    await waitFor(() => { expect(screen.getByLabelText(/average amount/)).toBeDefined() })

    expect(screen.getByLabelText(/color/)).toBeDefined()
    expect(screen.getByLabelText(/backgroundColor/)).toBeDefined()

    userEvent.type(screen.getByLabelText(/average amount/), '200')
  })

})
