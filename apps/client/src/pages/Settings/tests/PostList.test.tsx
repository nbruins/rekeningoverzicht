import '@testing-library/jest-dom'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { categoriesWithPosts, setTestStore } from '../../../utils'
import { PostList } from '../PostList'

describe('PostList tests', () => {

  const store = setTestStore()

  store.getActions().categories.setCategoriesWithPosts(categoriesWithPosts)

  it('should render the PostList', async () => {

    render(
      <StoreProvider store={store} >
        <PostList posts={categoriesWithPosts[0].post} />
      </StoreProvider>
    )

    expect(screen.getByText('Cursus/Opleiding')).toBeDefined()

    userEvent.click(screen.getAllByLabelText('edit')[0])
    await waitFor(() => { expect(screen.getByLabelText('cancel')).toBeDefined(); })
    userEvent.click(screen.getByLabelText('cancel'))

  })

  it('should render the PostList and edit post', async () => {

    render(
      <StoreProvider store={store} >
        <PostList posts={categoriesWithPosts[0].post} />
      </StoreProvider>
    )

    // open the first post
    userEvent.click(screen.getAllByLabelText('edit')[0])

    // edit the post
    await waitFor(() => { expect(screen.getByLabelText(/Post/i)).toBeDefined(); })
    userEvent.type(screen.getByLabelText(/Post/i), ' New edit')
    await waitFor(() => { expect(screen.getByLabelText(/Post/i)).toHaveValue('Abonnement New edit'); })

    // edit the regexp
    const regExpInput = screen.getByLabelText('Zoekvraag')
    // first empty value
    fireEvent.change(regExpInput, { target: { value: '' } })

    userEvent.type(regExpInput, 'update of regexp')
    await waitFor(() => { expect(regExpInput).toHaveValue('update of regexp'); })

    // save the post
    // userEvent.click(screen.getByLabelText('save'))

    // await waitFor(() => expect(screen.queryByLabelText('save')).toBeNull())

    // check if there was an error
    // expect(screen.getByRole('progressbar')).toBeDefined()
    // await waitFor(() => expect(screen.queryByRole('progressbar')).toBeNull())
    // expect(screen.queryByText('Error')).toBeNull()

  })

})
