import { render, screen, waitForElementToBeRemoved } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { categoriesWithPosts, setTestStore } from '../../../utils'
import { CategoryList } from '../CategoryList'

describe('CategoryList tests', () => {

  it('should render the CategoryList', async () => {
    const store = setTestStore()

    render(
      <StoreProvider store={store} >
        <CategoryList categoriesAndPosts={categoriesWithPosts} />
      </StoreProvider>
    )

    expect(screen.queryByText('categories and posts')).toBeInTheDocument()
    expect(screen.queryByText('Cursus/Opleiding')).toBeNull()

    userEvent.click(await screen.findByLabelText('open Abonementen en telecom'))

    expect(screen.findByText('Cursus/Opleiding')).toBeDefined()

    userEvent.click((await screen.findAllByLabelText('edit'))[0])

    expect(await screen.findByLabelText(/category/)).toBeInTheDocument()

    userEvent.click(await screen.findByLabelText('cancel'))

    await waitForElementToBeRemoved(() => screen.queryByLabelText('cancel'))
  })
})
