import React, { useEffect } from 'react'
import { Card, CardContent, Container, Typography } from '@mui/material'
import { MainPage } from '../components'
import { useTranslation } from 'react-i18next'

export const AboutPage = (): React.JSX.Element => {

  const { t } = useTranslation()
  const title = t('titles.about')
  useEffect(() => { document.title = title })

  return (
    <MainPage title={title}>
      <Container maxWidth="lg" className={'container'}>
        <Card variant="outlined">
          <CardContent>
            <Typography component="h1" variant={'h3'}>Rekeningoverzicht</Typography>
            <Typography variant="body2" component="p">
              Met dit programma kan een export van de Rabobank worden ingelezen.
              Vervolgens kunnen de transacties worden gecatogoriseerd
            </Typography>
            <Typography variant="body2" component="p">
              Versie: 1.0
            </Typography>
            <Typography variant="body2" component="p">
              Licentie: MIT
            </Typography>
          </CardContent>
        </Card>
      </Container>
    </MainPage>
  )
}
