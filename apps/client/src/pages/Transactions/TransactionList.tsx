import { Add, ArrowRightAlt, Error, Remove, Schedule } from '@mui/icons-material'
import { Box, CircularProgress, styled, TextField, Typography } from '@mui/material'
import React, { FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { FixedSizeList, ListChildComponentProps } from 'react-window'
import { CategoryIcon } from '../../components'
import { useWindowDimensions } from '../../hooks'
import { formatValue, getTotals } from '../../utils'
import { TransactionDetails } from './TransactionDetails'
import { UpdateTransactionForm } from './UpdateTransactionForm'
import { TransactionWithPostAndCategory } from '../../openapi-fetch'
import { useStoreState } from '../../state/typedHooks'
import Grid from '@mui/material/Grid2'

const filteredList = (searchFor: null | string, transactions: TransactionWithPostAndCategory[]): TransactionWithPostAndCategory[] => {
  if (!searchFor) return transactions

  return transactions.filter(transactionRaw => {
    const transaction = JSON.stringify(transactionRaw)
    return transaction.toUpperCase().includes(searchFor.toUpperCase())
  })
}

const SummaryRow = styled('div')({ display: 'flex', flexDirection: 'column' })

interface Props {
  transactions: TransactionWithPostAndCategory[],
  loading: boolean
}
export const TransactionList: FC<Props> = ({ loading, transactions }) => {

  const { t } = useTranslation()
  const { height, width } = useWindowDimensions()
  const listWidth = (width > 720) ? 720 : width - 16
  const [search, setSearch] = useState<string | null>(null)
  const [localTransactions, setLocalTransactions] = useState(transactions)
  const selectedTransactionIds = useStoreState((state) => state.transactions.selectedTransactionIds)

  useEffect(() => {
    setLocalTransactions(filteredList(search, transactions))
  }, [transactions, search])

  const lastTransaction = localTransactions[0]
  const dateLastTransaction = formatValue(lastTransaction?.interestDate, 'date')
  const { plus, min, total } = getTotals({ transactions: localTransactions })

  const updateTransactionForm = (transaction: TransactionWithPostAndCategory) => (
    <UpdateTransactionForm
      transactionIds={[transaction.id]}
      transactionPostName={transaction.post?.name}
      transactionNote={transaction.note ?? ''}
    >
      <CategoryIcon category={transaction.post?.category} />
    </UpdateTransactionForm>
  )

  const rowRenderer = ({ index, style }: ListChildComponentProps) => (
    <TransactionDetails style={style} transaction={localTransactions[index]} updateTransactionForm={updateTransactionForm} />)

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: listWidth }}>
      <Grid container spacing={2}>
        <Grid size={{ xs: 12, sm: 8 }}>
          {!loading && localTransactions.length > 0 &&
            <SummaryRow>
              <Typography variant={'caption'} sx={{ alignItems: 'first baseline', display: 'inline-flex' }}>
                <Schedule fontSize="small" />{dateLastTransaction}, {localTransactions.length}
                <Add fontSize="small" /> {formatValue(plus, 'currency')}
                <Remove fontSize="small" />{formatValue(min, 'currency')}
                <ArrowRightAlt fontSize="small" /> {formatValue(total, 'currency')}
              </Typography>
              {selectedTransactionIds.size > 0 &&
                <UpdateTransactionForm transactionIds={Array.from(selectedTransactionIds)}>
                  {t('update post')}
                </UpdateTransactionForm>
              }
            </SummaryRow>
          }
        </Grid>
        <Grid size={{ xs: 12, sm: 4 }} alignItems="center">
          <TextField id="text-search" label="Search" size="small"
            sx={{ marginLeft: '4px' }}
            onChange={(event) => { setSearch(event.target.value) }}
          />
        </Grid>
        <Grid size={{ xs: 12 }}>
          {(localTransactions.findIndex(transaction => transaction.error) >= 0) &&
            <p>
              Er zijn fouten gevonden. zie <Error /> voor details over de fout. Deze transacties worden niet geïmporteerd
            </p>
          }
        </Grid>
      </Grid>
      {loading && <CircularProgress />}
      {!loading && localTransactions.length === 0 && <h1>Er zijn geen transacties gevonden</h1>}
      {!loading && localTransactions.length > 0 &&
        <FixedSizeList
          width={listWidth}
          height={height - 160}
          itemSize={120}
          itemCount={localTransactions.length}
        >
          {rowRenderer}
        </FixedSizeList>
      }
    </Box>
  )
}
