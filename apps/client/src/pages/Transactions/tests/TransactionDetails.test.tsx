import { createTheme, ThemeProvider } from '@mui/material/styles'
import '@testing-library/jest-dom'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { TransactionDetails } from '..'
import { PostWithCategory, TransactionWithPostAndCategory } from '../../../openapi-fetch'
import { setTestStore } from '../../../utils'


describe('TransactionDetails tests', () => {

  const theme = createTheme()
  // configure store
  const store = setTestStore({ setDefault: ['filters'] })

  const basicTransaction: TransactionWithPostAndCategory = {
    id: 4,
    sequenceNumber: 4,
    accountNumber: 'NL66RABO0396868320',
    currency: 'EUR',
    amount: -5,
    balance: 864.09,
    entryDate: new Date('2020-12-31T00:00:00.000Z'),
    interestDate: new Date('2021-01-01T00:00:00.000Z'),
    toFromAccountNumber: 'NL84RABO03289212365',
    toFromName: 'J. Janssen',
    description: 'Cadeautje ',
    note: null,
    error: false,
    errorDescription: null,
    postId: null,
    post: null
  }
  const post: PostWithCategory = {
    id: 1,
    name: 'Groceries',
    regExp: 'AH',
    categoryId: 1,
    category: {
      id: 1,
      name: 'Household',
      icon: 'localGroceryStore',
      color: '#ffffff',
      backgroundColor: '#df7401',
      borderColor: '#df7401',
      gemmiddeldbedragNibud: 126,
      type: 'UITGAVEN'
    }
  }

  it('should render the TransactionDetails of a transaction without post', () => {
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={basicTransaction} />
        </StoreProvider>
      </ThemeProvider>
    )
    expect(screen.getByText('J. Janssen')).toBeDefined()
    expect(screen.getByLabelText('Onbekend')).toBeDefined()
  })

  it('should render the TransactionDetails of a transaction with an error', () => {
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={{ ...basicTransaction, error: true, errorDescription: 'TheError' }} />
        </StoreProvider>
      </ThemeProvider>
    )
    expect(screen.getByLabelText('TheError')).toBeDefined()
  })

  it('should render the TransactionDetails of a transaction with an error without desscription', () => {
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={{ ...basicTransaction, error: true }} />
        </StoreProvider>
      </ThemeProvider>
    )
    expect(screen.getByLabelText('Unknown error')).toBeDefined()
  })

  it('should render the TransactionDetails of a transaction with a Post', () => {
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={{ ...basicTransaction, post }} />
        </StoreProvider>
      </ThemeProvider>
    )
    expect(screen.getByLabelText('Household')).toBeDefined()
  })

  it('should render the TransactionDetails of a transaction without toFromName', () => {
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={{ ...basicTransaction, post, toFromName: null }} />
        </StoreProvider>
      </ThemeProvider>
    )
    expect(screen.getByLabelText('Household')).toBeDefined()
  })

  it('should render the TransactionDetails, details on click', async () => {
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={{ ...basicTransaction, balance: -100, amount: 5, post }} />
        </StoreProvider>
      </ThemeProvider>
    )
    expect(screen.queryByText('rekeningnummer')).toBeNull()
    userEvent.click(screen.getByTestId('ExpandMoreIcon'))
    await waitFor(() => { expect(screen.getByText('rekeningnummer: NL84RABO03289212365')).toBeDefined(); })
  })

  it('should render the TransactionDetails, updateform onclick', () => {
    const updateForm = (transaction: TransactionWithPostAndCategory) => <p>Update form {transaction.id}</p>
    render(
      <ThemeProvider theme={theme}>
        <StoreProvider store={store}>
          <TransactionDetails transaction={{ ...basicTransaction, post }} updateTransactionForm={updateForm} />
        </StoreProvider>
      </ThemeProvider>
    )

    expect(screen.getByText('Update form 4')).toBeDefined()
  })
})
