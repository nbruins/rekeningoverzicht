import { createTheme, ThemeProvider } from '@mui/material/styles'
import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { TransactionList } from '..'
import { setTestStore, transactions } from '../../../utils'

describe('TransactionList tests', () => {

  const theme = createTheme()

  const store = setTestStore({ setDefault: ['filters'] })

  it('should render the TransactionList in state loading', () => {

    render(
      <StoreProvider store={store}>
        <ThemeProvider theme={theme}>
          <TransactionList loading={true} transactions={[]} />
        </ThemeProvider>
      </StoreProvider>
    )

    expect(screen.getByRole('progressbar')).toBeDefined()
  })

  it('should render the TransactionList', () => {

    render(
      <StoreProvider store={store}>
        <ThemeProvider theme={theme}>
          <TransactionList loading={false} transactions={transactions} />
        </ThemeProvider>
      </StoreProvider>

    )
    expect(screen.getByText('Albert Heijn 1472 LOSSER')).toBeDefined()
  })

  it('should render the TransactionList without transactions', () => {

    render(
      <StoreProvider store={store}>
        <ThemeProvider theme={theme}>
          <TransactionList loading={false} transactions={[]} />
        </ThemeProvider>
      </StoreProvider>
    )

    expect(screen.getByText('Er zijn geen transacties gevonden')).toBeDefined()
  })
})
