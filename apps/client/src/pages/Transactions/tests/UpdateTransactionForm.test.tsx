import '@testing-library/jest-dom'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { setTestStore, transactions } from '../../../utils'
import { UpdateTransactionForm } from '../UpdateTransactionForm'

const transaction = transactions[0]

describe('UpdateTransactionForm tests', () => {

  it('should render the UpdateTransactionForm and click the icon', async () => {
    const store = setTestStore()
    render(
      <StoreProvider store={store} >
        <UpdateTransactionForm
          transactionIds={[transaction.id]}
          transactionPostName={transaction.post?.name}
          transactionNote={transaction.note ?? ''}
        >
          <p>Icon</p>
        </UpdateTransactionForm>
      </StoreProvider>
    )

    userEvent.click(screen.getByRole('button', { name: 'Icon' }))

    await waitFor(() => { expect(screen.getByText('Categorie aanpassen')).toBeDefined(); })

  })

})
