import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import fetchMock from 'fetch-mock-jest'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { setTestStore, transactions } from '../../../utils'
import { TransactionsPage } from '../TransactionsPage'

describe('TransactionsPage test', () => {

  const store = setTestStore({ setDefault: ['categories', 'filters'] })

  it('should render the TransactionsPage with no Errors', async () => {

    fetchMock.get('path:/api/transactions', transactions)
    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <TransactionsPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(await screen.findByText('titles.accountStatement')).toBeInTheDocument()

    fetchMock.mockReset()
  })

})
