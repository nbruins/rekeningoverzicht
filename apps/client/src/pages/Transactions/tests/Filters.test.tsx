import { createTheme, StyledEngineProvider, ThemeProvider } from '@mui/material/styles'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFnsV3'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { setTestStore } from '../../../utils'
import { Filters } from '../Filters'

describe('Filters tests', () => {

  const theme = createTheme()
  const store = setTestStore({ setDefault: ['categories'] })

  it('should render the Filters and category and post should be clickable', async () => {

    render(
      <StoreProvider store={store} >
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={theme}>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <Filters />
            </LocalizationProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </StoreProvider >
    )

    // Wait until category is loaded
    expect(screen.getByText('Reset')).toBeDefined()
    await waitFor(() => { expect(screen.getByLabelText('Abonementen en telecom')).toBeDefined() })

    // click the post and check if store is updated
    userEvent.click(screen.getByLabelText('Abonnement'))

    // then click the category and check if store is updated
    userEvent.click(screen.getByLabelText('Abonementen en telecom'))

  })

  it('should render the Filters and reset categories and post', async () => {

    render(
      <StoreProvider store={store} >
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={theme}>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <Filters />
            </LocalizationProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </StoreProvider >
    )

    // Wait until category is loaded
    expect(screen.getByLabelText('Abonementen en telecom')).toBeInTheDocument()
    userEvent.click(screen.getByLabelText('Abonementen en telecom'))
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    await waitFor(() => expect(expect(store.getState().transactions.filters.postIds).toHaveLength(7)))

    userEvent.click(screen.getByText('Reset'))

    // variables is not changed in the previous test so after reset it should match
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    await waitFor(() => expect(expect(store.getState().transactions.filters.postIds).toEqual([])))
  })

})
