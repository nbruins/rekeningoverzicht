import {
  Button, Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField
} from '@mui/material'
import React, { ChangeEvent, FC, useState } from 'react'
import { AutoCompletePost, Option } from '../../components'
import { useStoreActions } from '../../state/typedHooks'

interface Props {
  transactionIds: number[],
  transactionPostName?: string,
  transactionNote?: string,
  children: React.ReactNode
}
export const UpdateTransactionForm: FC<Props> = ({ transactionIds, transactionPostName, transactionNote, children }) => {

  const updateNoteAndPostForMultipleTransactions =
    useStoreActions((actions) => actions.transactions.updateNoteAndPostForMultipleTransactions)
  const resetSelectedTransactionIds =
    useStoreActions((actions) => actions.transactions.resetSelectedTransactionIds)

  const [showDialog, setShowDialog] = useState(false)

  const [selectedPost, setSelectedPost] = useState<{ name?: string, id?: number }>({
    name: transactionPostName
  })
  const handleSelectPost = (event: ChangeEvent<unknown>, value: Option | readonly Option[] | null) => {
    if (!Array.isArray(value)) {
      const option = value as Option
      setSelectedPost({ name: option.post, id: option.postId })
    }
  }

  const [note, setNote] = useState<string>(transactionNote ?? '')
  const handleOnChangeNote = (event: React.ChangeEvent<{ value: string }>) => { setNote(event.target.value); }

  const save = () => {

    const postId = selectedPost.id
    if (postId)
      updateNoteAndPostForMultipleTransactions({ note, postId, transactionIds })
    setShowDialog(false)
    resetSelectedTransactionIds(true)
  }

  return (
    <>
      <Button sx={{ border: 'none', background: 'none', color: 'unset' }} onClick={() => { setShowDialog(true); }}>
        {children}
      </Button>
      <form>
        <Dialog
          open={showDialog}
          onClose={() => { setShowDialog(false); }}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            Categorie aanpassen
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Selecteer een categorie
            </DialogContentText>
            <AutoCompletePost handleSelectPost={handleSelectPost} />
            <DialogContentText>
              Notitie
            </DialogContentText>
            <TextField
              id="note"
              label="Notitie"
              multiline
              maxRows="4"
              value={note}
              onChange={handleOnChangeNote}
            />
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={() => { setShowDialog(false); }} color="primary">
              Cancel
            </Button>
            <Button onClick={save} color="primary">
              Wijzigen
            </Button>
          </DialogActions>
        </Dialog>
      </form>
    </>
  )
}
