import { Box, Button } from '@mui/material'
import { DatePicker } from '@mui/x-date-pickers'
import React, { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useStoreActions, useStoreState } from '../../state/typedHooks'
import { dateService } from '../../utils'

export const Filters: FC = () => {

  const { t } = useTranslation()

  const categoriesWithPosts = useStoreState((state) => state.categories.categoriesWithPosts)

  const filters = useStoreState((state) => state.transactions.filters)
  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)

  const accountNumbers = ['NL66RABO0129637718', 'NL84ASNB8844625520']
  const [selectedAccountNumbers, setSelectedAccountNumbers] = useState<string[]>([])
  const handleAccountNumberChange = (accountNmbr: string) => {
    const isSelectedAN = selectedAccountNumbers.includes(accountNmbr)

    if (isSelectedAN) {
      setSelectedAccountNumbers(selectedAccountNumbers.filter(num => num === accountNmbr))
      setFilters({ ...filters, accountNumbers: [... new Set(filters.accountNumbers.filter(num => num === accountNmbr))] })
    } else {
      setSelectedAccountNumbers([...selectedAccountNumbers, accountNmbr])
      setFilters({ ...filters, accountNumbers: [...selectedAccountNumbers, accountNmbr] })
    }
  }


  const [selectedCategories, setSelectedCategories] = useState<number[]>([])
  const handleCategoryChange = (categoryId: number) => {
    const isSelected = selectedCategories.includes(categoryId)
    const postIds = categoriesWithPosts.find(cat => cat.id === categoryId)?.post?.map(post => post.id)
    if (isSelected) {
      setSelectedCategories(selectedCategories.filter(id => id !== categoryId))
      postIds && setFilters({ ...filters, postIds: [... new Set(filters.postIds.filter(id => !postIds.includes(id)))] })
    } else {
      setSelectedCategories([...selectedCategories, categoryId])
      postIds && setFilters({ ...filters, postIds: [... new Set([...filters.postIds, ...postIds])] })
    }
  }

  const handleOnClickPost = (postId: number) => {
    filters.postIds.includes(postId) ?
      setFilters({ ...filters, postIds: filters.postIds ? filters.postIds.filter(id => id !== postId) : [] }) :
      setFilters({ ...filters, postIds: filters.postIds ? [...filters.postIds, postId] : [postId] })
  }
  return (
    <Box sx={{ alignSelf: 'flex-end', padding: '0 12px 0 0' }} width="280px">
      <Box sx={{ marginBottom: '8px' }}>
        <DatePicker
          label={t('AfterEqual')}
          value={filters.dateAfterEqual}
          onChange={date => { date && setFilters({ ...filters, dateAfterEqual: dateService.localDateAsUTC(date) }) }}
          openTo="day"
        />
      </Box>
      <DatePicker
        label={t('BeforeEqual')}
        value={filters.dateBeforeEqual}
        onChange={date => { date && setFilters({ ...filters, dateBeforeEqual: dateService.localDateAsUTC(date) }) }}
        openTo="day"
      />
      <Button
        variant="contained"
        fullWidth
        color={'primary'}
        type={'reset'}
        sx={{ margin: '6px 0 12px 0' }}
        onClick={() => { setFilters({ ...filters, postIds: [] }); }}
      >
        Reset
      </Button>
      {accountNumbers.map(accountNUmber =>
        <Box key={accountNUmber} sx={{ padding: '6px' }}>
          <div>
            <label htmlFor={accountNUmber}>{accountNUmber}</label>
            <input type="checkbox" id={accountNUmber} name={accountNUmber}
              checked={!!selectedAccountNumbers.includes(accountNUmber)}
              onChange={() => { handleAccountNumberChange(accountNUmber); }}
            />
          </div>
        </Box>
      )}
      {categoriesWithPosts.map(category =>
        <Box key={category.id} sx={{ padding: '6px' }}>
          <div>
            <label htmlFor={category.name}>{category.name}</label>
            <input type="checkbox" id={category.name} name={category.name}
              checked={!!selectedCategories.includes(category.id)}
              onChange={() => { handleCategoryChange(category.id); }}
            />
          </div>
          {category.post?.map(post =>
            <div key={post.id}>
              <input type="checkbox" id={post.name} name={post.name}
                checked={!!filters.postIds.includes(post.id)}
                onChange={() => { handleOnClickPost(post.id); }}
              />
              <label htmlFor={post.name}>{post.name}</label>
            </div>
          )}
        </Box>
      )}
    </Box>
  )
}


