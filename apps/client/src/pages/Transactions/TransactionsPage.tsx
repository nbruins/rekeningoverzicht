import Grid from '@mui/material/Grid2'
import React, { FC, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { MainPage } from '../../components'
import { useWindowDimensions } from '../../hooks'
import { useStoreActions, useStoreState } from '../../state/typedHooks'
import { dateService } from '../../utils'
import { Filters } from './Filters'
import { TransactionList } from './TransactionList'

export const TransactionsPage: FC = () => {

  const { t } = useTranslation()
  const title = t('titles.accountStatement')
  useEffect(() => { document.title = title }, [title])
  const { height, width } = useWindowDimensions()

  const loading = useStoreState((state) => state.transactions.loading)
  const transactions = useStoreState((state) => state.transactions.transactions)
  const transactionsError = useStoreState((state) => state.transactions.error)
  const accountNumbers = useStoreState((state) => state.transactions.filters.accountNumbers)
  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)

  useEffect(() => {
    setFilters({
      dateAfterEqual: dateService.firstDateOfYear(),
      dateBeforeEqual: dateService.currentDate(),
      accountNumbers,
      postIds: []
    })
  }, [])

  return (
    <MainPage title={title}>
      <main>
        {transactionsError && <p>fout: {transactionsError}</p>}
        <Grid container justifyContent="center" alignItems="flex-start" spacing={2} columns={{ xs: 1, sm: 12 }}>
          {(width >= 600) &&
            <Grid size={{ xs: 0, sm: 3 }} sx={{ overflow: 'auto', flexDirection: 'column' }} style={{ height: height - 100 }}>
              <Filters />
            </Grid>
          }
          <Grid size={{ xs: 12, sm: 9 }}>
            {transactions.length > 0 &&
              <TransactionList loading={loading} transactions={transactions} />
            }
          </Grid>
        </Grid>
      </main>
    </MainPage>
  )
}
