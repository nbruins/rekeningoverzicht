import styled from '@emotion/styled'
import { Error, ExpandLess, ExpandMore } from '@mui/icons-material'
import { Box, Button, IconButton, SxProps, Tooltip } from '@mui/material'
import React, { CSSProperties, FC, useState } from 'react'
import { CategoryIcon } from '../../components'
import { TransactionWithPostAndCategory } from '../../openapi-fetch'
import { useStoreActions, useStoreState } from '../../state/typedHooks'
import { bedragMin, bedragPlus } from '../../styles'
import { formatValue } from '../../utils'

const baseStyle: SxProps = {
  display: 'flex',
  flexDirection: 'row',
  fontSize: '0.72rem',
  alignItems: 'center',
  width: '100%',
  borderBottom: '1px solid #ccc'
}

const detailsStyle: SxProps = {
  '& p': {
    padding: '2px 0',
    margin: 0,
    whiteSpace: 'unset',
    overflow: 'unset',
    textOverflow: 'unset'
  }
}

const transactionStyle: SxProps = {
  'flexGrow': 1,
  'margin': '4px 0 0 4px',
  'minWidth': 0,
  '& p': {
    paddingTop: '2px',
    margin: 0,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  '& p:first-of-type': {
    fontSize: '0.68rem',
    textAlign: 'left',
    color: 'grey'
  }
}

const selectedStyle: SxProps = {
  backgroundColor: '#D3D3D3'
}

const Span = styled.span<{ balance: number }>`
  color: ${props => props.balance <= 0 ? 'red' : 'green'};
`

interface Props {
  transaction: TransactionWithPostAndCategory
  updateTransactionForm?: (transaction: TransactionWithPostAndCategory) => React.ReactNode
  style?: CSSProperties
}

export const TransactionDetails: FC<Props> = ({ transaction, updateTransactionForm, style }) => {

  const selectedTransactionIds = useStoreState(state => state.transactions.selectedTransactionIds)
  const setSelectedTransactionIds = useStoreActions((actions) => actions.transactions.setSelectedTransactionIds)
  const [showDetails, setShowDetails] = useState(false)

  return (
    <Box
      style={style}
      sx={{
        ...baseStyle,
        ...(showDetails ? detailsStyle : undefined),
        ...((selectedTransactionIds.has(transaction.id) ?? false) ? selectedStyle : undefined)
      }}
    >
      {transaction.error ?
        <Tooltip title={transaction.errorDescription ?? 'Unknown error'}>
          <Button><Error /></Button>
        </Tooltip>
        : updateTransactionForm ? updateTransactionForm(transaction) : <CategoryIcon category={transaction.post?.category} />
      }
      <IconButton size="small" aria-label="hide" onClick={() => { setShowDetails(!showDetails) }}>
        {showDetails ? <ExpandLess fontSize="inherit" /> : <ExpandMore fontSize="inherit" />}
      </IconButton>
      <Box sx={transactionStyle} onClick={() => { setSelectedTransactionIds(transaction.id) }}>
        <p>
          {formatValue(transaction.interestDate, 'date')}&nbsp;
          <Span balance={transaction.balance}>
            saldo: {formatValue(transaction.balance, 'currency')}
          </Span>
        </p>
        <p>{transaction.toFromName ?? transaction.description}</p>
        <p>{transaction.toFromName ? transaction.description : ''}</p>
        <p>{transaction.note ?? ''}</p>
        {showDetails &&
          <>
            <p>
              rekeningnummer: {transaction.toFromAccountNumber}
            </p>
            <p>{transaction.post?.category?.name}, {transaction.post?.name}</p>
          </>
        }
      </Box>
      <Box sx={(transaction.amount <= 0) ? bedragMin : bedragPlus}>
        {formatValue(transaction.amount, 'currency')}
      </Box>
    </Box>
  )
}
