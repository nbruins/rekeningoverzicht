import {
  Autocomplete, Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid2,
  TextField
} from '@mui/material'
import React, { useState } from 'react'
import { useStoreActions, useStoreState } from '../../state/typedHooks'

interface AddRowProps {
  year: number
}

export const BudgetAddRow = ({ year }: AddRowProps): React.JSX.Element | null => {


  const createBudgetForOneMonth = useStoreActions((actions) => actions.budget.createBudgetForOneMonth)
  const loading = useStoreState((state) => state.budget.loading)
  const postsWithoutBudget = useStoreState((state) => state.budget.postsWithoutBudget)

  const [open, setOpen] = useState<boolean>(false)
  const [selectedPostId, setSelectedPost] = useState<number | null>(null)

  const addNewRow = () => {
    if (selectedPostId) {
      for (let i = 0; i < 12; i++) {
        void createBudgetForOneMonth({
          year,
          month: i,
          budget: 0,
          postId: selectedPostId
        })
      }
    }

    setOpen(!open)
  }

  if (loading) return <CircularProgress />
  if (postsWithoutBudget.length <= 0)
    return <Button variant="contained" color="primary" disabled={true}>Regel toevoegen</Button>

  return <>
    <Grid2
      container
      direction="row"
      justifyContent="flex-end"
      alignItems="center"
    >
      <Button variant="contained" color="primary" onClick={() => { setOpen(!open) }}>Regel toevoegen</Button>
    </Grid2>
    <Dialog open={open} onClose={() => { setOpen(!open) }} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Nieuwe begroting toevoegen</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Selecteer een post om deze aan de begroting toe te voegen.
        </DialogContentText>
        <Autocomplete
          id="categories_posts"
          options={postsWithoutBudget}
          groupBy={option => option.category}
          getOptionLabel={option => option.post}
          isOptionEqualToValue={option => option.postId === selectedPostId}
          style={{ width: 300 }}
          renderInput={params => <TextField {...params} label="Selecteer een post" variant="outlined" />}
          onChange={(_undefined, post) => { setSelectedPost(post?.postId ?? null) }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={() => { setOpen(!open) }} color="primary">
          Cancel
        </Button>
        <Button onClick={addNewRow} color="primary">
          Toevoegen
        </Button>
      </DialogActions>
    </Dialog>
  </>
}
