import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { StoreMock } from '../../../types'
import { budgets, postsWithoutBudget, setTestStore } from '../../../utils'
import { BudgetPage } from '../BudgetPage'

describe('BudgetPage test', () => {

  const mock: StoreMock = { budgets: { get: budgets, getPosts: postsWithoutBudget } }
  const store = setTestStore({ setDefault: ['filters'], mock })

  it('should render the BudgetPage and delete a row', async () => {

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <BudgetPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(await screen.findByText('category')).toBeInTheDocument()

  })
})
