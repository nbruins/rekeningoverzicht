import '@testing-library/jest-dom'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BudgetAddRow } from '..'
import { setTestStore } from '../../../utils'

describe('BudgetAddRow tests', () => {

  it('should render the BudgetAddRow without posts, button should be disabled', async () => {

    const store = setTestStore()

    render(
      <StoreProvider store={store} >
        <BudgetAddRow year={2021} />
      </StoreProvider>
    )

    expect(await screen.findByRole('button', { name: 'Regel toevoegen' })).toBeDisabled()

  })

  it('should render the BudgetAddRow and click Cancel', async () => {

    const store = setTestStore()
    store.getActions().budget.setPostsWithoutBudget([{
      category: 'Category1',
      post: 'postName',
      postId: 1
    }])

    await waitFor(() => { expect(store.getState().budget.postsWithoutBudget).toHaveLength(1); })
    render(
      <StoreProvider store={store} >
        <BudgetAddRow year={2021} />
      </StoreProvider>
    )

    userEvent.click(await screen.findByRole('button', { name: 'Regel toevoegen' }))
    userEvent.click(await screen.findByRole('button', { name: 'Cancel' }))
  })

  it('should render the BudgetAddRow and click on Add', async () => {

    const store = setTestStore()
    store.getActions().budget.setPostsWithoutBudget([{
      category: 'Category1',
      post: 'postName',
      postId: 1
    }])

    await waitFor(() => { expect(store.getState().budget.postsWithoutBudget).toHaveLength(1); })
    render(
      <StoreProvider store={store} >
        <BudgetAddRow year={2021} />
      </StoreProvider>
    )

    userEvent.click(await screen.findByRole('button', { name: 'Regel toevoegen' }))

    userEvent.click(await screen.findByLabelText('Selecteer een post'))

    // click on autocomplete item
    userEvent.click(await screen.findByText('postName'))

    // And add the new budget
    userEvent.click(await screen.findByText('Toevoegen'))
  })


})
