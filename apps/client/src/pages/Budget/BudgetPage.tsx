import { CircularProgress, Container, Paper } from '@mui/material'
import { DataGrid, GridCellParams, GridColDef, GridInputRowSelectionModel, GridRowParams } from '@mui/x-data-grid'
import { TFunction } from 'i18next'
import React, { FC, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { MainPage, MonthYearFilter } from '../../components'
import { BudgetResponse } from '../../openapi-fetch'
import { useStoreActions, useStoreState } from '../../state/typedHooks'
import { dateService } from '../../utils'
import { BudgetAddRow } from './BudgetAddRow'

const getBudgetColumns = (t: TFunction): GridColDef[] => {

  const months = dateService.months()

  return [
    {
      headerName: t('category'),
      field: 'category',
      editable: false,
      width: 240
    },
    {
      headerName: t('post'),
      field: 'postName',
      editable: false,
      width: 240
    },
    ...months.map((month) => (
      {
        headerName: month,
        field: month,
        type: 'number',
        width: 80,
        editable: true
      }
    ))
  ] as GridColDef[]
}

export const BudgetPage: FC = () => {

  const { t } = useTranslation()
  const title = t('titles.budget')
  useEffect(() => { document.title = title })

  const dateBeforeEqual = useStoreState((state) => state.transactions.filters).dateBeforeEqual

  const getBudget = useStoreActions((actions) => actions.budget.getBudget)
  const getAllPostsWithoutBudgetForYear = useStoreActions((actions) => actions.budget.getAllPostsWithoutBudgetForYear)
  const updateBudget = useStoreActions((actions) => actions.budget.updateBudget)
  const loading = useStoreState((state) => state.budget.loading)
  const budgets = useStoreState((state) => state.budget.budget)

  const [columns] = useState(getBudgetColumns(t))
  const [months] = useState(dateService.months())

  useEffect(() => {
    getBudget(dateService.getYear(dateBeforeEqual))
    getAllPostsWithoutBudgetForYear(dateService.getYear(dateBeforeEqual))
  }, [dateBeforeEqual])

  const handleProcessRowUpdate = async (updatedRow: BudgetResponse): Promise<BudgetResponse> => {

    const year = dateService.getYear(dateBeforeEqual)
    const postId = updatedRow.postId
    const budgetPerMonth = months.map(month => Number(updatedRow[month]))

    if (year && postId && budgetPerMonth)
      await updateBudget({ year, postId, budgetPerMonth })

    return updatedRow

  }
  const [selectionModel, setSelectionModel] = React.useState<GridInputRowSelectionModel>([])

  return (
    <MainPage title={title}>
      <Container maxWidth={false} className={'container'}>
        <h1>{title}</h1>
        <MonthYearFilter views={['year']} openTo='year' />
        {loading && <CircularProgress />}
        {!loading && !budgets && <h1>De begroting is nog niet beschikbaar</h1>}
        {!loading && budgets &&
          <>
            <BudgetAddRow year={dateService.getYear(dateBeforeEqual)} />
            <Paper sx={{ height: '600px' }}>
              <DataGrid
                columns={columns}
                rows={budgets}
                editMode="row"
                processRowUpdate={handleProcessRowUpdate}
                onProcessRowUpdateError={(error: unknown) => { console.log({ error }) }}
                isRowSelectable={(params: GridRowParams<BudgetResponse>) => params.row.postName !== ''}
                isCellEditable={(params: GridCellParams) => (params.row as BudgetResponse).postName !== ''}
                onRowSelectionModelChange={(newSelectionModel) => { setSelectionModel(newSelectionModel) }}
                rowSelectionModel={selectionModel}
              />
            </Paper>
          </>
        }
      </Container>
    </MainPage>
  )
}
