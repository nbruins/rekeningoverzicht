import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { setTestStore } from '../../utils'
import { NotFoundPage } from '../NotFoundPage'

describe('NotFoundPage test', () => {

  const store = setTestStore()

  it('should render the NotFoundPage with no Errors', () => {

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <NotFoundPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(screen.getAllByText('titles.page not found').length).toBe(2)
  })

})
