import { render, screen } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { setTestStore } from '../../utils'
import { AboutPage } from '../AboutPage'

describe('AboutPage test', () => {

  const store = setTestStore()

  it('should render the AboutPage with no Errors', () => {

    render(
      <StoreProvider store={store} >
        <BrowserRouter>
          <AboutPage />
        </BrowserRouter>
      </StoreProvider >
    )

    expect(screen.getByText('titles.about')).toBeDefined()

  })

})
