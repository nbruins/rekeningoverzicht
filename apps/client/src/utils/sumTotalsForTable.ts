import { BudgetResponse, Category } from '../openapi-fetch'
import { Months, TransactionsAndCategories } from '../types'

type PostsTotals = Record<string, {
  id: number
  name: string
  budget: number
  actual: number
}>

export interface CategoryTotal extends Category {
  budget: number
  actual: number
  posts: PostsTotals
}

export type CategoryTotals = Record<string, CategoryTotal>

interface SumTotalsForTable extends TransactionsAndCategories {
  budgets: BudgetResponse[],
  month: Months
}

export const sumTotalsForTable = ({ transactions, categoriesWithPosts, budgets, month }: SumTotalsForTable): CategoryTotals => {

  // first create an empty list based on all posts
  const catTotals: CategoryTotals = {}
  categoriesWithPosts.forEach(category => {
    const posts = ('post' in category && category.post) ? category.post : ('post' in category) ? category.post : []
    catTotals[category.name] = {
      ...category,
      budget: 0,
      actual: 0,
      type: category.type ?? 'UITGAVEN',
      posts: posts?.reduce((acc, cur) =>
        ({ ...acc, [cur.name]: { id: cur.id, name: cur.name, budget: 0, actual: 0 } }), {}) ?? ''
    }
  })

  // Then add the Budget
  budgets.forEach(budget => {

    const category = budget.parentId ?? budget.category
    const post = budget.postName

    if (catTotals[category]) {
      catTotals[category].budget += budget[month] ?? 0
      if (post && catTotals[category].posts[post]?.budget !== undefined)
        catTotals[category].posts[post].budget += budget[month] ?? 0
    }
  })

  // Then add the Transactions
  transactions.forEach(transaction => {
    if (transaction.post) {
      const post = transaction.post.name
      const category = transaction.post.category?.name ?? 'Unknown'
      const amount = transaction.amount
      if (catTotals[category]) {
        catTotals[category].actual += amount
        if (catTotals[category].posts[post].actual !== undefined)
          catTotals[category].posts[post].actual += amount ?? 0
      }
    }
  })

  return catTotals
}
