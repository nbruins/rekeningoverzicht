
export type FieldType = 'date' | 'currency' | 'category' | 'post'

const currencyFormatter = new Intl.NumberFormat('nl-NL', {
  style: 'currency',
  currency: 'EUR',
  minimumFractionDigits: 2
})

export const formatValue = (value?: unknown, type?: FieldType): string => {
  if (value === undefined) return ''
  if (value instanceof Date) return value.toLocaleDateString('nl-NL')
  if (typeof value === 'string')
    return type === 'date'? new Date(value).toLocaleDateString('nl-NL') : value

  if (typeof value === 'number' && type === 'currency')
    return currencyFormatter.format(value)

  return ''
}

export const autoFormatValue = (value?: Date | number | string): string => {
  if (value === undefined) return ''
  if (value instanceof Date) return value.toLocaleDateString('nl-NL')
  if (typeof value === 'number') return currencyFormatter.format(value)
  return value
}
