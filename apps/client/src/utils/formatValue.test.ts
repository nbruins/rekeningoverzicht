import { autoFormatValue, FieldType, formatValue } from './formatValue'

describe('FormatValue tests', () => {

  const tDate: FieldType = 'date'
  const tCurrency: FieldType = 'currency'

  it('should get an empty string on formatValue', () => {
    expect(formatValue()).toBe('')
  })

  test.each([
    ['16-5-2020', new Date('2020/05/16'), undefined],
    ['16-5-2020', '2020/05/16', tDate],
    ['0', '0', tCurrency],
    ['€ -1,00', -1, tCurrency],
    ['€ -1,00', -1, tCurrency],
    ['€ 0,00', 0, tCurrency],
    ['€ 1,00', 1, tCurrency],
    ['€ 1,00', 1, tCurrency],
    ['€ 1,35', 1.35, tCurrency],
    ['€ 121,35', 121.35, tCurrency],
  ])('should format the value',
    (expected: string, value?: string | number | Date, type?: FieldType) => {
      expect(formatValue(value, type)).toBe(expected)
    })

  it('should get an empty string on autoFormatValue', () => {
    expect(autoFormatValue()).toBe('')
  })

  test.each([
    [new Date('2020/05/16'), '16-5-2020'],
    [-1, '€ -1,00'],
    [0, '€ 0,00'],
    [1, '€ 1,00'],
    [1.35, '€ 1,35'],
    [121.35, '€ 121,35'],
    ['something', 'something']
  ])('should format the value automatically', (value, expected) => {
    expect(autoFormatValue(value)).toBe(expected)
  })

})
