declare global {
  interface Window { env: Record<string, string> }
}

export const getSetting = (name: string, defaultValue = ''): string => {
  try {
    return window.env[String(name)] ?? defaultValue
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
  } catch (e) {
    return defaultValue
  }
}