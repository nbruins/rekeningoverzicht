import { sumTotalsForTreemap } from './sumTotalsForTreemap'
import { categoriesWithPosts, transactions } from './__mock__'

describe('sumTotalsForTreemap tests', () => {

  it('should get the totals', () => {
    const result = sumTotalsForTreemap({ transactions, categoriesWithPosts })[0]

    expect(result.type).toEqual('treemap')
    expect(result.branchvalues).toEqual('total')
    expect(result.textinfo).toEqual('label+value+percent parent+percent entry')
    expect(result.labels.length).toEqual(result.values.length)
  })
})
