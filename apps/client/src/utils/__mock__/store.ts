import { configureStore } from '../../state'
import { StoreMock } from '../../types'
import { dateService } from '../dateService'
import { categoriesWithPosts } from './categoriesWithPosts'
import { transactionsTestDate } from './transactions'

interface Props {
  setDefault?: ('filters' | 'categories')[]
  mock?: StoreMock
}
export const setTestStore = ({ setDefault, mock }: Props = {}) => {

  const store = configureStore({ mock })

  store.getActions().budget.setIsLoaded(true)

  if (setDefault?.includes('filters'))
    store.getActions().transactions.setFilters({
      dateAfterEqual: dateService.firstDateOfYear(transactionsTestDate),
      dateBeforeEqual: dateService.lastDateOfMonth(transactionsTestDate),
      accountNumbers: [],
      postIds: []
    })

  if (setDefault?.includes('categories'))
    store.getActions().categories.setCategoriesWithPosts(categoriesWithPosts)

  return store
}