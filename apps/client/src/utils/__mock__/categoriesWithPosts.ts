import { Category } from '../../openapi-fetch'

 
export const categoriesWithPosts: Required<Category>[] = [
  {
    id: 1,
    name: 'Abonementen en telecom',
    icon: 'cardMembership',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 266,
    backgroundColor: '#df01d7',
    borderColor: '#3b0b39',
    color: '#ffffff',
    post: [
      {
        id: 67,
        name: 'Abonnement',
        regExp: 'TrosKompas|DAGBLADEN|Sanoma|Telegraaf|Anwb Contributies|Psychologie Magazine|Kidsweek|Spotify|Vereniging Eigen Huis|Netflix|DPG MEDIA',
        categoryId: 1
      },
      {
        id: 80,
        name: 'Cursus/Opleiding',
        regExp: '',
        categoryId: 1
      },
      {
        id: 95,
        name: 'Internet/Kabel tv/Telefoon',
        regExp: 'LIVESCHIJF|TWEAK',
        categoryId: 1
      },
      {
        id: 101,
        name: 'Loterij',
        regExp: 'POSTCODE LOTERIJ',
        categoryId: 1
      },
      {
        id: 104,
        name: 'Muziekles',
        regExp: 'HARMONIE ORKEST TWENTE|vioolles',
        categoryId: 1
      },
      {
        id: 121,
        name: 'Sportabonement',
        regExp: 'Tennisles|Tennis|ARIENSGROEP|scouting|Iphitos',
        categoryId: 1
      },
      {
        id: 122,
        name: 'Telefoon mobiel',
        regExp: 'KPN|T-MOBILE|Orange|RABO MOBIEL|VODAFONE|hollandsnieuwe',
        categoryId: 1
      }
    ],
  },
  {
    id: 2,
    name: 'Energie en lokale lasten',
    icon: 'power',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 305,
    backgroundColor: '#6b5b95',
    borderColor: '#878f99',
    color: '#ffffff',
    post: [
      {
        id: 73,
        name: 'Belasting gemeente',
        regExp: 'GBT ONTVANGSTEN|ENSCHEDE BELASTINGEN|Gem.Belastingen|Gemeentelijke Belastingen',
        categoryId: 2
      },
      {
        id: 75,
        name: 'Belasting waterschap',
        regExp: 'GBLT',
        categoryId: 2
      },
      {
        id: 82,
        name: 'Energie',
        regExp: 'BUDGETENERGIE',
        categoryId: 2
      },
      {
        id: 83,
        name: 'Energie lening',
        regExp: 'STICHTING NEF',
        categoryId: 2
      },
      {
        id: 127,
        name: 'Water',
        regExp: 'Lococensus|VITENS|Waterschap Regge Dinkel',
        categoryId: 2
      }
    ],
  },
  {
    id: 3,
    name: 'Huishoudelijke uitgaven',
    icon: 'localGroceryStore',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 710,
    backgroundColor: '#df7401',
    borderColor: '#3b240b',
    color: '#ffffff',
    post: [
      {
        id: 78,
        name: 'Boodschappen',
        regExp: 'ROSSMANN|NETTORAMA|ETOS|supermarkt|ALBERT HEIJN|SLIJTERIJ|C1000|KRUIDVAT|SALOMONS ESCHMARKE ENSCH|SANDERS|TER HUURNE|GALL & GALL|Fruithandel Luiten|Banketbakkerij|Klaas+Kock|Lidl|HARDICK|Aldi|EDEKA|EMTE|Penny|Oonk|Rudy\'s Visbakkerij|Netto|Jumbo|BAKKERIJ',
        categoryId: 3
      },
      {
        id: 79,
        name: 'Cadeautje',
        regExp: 'BART SMIT|FOTO2GO|BLOEMEN|cadeau|KAARTJE2GO|Bloemsierkunst|GREETZ.NL|moederdag',
        categoryId: 3
      },
      {
        id: 81,
        name: 'Diversen',
        regExp: '',
        categoryId: 3
      },
      {
        id: 86,
        name: 'Goede doelen',
        regExp: 'ZONNEBLOEM',
        categoryId: 3
      },
      {
        id: 89,
        name: 'Huisdieren',
        regExp: '',
        categoryId: 3
      },
      {
        id: 96,
        name: 'Kantine',
        regExp: 'Bedrijfsrest',
        categoryId: 3
      },
      {
        id: 97,
        name: 'Kapper/Verzorging',
        regExp: 'HAARMODE ALEXANDRA|Amikappers|Hairteam|Hairshop|KuypersHair|AMI Kappers|Danielle\'sHaarstudio',
        categoryId: 3
      },
      {
        id: 107,
        name: 'Onkosten Niek',
        regExp: 'Schiedam|Jaarbeurs',
        categoryId: 3
      },
      {
        id: 108,
        name: 'Onkosten Simone',
        regExp: '',
        categoryId: 3
      },
      {
        id: 113,
        name: 'Pin/Contanten',
        regExp: 'Chipknip|Geldautomaat|MINITIX',
        categoryId: 3
      },
      {
        id: 129,
        name: 'Zakgeld',
        regExp: 'Zakgeld',
        categoryId: 3
      },
      {
        id: 132,
        name: 'creditcard',
        regExp: 'Zie rekeningoverzicht|Interpay',
        categoryId: 3
      }
    ],

  },
  {
    id: 4,
    name: 'Inkomsten',
    icon: 'euro',
    type: 'INKOMSTEN',
    gemmiddeldbedragNibud: 0,
    backgroundColor: '#008000',
    borderColor: '#173b0b',
    color: '#043000',
    post: [
      {
        id: 74,
        name: 'Belasting inkomsten',
        regExp: 'TERUGGAAF|3171784137070002',
        categoryId: 4
      },
      {
        id: 98,
        name: 'Kinderbijslag',
        regExp: 'KINDERBIJSLAG|Sociale Verzekeringsbank',
        categoryId: 4
      },
      {
        id: 115,
        name: 'Salaris Niek',
        regExp: 'EMAXX|ENSCHEDE ADM. KANTOO|EXXELLENCE',
        categoryId: 4
      },
      {
        id: 116,
        name: 'Salaris Simone',
        regExp: 'NR. 3167 00116283|potje KV|Loon/salaris|SALARIS',
        categoryId: 4
      }
    ],

  },
  {
    id: 5,
    name: 'Inventaris/huis',
    icon: 'build',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 513,
    backgroundColor: '#7401df',
    borderColor: '#240b3b',
    color: '#ffffff',
    post: [
      {
        id: 68,
        name: 'Apparatuur',
        regExp: '',
        categoryId: 5
      },
      {
        id: 85,
        name: 'Gereedschap en inventaris',
        regExp: 'HOBBYCENTR. MORSHU|FIXET|KARWEI|BOUWMARKT|PRAXIS|Hellweg|Gamma',
        categoryId: 5
      },
      {
        id: 88,
        name: 'Huis/Tuin',
        regExp: 'BLOKKER|Hema|LEEN BAKKER|IKEA|XENOS|INTRATUIN|TUINCENTRUM|1201 action|WELKOOP|Dierenspeciaalzaak|MARSKRAMER|Bloem|Zonwering|Glazenwasser|Action 1370',
        categoryId: 5
      },
      {
        id: 94,
        name: 'Inrichting',
        regExp: '',
        categoryId: 5
      },
      {
        id: 102,
        name: 'Marktplaats',
        regExp: '',
        categoryId: 5
      },
      {
        id: 110,
        name: 'Opknappen huis',
        regExp: '',
        categoryId: 5
      },
      {
        id: 123,
        name: 'Tuin',
        regExp: '',
        categoryId: 5
      }
    ],

  },
  {
    id: 6,
    name: 'Kleding en Schoenen',
    icon: 'store',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 225,
    backgroundColor: '#8080c0',
    borderColor: '#3b170b',
    color: '#ffffff',
    post: [
      {
        id: 99,
        name: 'Kleding\\\\Schoenen',
        regExp: 'Zeeman|OPEN32|Esprit|HANS TEXTIEL|H\\\\&M|VAN HAREN|Schuurman Schoenen|TER STAL|SLIM|C\\\\&A|KIK|Bakker sport|SCORE|ETAM|Tuunte|Deichmann|Scapino|Hennes \\\\& Mauritz|Wehkamp|MENGER MODES|V\\\\&D|PERRY SPORT|PIET ZOOMERS|Schoenen|Bonprix|RUNNING CENTER|PRIMARK|MENGER',
        categoryId: 6
      },
      {
        id: 100,
        name: 'Kleedgeld Meike',
        regExp: '',
        categoryId: 6
      }
    ],

  },
  {
    id: 7,
    name: 'Niet ingedeeld',
    icon: 'helpOutline',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 0,
    backgroundColor: '#ff0000',
    borderColor: '#4d0000',
    color: '#ffffff',
    post: [
      {
        id: 106,
        name: 'Onbekend',
        regExp: '',
        categoryId: 7
      },
      {
        id: 111,
        name: 'Overige',
        regExp: '',
        categoryId: 7
      }
    ],

  },
  {
    id: 8,
    name: 'Onderwijs',
    icon: 'school',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 9,
    backgroundColor: '#a9ca46',
    borderColor: '#ce2275',
    color: '#682366',
    post: [
      {
        id: 117,
        name: 'School',
        regExp: 'Troubadour',
        categoryId: 8
      }
    ],

  },
  {
    id: 9,
    name: 'Overige medische kosten',
    icon: 'localHospital',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 82,
    backgroundColor: '#ffff00',
    borderColor: '#5e610b',
    color: '#3d2900',
    post: [
      {
        id: 103,
        name: 'Medischekosten',
        regExp: 'FA~MED|Apotheek|Mediq Apotheken',
        categoryId: 9
      }
    ],

  },
  {
    id: 10,
    name: 'Overige vaste lasten',
    icon: 'payment',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 0,
    backgroundColor: '#08088a',
    borderColor: '#0b0b3b',
    color: '#ffffff',
    post: [
      {
        id: 70,
        name: 'Bankrekening',
        regExp: 'Kosten Europas|Kosten Rabo|\\\\(Extra\\\\) Rabocard|\\\\(Extra\\\\) Wereldpas|Rabo TotaalPakket|GoldCardPeriode|Rabo GoldCard Periode',
        categoryId: 10
      },
      {
        id: 90,
        name: 'Hypotheek Keuzeplus',
        regExp: '',
        categoryId: 10
      },
      {
        id: 114,
        name: 'Rente',
        regExp: 'Debetrente',
        categoryId: 10
      },
      {
        id: 131,
        name: 'Zwemles',
        regExp: 'Zwembad de Brug',
        categoryId: 10
      }
    ],

  },
  {
    id: 11,
    name: 'Persoonlijk budget',
    icon: 'emojiPeople',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 0,
    backgroundColor: '#336633',
    borderColor: '#333300',
    color: '#FFFFFF',
    post: [
      {
        id: 105,
        name: 'Niek',
        regExp: '',
        categoryId: 11
      },
      {
        id: 118,
        name: 'Simone',
        regExp: '',
        categoryId: 11
      }
    ],

  },
  {
    id: 12,
    name: 'Spaarrekening',
    icon: 'trendingUp',
    type: 'SPAREN',
    gemmiddeldbedragNibud: 500,
    backgroundColor: '#00ffbf',
    borderColor: '#0b3b39',
    color: '#00261d',
    post: [
      {
        id: 119,
        name: 'Sparen',
        regExp: 'sparen|NL18RABO1340129809',
        categoryId: 12
      }
    ],

  },
  {
    id: 13,
    name: 'Vervoer',
    icon: 'commute',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 269,
    backgroundColor: '#df0174',
    borderColor: '#3b0b24',
    color: '#ffffff',
    post: [
      {
        id: 69,
        name: 'Auto',
        regExp: 'WASH',
        categoryId: 13
      },
      {
        id: 71,
        name: 'Bekeuring',
        regExp: 'Justitieel',
        categoryId: 13
      },
      {
        id: 72,
        name: 'Belasting auto',
        regExp: '75-JP-TB|99-JTR-8|52-DS-GV',
        categoryId: 13
      },
      {
        id: 76,
        name: 'Benzine',
        regExp: 'EASYTANK|TANGO|TEXACO|TANKSTELLE|AVIA|TAMOIL|GULF|WEGHORST|HERON|SHELL|BP POL|TankstationZuiderval|amiGo Losser|Esso|Tankstation|Fieten Olie',
        categoryId: 13
      },
      {
        id: 84,
        name: 'Fiets',
        regExp: 'DAMHUIS RIJWIELEN|HALFORDS|Rijwielpaleis',
        categoryId: 13
      },
      {
        id: 109,
        name: 'Openbaar vervoer',
        regExp: 'NS-GLANERBURG|OV-Chipkaart|NS Glanerbrug',
        categoryId: 13
      }
    ],

  },
  {
    id: 14,
    name: 'Verzekeringen',
    icon: 'security',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 435,
    backgroundColor: '#6e6e6e',
    borderColor: '#1c1c1c',
    color: '#ffffff',
    post: [
      {
        id: 125,
        name: 'Verzekering',
        regExp: 'ALLES IN EEN POLIS|DELA NATURA|OVERLIJDENSRISICOVERZEKERING',
        categoryId: 14
      },
      {
        id: 130,
        name: 'Ziektekostenpremie',
        regExp: 'UNIVE ZORG',
        categoryId: 14
      }
    ],

  },
  {
    id: 15,
    name: 'Vrijetijdsuitgaven',
    icon: 'beachAccess',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 648,
    backgroundColor: '#ff80ff',
    borderColor: '#800040',
    color: '#ffffff',
    post: [
      {
        id: 77,
        name: 'Boeken/Tijdschriften/DVD',
        regExp: '',
        categoryId: 15
      },
      {
        id: 87,
        name: 'Hobby',
        regExp: 'FOTO DE BOER|Webprint|FUNPRICE|PCextreme|BCC|PROGRESS|Voice and Spirit|4Launch|Megekko B.V.|Coolblue|metaregistrar|Networking4all',
        categoryId: 15
      },
      {
        id: 120,
        name: 'Sport',
        regExp: '',
        categoryId: 15
      },
      {
        id: 124,
        name: 'Vakanties',
        regExp: '',
        categoryId: 15
      },
      {
        id: 126,
        name: 'Vrijetijdsbesteding',
        regExp: 'Kids City|MISTER PANCAKE|DE SNIPPERT|McDonald|Mc Donald|Pizzeria|NATUR ZOO RHEINE|Optisport|WOLFF|Twentse Schouwburg|EASTERN PLAZA|Kinepolis|Thuisbezorgd|Happy Italy|Restaurant|Starballs|Ballorig|De Egel',
        categoryId: 15
      },
      {
        id: 128,
        name: 'Wintersport',
        regExp: '',
        categoryId: 15
      }
    ],

  },
  {
    id: 16,
    name: 'Wonen',
    icon: 'house',
    type: 'UITGAVEN',
    gemmiddeldbedragNibud: 1055,
    backgroundColor: '#01a9db',
    borderColor: '#0b2f3a',
    color: '#ffffff',
    post: [
      {
        id: 91,
        name: 'Hypotheek Rente',
        regExp: 'RENTE LENING',
        categoryId: 16
      },
      {
        id: 92,
        name: 'Hypotheek aflossing',
        regExp: 'OpbouwSpaarrekening',
        categoryId: 16
      },
      {
        id: 93,
        name: 'Hypotheek linear',
        regExp: 'RENTE \\\\+ AFLOSSING LENING',
        categoryId: 16
      },
      {
        id: 112,
        name: 'Overlijdensrisicoverzekering',
        regExp: 'Overlijdensrisicoverzekering',
        categoryId: 16
      }
    ],
  }
]
