import { PostsWithOutBudgetResponse } from '../../openapi-fetch'

export const postsWithoutBudget: PostsWithOutBudgetResponse[] = [
  { category: 'Huishoudelijke uitgaven', post: 'creditcard', postId: 132 },
  { category: 'Niet ingedeeld', post: 'Onbekend', postId: 7 },
  { category: 'Overige vaste lasten', post: 'Zwemles', postId: 131 }
]