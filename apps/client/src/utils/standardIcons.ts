import {
  BeachAccess,
  Build,
  CardMembership, Commute,
  EmojiPeople,
  Euro, HelpOutline, House,
  LocalGroceryStore, LocalHospital, Payment,
  Power, School, Security,
  Store,
  SvgIconComponent, TrendingUp
} from '@mui/icons-material'

export const standardIcons: Record<string, SvgIconComponent> = {
  cardMembership: CardMembership,
  power: Power,
  localGroceryStore: LocalGroceryStore,
  euro: Euro,
  emojiPeople: EmojiPeople,
  build: Build,
  store: Store,
  school: School,
  localHospital: LocalHospital,
  payment: Payment,
  trendingUp: TrendingUp,
  commute: Commute,
  security: Security,
  beachAccess: BeachAccess,
  house: House,
  helpOutline: HelpOutline
}
