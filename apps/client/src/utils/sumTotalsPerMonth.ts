import { ChartDataset, ChartTypeRegistry } from 'chart.js'
import { Category, TransactionWithPostAndCategory } from '../openapi-fetch'

export interface PostTotals { postTotals: Record<string, number[]> }

interface CategoryTotals {
  categoryTotals: ChartDataset<keyof ChartTypeRegistry, number[]> & PostTotals
}

type TotalsPerCatWithTotalsPerPost = Record<string, CategoryTotals>
export interface TotalsPerCategoryPerMonth {
  lastTransactionDate: Date,
  totals: (ChartDataset<keyof ChartTypeRegistry, number[]> & PostTotals)[]
}

const addEmptyMonths = (): number[] => ([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

const addNewCategory = (category: Category, postName: string): CategoryTotals => ({
  categoryTotals: {
    backgroundColor: category.backgroundColor ?? '#FFF',
    label: category.name,
    stack: category.type ?? 'UITGAVEN',
    data: addEmptyMonths(),
    postTotals: { [postName]: addEmptyMonths() }
  }
})

export const sumTotalsPerCategoryPerMonth = (transactions: TransactionWithPostAndCategory[]): TotalsPerCategoryPerMonth => {

  let lastTransactionDate: Date | null = null
  const totalPerMonthPerCategory: TotalsPerCatWithTotalsPerPost = {}
  transactions.forEach(transaction => {
    const amount = transaction.amount
    const category = transaction.post?.category
    const postName = transaction.post?.name
    const interestDate = transaction.interestDate
    if (!lastTransactionDate || lastTransactionDate <= interestDate) lastTransactionDate = interestDate
    const month = interestDate.getMonth()
    if (category && postName && amount && month !== undefined) {
      if (!totalPerMonthPerCategory[category.name]?.categoryTotals.data)
        totalPerMonthPerCategory[category.name] = addNewCategory(category, postName)
      if (!totalPerMonthPerCategory[category.name]?.categoryTotals.postTotals[postName])
        totalPerMonthPerCategory[category.name].categoryTotals.postTotals[postName] = addEmptyMonths()

      totalPerMonthPerCategory[category.name].categoryTotals.data[month] += category.type === 'UITGAVEN' ? amount * -1 : amount
      totalPerMonthPerCategory[category.name].categoryTotals.postTotals[postName][month] +=
        category.type === 'UITGAVEN' ? amount * -1 : amount
    }
  })
  return {
    lastTransactionDate: lastTransactionDate ?? new Date(),
    totals: Object.values(totalPerMonthPerCategory)
      .map(tpm => tpm.categoryTotals)
      .sort((a, b) => {
        if (!a.label || !b.label)
          return 0
        return a.label < b.label ? -1 : a.label > b.label ? 1 : 0
      })
  }
}
