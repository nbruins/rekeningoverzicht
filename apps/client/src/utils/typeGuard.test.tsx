import React from 'react'
import { render, screen } from '@testing-library/react'
import { isInputElement } from './typeGuards'

describe('isInputElement tests', () => {

  it('should indicate that the field is of type Input', () => {

    render(
      <>
        <label htmlFor="inputField">input field</label>
        <input type="text" id="inputField" name="inputField" />
      </>
    )
    expect(isInputElement(screen.getByLabelText('input field'))).toBeTruthy()
  })

  it('should indicate that the field is not of type Input', () => {

    render(<p>text</p>)
    expect(isInputElement(screen.getByText('text'))).toBeFalsy()
  })

})
