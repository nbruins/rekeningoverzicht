import { endOfMonth, endOfYear, format, getDate, getISOWeek, getMonth, getYear, startOfMonth, startOfYear } from 'date-fns'
import { Months } from '../types'

const localDateAsUTC = (localDate: Date): Date => {
  const year = getYear(localDate)
  const month = getMonth(localDate)
  const day = getDate(localDate)
  return new Date(Date.UTC(year, month, day))
}
const currentDate = localDateAsUTC(new Date())

const convertDateStringToUTC = (date: string): Date => {

  const dateSplit = date.split('-')
  const year = (dateSplit[0].length === 4) ? dateSplit[0] : dateSplit[2]
  const month = dateSplit[1]
  const day = (dateSplit[0].length === 4) ? dateSplit[2] : dateSplit[0]

  return new Date(Date.parse(`${year}-${month}-${day}T00:00:00.000Z`))
}

export const dateService = {
  currentDate: (): Date => currentDate,
  firstDateOfYear: (date?: Date): Date => localDateAsUTC(startOfYear(date ?? currentDate)),
  lastDateOfYear: (date?: Date): Date => localDateAsUTC(endOfYear(date ?? currentDate)),
  firstDateOfMonth: (date?: Date): Date => localDateAsUTC(startOfMonth(date ?? currentDate)),
  lastDateOfMonth: (date?: Date): Date => localDateAsUTC(endOfMonth(date ?? currentDate)),
  getYear: (date?: Date): number => getYear(date ?? currentDate),
  getMonth: (date?: Date): number => getMonth(date ?? currentDate),
  getISOWeek: (date?: Date): number => getISOWeek(date ?? currentDate),
  getDate: (date?: Date): number => getDate(date ?? currentDate),
  months: (): Months[] => (['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']),
  formatYYYYMMDD: (date?: Date): string => format(date ?? currentDate, 'yyyy-MM-dd'),
  localDateAsUTC: (localDate?: Date): Date => localDate ? localDateAsUTC(localDate) : currentDate,
  convertDateStringToUTC
}

