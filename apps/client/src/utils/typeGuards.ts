export const isInputElement = (el: HTMLElement): el is HTMLInputElement => ('value' in el)
