import { dateService } from './dateService'

describe('dateService tests', () => {

  const date = new Date(Date.UTC(2021, 0, 8))
  const dateInFeb = new Date(Date.UTC(2022, 1, 2))
  const dateInFebLeapYear = new Date(Date.UTC(2024, 1, 2))

  it('should current date same as localDateAsUTC', () => {
    expect(dateService.currentDate()).toBe(dateService.localDateAsUTC())
  })
  it('should get firstDateOfyear with no given date', () => {
    expect(dateService.firstDateOfYear() instanceof Date).toBeTruthy()
  })
  it('should get lastDateOfYear with no given date', () => {
    expect(dateService.lastDateOfYear() instanceof Date).toBeTruthy()
  })
  it('should get firstDateOfMonth with no given date', () => {
    expect(dateService.firstDateOfMonth() instanceof Date).toBeTruthy()
  })
  it('should get lastDateOfMonth with no given date', () => {
    expect(dateService.lastDateOfMonth() instanceof Date).toBeTruthy()
  })
  it('should get a number when calling getYear with no given Date', () => {
    expect(typeof dateService.getYear()).toBe('number')
  })
  it('should get a number when calling getMonth with no given Date', () => {
    expect(typeof dateService.getMonth()).toBe('number')
  })
  it('should get a number when calling getDate with no given Date', () => {
    expect(typeof dateService.getDate()).toBe('number')
  })
  it('should get the months', () => {
    expect(dateService.months()).toEqual(['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'])
  })
  it('should get a string when calling formatYYYYMMDD with no given Date', () => {
    expect(typeof dateService.formatYYYYMMDD()).toBe('string')
  })
  it('should get lastDateOfYear with no given date', () => {
    expect(dateService.lastDateOfYear() instanceof Date).toBeTruthy()
  })

  it('should get same date with localDateAsUTC as the given date', () => {
    expect(dateService.localDateAsUTC(date)).toStrictEqual(date)
  })
  it('should get firstDateOfYear with a given date ', () => {
    expect(dateService.firstDateOfYear(date)).toStrictEqual(new Date(Date.UTC(2021, 0, 1)))
  })
  it('should get lastDateOfYear with a given date ', () => {
    expect(dateService.lastDateOfYear(date)).toStrictEqual(new Date(Date.UTC(2021, 11, 31)))
  })
  it('should get firstDateOfMonth with a given date ', () => {
    expect(dateService.firstDateOfMonth(date)).toStrictEqual(new Date(Date.UTC(2021, 0, 1)))
    expect(dateService.firstDateOfMonth(dateInFeb)).toStrictEqual(new Date(Date.UTC(2022, 1, 1)))
    expect(dateService.firstDateOfMonth(dateInFebLeapYear)).toStrictEqual(new Date(Date.UTC(2024, 1, 1)))
  })
  it('should get lastDateOfMonth with a given date ', () => {
    expect(dateService.lastDateOfMonth(date)).toStrictEqual(new Date(Date.UTC(2021, 0, 31)))
    expect(dateService.lastDateOfMonth(dateInFeb)).toStrictEqual(new Date(Date.UTC(2022, 1, 28)))
    expect(dateService.lastDateOfMonth(dateInFebLeapYear)).toStrictEqual(new Date(Date.UTC(2024, 1, 29)))
  })
  it('should get getYear with a given date ', () => {
    expect(dateService.getYear(date)).toBe(2021)
  })
  it('should get getMonth with a given date ', () => {
    expect(dateService.getMonth(date)).toBe(0)
  })
  it('should get getDate with a given date ', () => {
    expect(dateService.getDate(date)).toBe(8)
  })
  it('should format a given date ', () => {
    expect(dateService.formatYYYYMMDD(date)).toBe('2021-01-08')
  })
})
