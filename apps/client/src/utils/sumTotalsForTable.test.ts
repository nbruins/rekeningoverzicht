import { sumTotalsForTable } from './sumTotalsForTable'
import { budgets, categoriesWithPosts, transactions } from './__mock__'

describe('sumTotalsForTable tests', () => {

  it('should get the totals', () => {
    const result = sumTotalsForTable({ transactions, categoriesWithPosts, budgets, month: 'feb' })

    const house = result['Huishoudelijke uitgaven']

    // Then there should be 16 categories
    expect(Object.keys(result).length).toEqual(16)

    // Then huishoudelijke uitgaven should be
    expect(house.actual).toBe(-21.32 + -2.49 + -10.09 + -10.09)
    expect(house.budget).toBe(-894)

    // And huishoudelijke uitgaven should have 12 posts
    expect(Object.keys(house.posts).length).toEqual(12)

    const groceries = house.posts.Boodschappen
    expect(groceries.actual).toEqual(-21.32 + -2.49 + -10.09 + -10.09)
    expect(groceries.budget).toEqual(-800)
  })
})


