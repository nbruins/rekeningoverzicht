import { sumTotalsPerCategoryPerMonth } from './sumTotalsPerMonth'
import { transactions } from './__mock__/transactions'

describe('sumTotalsPerMonth tests', () => {

  it('should get the totals', () => {
    const result = sumTotalsPerCategoryPerMonth(transactions)

    expect(result.totals.length).toEqual(2)
    expect(result.totals[0].label).toEqual('Energie en lokale lasten')
    expect(result.totals[0].postTotals).toBeDefined()
    expect(result.totals[1].label).toEqual('Huishoudelijke uitgaven')
  })
})
