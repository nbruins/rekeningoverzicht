import { TransactionWithPostAndCategory } from '../openapi-fetch'


interface Props {
  transactions: TransactionWithPostAndCategory[]
}

interface GetTotals {
  plus: number
  min: number
  total: number
}

export const getTotals = ({ transactions }: Props): GetTotals => {

  let plus = 0
  let min = 0
  transactions.forEach(transaction =>
    transaction.amount > 0 ? plus += transaction.amount : min += transaction.amount
  )
  return { plus, min, total: plus + min }
}
