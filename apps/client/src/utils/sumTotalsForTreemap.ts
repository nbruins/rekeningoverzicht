import { TransactionsAndCategories } from '../types'
import { Color, ScatterData } from 'plotly.js'

export const sumTotalsForTreemap = ({ transactions, categoriesWithPosts }: TransactionsAndCategories): ScatterData[] => {

  const totalPerPost: Record<string, { total: number }> = {}
  transactions.forEach(transaction => {
    const amount = transaction.amount
    const postName = transaction.post?.name
    if (postName && amount)
      if (totalPerPost[postName]?.total) {
        totalPerPost[postName].total += amount
      } else {
        totalPerPost[postName] = { total: amount }
      }
  })
  const labels: string[] = []
  const parents: string[] = []
  const values: number[] = []
  const colors: Color[] = []
  categoriesWithPosts.forEach(category => {
    let categoryTotal = 0
    const catIndex = values.length
    const categoryColor = category.backgroundColor ?? '#FFF'
    labels.push(category.name)
    parents.push('')
    values.push(0)
    colors.push(categoryColor)
    const posts = ('post' in category && category.post) ? category.post : ('Post' in category) ? category.post : []
    posts?.forEach(post => {
      labels.push(post.name)
      parents.push(category.name)
      const postTotal = totalPerPost[post.name] ? Math.abs(Math.round(totalPerPost[post.name].total)) : 0
      values.push(postTotal)
      categoryTotal += postTotal
      colors.push(categoryColor)
    })
    values[catIndex] = categoryTotal
  })
  return [{
    type: 'treemap',
    branchvalues: 'total',
    labels,
    parents,
    values,
    textinfo: 'label+value+percent parent+percent entry',
    marker: { colors }
  }] as unknown as ScatterData[]

}
