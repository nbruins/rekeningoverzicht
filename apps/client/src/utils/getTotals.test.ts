import { getTotals } from './getTotals'
import { transactions } from './__mock__/transactions'

describe('getTotals tests', () => {

  it('should get the totals of the standard list', () => {
    const result = getTotals({ transactions })
    const min = -21.32 + -2.49 + -10.09 + -10.09
    const plus = 20.15
    const expected = { min, plus, total: plus + min }
    expect(result).toEqual(expected)
  })

})
