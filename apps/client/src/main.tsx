import { AuthenticationResult, EventMessage, EventType, PublicClientApplication } from '@azure/msal-browser'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { createRoot } from 'react-dom/client'
import { Provider, ReactReduxContext as context } from 'react-redux'
import App from './App'
import { msalConfig } from './authconfig'
import './i18n'
import { configureStore } from './state'

/**
 * MSAL should be instantiated outside of the component tree to prevent it from being re-instantiated on re-renders.
 * For more, visit: https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-react/docs/getting-started.md
 */
const msalInstance = new PublicClientApplication(msalConfig())

msalInstance.initialize().then(() => {
  // Account selection logic is app dependent. Adjust as needed for different use cases.
  const accounts = msalInstance.getAllAccounts()
  if (accounts.length > 0) {
    msalInstance.setActiveAccount(accounts[0])
  }

  msalInstance.addEventCallback((event: EventMessage) => {
    if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
      const payload = event.payload as AuthenticationResult
      const account = payload.account
      msalInstance.setActiveAccount(account)
    }
  })

  const store = configureStore()
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const container = document.getElementById('root')!

  const root = createRoot(container)
  root.render(
    <StoreProvider store={store} >
      <Provider store={store} context={context}>
        <App instance={msalInstance} />
      </Provider>
    </StoreProvider >
  )
})