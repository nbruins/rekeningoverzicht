import { IPublicClientApplication } from '@azure/msal-browser'
import { MsalProvider } from '@azure/msal-react'
import { createTheme, ThemeOptions, ThemeProvider } from '@mui/material/styles'
import React, { FC, lazy, Suspense, useEffect } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { AboutPage, BudgetPage, ImportPage, NotFoundPage, SettingsPage, TransactionsPage } from './pages'
import { useStoreActions, useStoreState } from './state/typedHooks'

const Reports = lazy(() => import('./pages/Reports/ReportsPage'))
 
export const ColorModeContext = React.createContext({ toggleColorMode: () => { undefined } })

const App: FC<{ instance: IPublicClientApplication }> = ({ instance }) => {

  const getCategoriesWithPosts = useStoreActions((actions) => actions.categories.getCategoriesWithPosts)

  useEffect(() => { getCategoriesWithPosts() }, [])

  // const navigate = useNavigate()
  // const navigationClient = new CustomNavigationClient(navigate)
  // instance.setNavigationClient(navigationClient)

  const themeOptions = useStoreState((state) => state.config.themeOptions)
  const setThemeOptions = useStoreActions((actions) => actions.config.setThemeOptions)

  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        const updatedTheme: ThemeOptions = {
          palette: {
            ...themeOptions.palette,
            mode: themeOptions.palette?.mode === 'light' ? 'dark' : 'light'
          }
        }
        setThemeOptions(updatedTheme)
      },
    }),
    [themeOptions],
  )

  const theme = React.useMemo(() => createTheme(themeOptions), [themeOptions])

  return (
    <MsalProvider instance={(instance)}>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>
          <Suspense fallback="loading">
            <BrowserRouter>
              <Routes>
                <Route path={'/'} element={<TransactionsPage />} />
                <Route path={'/accountstatement'} element={<TransactionsPage />} />
                <Route path={'/transactions'} element={<TransactionsPage />} />
                <Route path={'/monthlyrepost'} element={<Reports />} />
                <Route path={'/budget'} element={<BudgetPage />} />
                <Route path={'/import'} element={<ImportPage />} />
                <Route path={'/configuration'} element={<SettingsPage />} />
                <Route path={'/about'} element={<AboutPage />} />
                <Route path="*" element={<NotFoundPage />} />
              </Routes>
            </BrowserRouter>
          </Suspense>
        </ThemeProvider>
      </ColorModeContext.Provider>
    </MsalProvider>
  )
}

export default App
