import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFnsV3'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { dateService, setTestStore } from '../../utils'
import { MonthYearFilter } from '../MonthYearFilter'

describe('MonthYearFilter tests', () => {

  it('should render the MonthYearFilter with default views', async () => {

    const user = userEvent.setup()
    // configure store
    const store = setTestStore()

    render(
      <StoreProvider store={store} >
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <MonthYearFilter />
        </LocalizationProvider>
      </StoreProvider >
    )


    // test for a month
    expect(await screen.findByLabelText('Jaar/Maand')).toBeInTheDocument()

    user.click(await screen.findByRole('button', { name: /Choose date/ }))
    expect(await screen.findByRole('radio', { name: 'April' })).toBeInTheDocument()

    user.click(await screen.findByRole('radio', { name: 'April' }))

    expect(await screen.findByRole('button', { name: /Choose date/ })).toBeInTheDocument()

    const dateInExpectedMonth = new Date(dateService.getYear(), 3, 1)

    await waitFor(() => { expect(store.getState().transactions.filters.dateBeforeEqual).toEqual(dateService.lastDateOfMonth(dateInExpectedMonth)) })
    await waitFor(() => { expect(store.getState().transactions.filters.dateAfterEqual).toEqual(dateService.firstDateOfMonth(dateInExpectedMonth)) })

  })

  it('should render the MonthYearFilter opening to year', async () => {

    const user = userEvent.setup()

    // configure store
    const store = setTestStore()
    const year = 2024
    const today = dateService.currentDate()
    const dateInExpectedYear = new Date(year, dateService.getMonth(today), 1)

    render(
      <StoreProvider store={store} >
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <MonthYearFilter openTo='year' />
        </LocalizationProvider>
      </StoreProvider >
    )

    // test for a year
    expect(await screen.findByLabelText('Jaar/Maand')).toBeInTheDocument()

    user.click(await screen.findByRole('button', { name: /Choose date/ }))
    expect(await screen.findByRole('radio', { name: String(year) })).toBeInTheDocument()

    user.click(await screen.findByRole('radio', { name: String(year) }))
    expect(await screen.findByRole('button', { name: /Choose date/ })).toBeInTheDocument()

    await waitFor(() => { expect(store.getState().transactions.filters.dateBeforeEqual).toEqual(dateService.lastDateOfMonth(dateInExpectedYear)) })
    // await waitFor(() =>
    //   expect(store.getState().transactions.filters.dateAfterEqual).toEqual(dateService.firstDateOfMonth(dateInExpectedYear)))
  })

  it('should render the MonthYearFilter opening to year with year view', async () => {

    const user = userEvent.setup()

    // configure store
    const store = setTestStore()
    const year = 2022
    const today = dateService.currentDate()
    const dateInExpectedYear = new Date(year, dateService.getMonth(today), 1)

    render(
      <StoreProvider store={store} >
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <MonthYearFilter views={['year']} openTo='year' />
        </LocalizationProvider>
      </StoreProvider >
    )

    // test for a year
    expect(await screen.findByLabelText('Jaar/Maand')).toBeInTheDocument()

    user.click(await screen.findByRole('button', { name: /Choose date/ }))
    expect(await screen.findByRole('radio', { name: String(year) })).toBeInTheDocument()

    user.click(await screen.findByRole('radio', { name: String(year) }))
    expect(await screen.findByRole('button', { name: /Choose date/ })).toBeInTheDocument()

    await waitFor(() => { expect(store.getState().transactions.filters.dateBeforeEqual).toEqual(dateService.lastDateOfYear(dateInExpectedYear)) })
    await waitFor(() => { expect(store.getState().transactions.filters.dateAfterEqual).toEqual(dateService.firstDateOfYear(dateInExpectedYear)) })
  })

})
