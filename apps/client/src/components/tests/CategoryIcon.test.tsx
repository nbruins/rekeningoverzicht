import React from 'react'
import { render, screen } from '@testing-library/react'
import { CategoryIcon } from '../CategoryIcon'

describe('CategoryIcon tests', () => {

  it('should render the categoryIcon without category', () => {

    render(<CategoryIcon />)
    expect(screen.getByLabelText('Onbekend')).toBeDefined()
  })

  it('should render the categoryIcon with category', () => {

    render(<CategoryIcon
      category={{
        name: 'The Name',
        icon: 'power',
        backgroundColor: '#FFF',
        color: '#000',
        borderColor: '#fff',
        gemmiddeldbedragNibud: 126,
        type: 'UITGAVEN'
      }} />
    )

    expect(screen.getByLabelText('The Name')).toBeDefined()
  })
})
