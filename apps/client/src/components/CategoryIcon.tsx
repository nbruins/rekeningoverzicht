import { HelpOutline } from '@mui/icons-material'
import { Box } from '@mui/material'
import React from 'react'
import { standardIcons } from '../utils'
import { PickCategoryExcludeKeyofCategoryIdOrPost } from '../openapi-fetch'

const icon = {
  'display': 'flex',
  'alignItems': 'center',
  'justifyContent': 'center',
  'height': '40px',
  'minHeight': '40px',
  'width': '40px',
  'minWidth': '40px',
  'borderRadius': '40px',
  '& button': {
    'background': 'none',
    'color': 'inherit',
    'border': 'none',
    'padding': '0',
    'font': 'inherit',
    'cursor': 'pointer',
    'outline': 'inherit',
    '& svg': {
      marginTop: '8px',
    }
  }
}


interface Props<T extends PickCategoryExcludeKeyofCategoryIdOrPost> {
  category?: T | null
}

export const CategoryIcon = <T extends PickCategoryExcludeKeyofCategoryIdOrPost>({ category }: Props<T>): React.JSX.Element => {

  const backgroundColor = category?.backgroundColor ?? '#FFF'
  const color = category?.color ?? '#000'
  const iconName = category?.icon ?? 'helpOutline'

  const Icon = standardIcons[iconName] || HelpOutline
  return (
    <Box sx={{ ...icon, backgroundColor, color }}>
      <Icon aria-label={category?.name ?? 'Onbekend'} />
    </Box>)
}
