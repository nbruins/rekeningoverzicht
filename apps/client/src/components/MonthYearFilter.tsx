import Grid2 from '@mui/material/Grid2'
import { DatePicker } from '@mui/x-date-pickers'
import React, { useEffect, useState } from 'react'
import { useStoreActions, useStoreState } from '../state/typedHooks'
import { dateService } from '../utils'

interface Props {
  views?: ('year' | 'month')[]
  openTo?: 'year' | 'month'
}
export const MonthYearFilter = ({ views = ['year', 'month'], openTo = 'month' }: Props): React.JSX.Element => {

  const filters = useStoreState((state) => state.transactions.filters)
  const setFilters = useStoreActions((actions) => actions.transactions.setFilters)

  const [selectedDate, setSelectedDate] = useState<Date>(filters.dateBeforeEqual)

  useEffect(() => {
    setSelectedDate(filters.dateBeforeEqual)
  }, [filters])

  const handleDateChange = (date: unknown) => {

    const newDate = (date as Date) ?? dateService.currentDate()
    setFilters({
      ...filters,
      dateAfterEqual: views.includes('month') ? dateService.firstDateOfMonth(newDate) : dateService.firstDateOfYear(newDate),
      dateBeforeEqual: views.includes('month') ? dateService.lastDateOfMonth(newDate) : dateService.lastDateOfYear(newDate)
    })
    setSelectedDate(newDate)
  }

  return (
    <Grid2 container justifyContent="space-around">
      <DatePicker
        label="Jaar/Maand"
        value={selectedDate}
        onChange={handleDateChange}
        openTo={openTo}
        views={views}
      />
    </Grid2>
  )
}
