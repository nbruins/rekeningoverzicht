import { UnauthenticatedTemplate, useMsal } from '@azure/msal-react'
import React, { FC } from 'react'

import { Button } from '@mui/material'
import { loginRequest } from '../authconfig'

export const SignInButton: FC = () => {

  const { instance } = useMsal()

  const handleLogin = () => {
    instance.loginRedirect(loginRequest)
  }

  return (
    <UnauthenticatedTemplate>
      <Button variant="contained" color="secondary" onClick={() => { handleLogin(); }}>Login</Button>
    </UnauthenticatedTemplate>
  )
}