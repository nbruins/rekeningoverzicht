import { CircularProgress, TextField } from '@mui/material'
import Autocomplete from '@mui/material/Autocomplete'
import React, { ChangeEvent, FC, useEffect, useState } from 'react'
import { Category } from '../openapi-fetch'
import { useStoreState } from '../state/typedHooks'

export interface Option {
  category: string,
  post: string
  postId: number
}

const processPosts = (categories: Category[]): Option[] => {
  const options: Option[] = []
  categories.forEach(category =>
    category.post?.forEach(post => {
      options.push({
        category: category.name,
        post: post.name,
        postId: post.id
      })
    })
  )
  return options
}

interface AutoCompletePostProps {
  handleSelectPost: (event: ChangeEvent<unknown>, value: Option | readonly Option[] | null) => void
  defaultValue?: Option[]
  multiple?: boolean
}

export const AutoCompletePost: FC<AutoCompletePostProps> = ({ handleSelectPost, defaultValue, multiple = false }) => {

  const categoriesWithPosts = useStoreState((state) => state.categories.categoriesWithPosts)
  const loading = useStoreState((state) => state.categories.loading)

  const [options, setOptions] = useState<Option[]>([])

  useEffect(() => {
    if (categoriesWithPosts)
      setOptions(processPosts(categoriesWithPosts))
  }, [categoriesWithPosts])

  if (loading)
    return (<CircularProgress />)
  if (options.length <= 0)
    return <p>no cat</p>

  return (
    <Autocomplete
      id="categories_posts"
      limitTags={2}
      multiple={multiple}
      options={options}
      defaultValue={defaultValue}
      groupBy={option => option.category}
      getOptionLabel={option => option.post}
      renderInput={params => <TextField {...params} label="With categories" variant="outlined" />}
      onChange={handleSelectPost}
      sx={{ minWidth: '260px', maxWidth: '100%' }}
    />
  )
}