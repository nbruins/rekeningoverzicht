import { AuthenticatedTemplate, UnauthenticatedTemplate } from '@azure/msal-react'
import Brightness4Icon from '@mui/icons-material/Brightness4'
import Brightness7Icon from '@mui/icons-material/Brightness7'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import MenuIcon from '@mui/icons-material/Menu'
import TranslateIcon from '@mui/icons-material/Translate'
import { Box, Container, CssBaseline, Divider, IconButton, MenuItem, Popover, Toolbar, Typography } from '@mui/material'
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar'
import MuiDrawer from '@mui/material/Drawer'
import { styled, useTheme } from '@mui/material/styles'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFnsV3'
import * as React from 'react'
import { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { ColorModeContext } from '../../App'
import { useWindowDimensions } from '../../hooks'
import { SignInButton } from '../SignInButton'
import { SignOutButton } from '../SignOutButton'
import { MenuList } from './MenuList'
import { menuItems } from './menuItems'

const drawerWidth = 240

interface AppBarProps extends MuiAppBarProps {
  open?: boolean
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${String(drawerWidth)}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}))

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      }),
    },
  }),
)

export const MainPage: FC<{ title: string, children: React.ReactNode }> = ({ title, children }) => {

  const { i18n, t } = useTranslation()

  const { width } = useWindowDimensions()

  const navigate = useNavigate()

  const [open, setOpen] = useState(false)
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null)

  const muiTheme = useTheme()
  const colorMode = React.useContext(ColorModeContext)

  const toggleDrawer = () => {
    setOpen(!open)
  }
  const handleMenuClick = (goTo: string) => {
    toggleDrawer()
    navigate(goTo)
  }

  const openLanguage = (event: React.MouseEvent<HTMLButtonElement>) => { setAnchorEl(event.currentTarget) }
  const changeLanguage = (lng: string) => {
    void i18n.changeLanguage(lng)
    setAnchorEl(null)
  }

  // class NlLocalizedUtils extends DateFnsUtils {
  //   getDatePickerHeaderText(date: Date) {
  //     return format(date, 'd MMM yyyy', { locale: this.locale })
  //   }
  // }

  // const locale = i18n?.language === 'en' ? enLocale : i18n?.language === 'de'? deLocale : nlLocale
  // const localeUtils = i18n?.language === 'en' ? DateFnsUtils : i18n?.language === 'de'? NlLocalizedUtils : NlLocalizedUtils

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Box sx={{ display: 'flex' }} >
        <CssBaseline />
        <AppBar position="absolute" open={open}>
          <Toolbar
            sx={{
              pr: '24px', // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: '36px',
                ...(open && { display: 'none' }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              {title}
            </Typography>
            <UnauthenticatedTemplate>
              <SignInButton />
            </UnauthenticatedTemplate>
            <AuthenticatedTemplate>
              <SignOutButton />
            </AuthenticatedTemplate>
            <IconButton onClick={colorMode.toggleColorMode} size="large">
              {muiTheme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
            </IconButton>
            <IconButton
              aria-label={'language'}
              aria-describedby={anchorEl ? 'choose-language' : undefined}
              color="inherit"
              onClick={openLanguage}
              size="large">
              <TranslateIcon />
            </IconButton>
            <Popover
              id={anchorEl ? 'choose-language' : undefined}
              open={Boolean(anchorEl)}
              anchorEl={anchorEl}
              onClose={() => { setAnchorEl(null) }}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              {['nl', 'de', 'en'].map(lang => (
                <MenuItem key={lang} onClick={() => { changeLanguage(lang) }}>{t(lang)}</MenuItem>
              ))}
            </Popover>
          </Toolbar>
        </AppBar>
        <Drawer open={open} variant={(width < 720 && !open) ? 'temporary' : 'permanent'}>
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <MenuList menuItems={menuItems} onClick={handleMenuClick} />
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto'
          }}
        >
          <Toolbar />
          <Container maxWidth={false} sx={{ mt: 4, mb: 4, padding: 0 }} >
            {children}
          </Container>
        </Box>
      </Box>
    </LocalizationProvider>
  )
}
