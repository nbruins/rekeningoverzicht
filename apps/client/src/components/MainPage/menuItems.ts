import {MenuListItem} from './MenuList'
import {AccountBalance, Euro, Info, Publish, Settings, ShowChart} from '@mui/icons-material'

export const menuItems: MenuListItem[] = [
  {name: 'transactions', icon: AccountBalance},
  {name: 'monthlyrepost', icon: ShowChart},
  {name: 'budget', icon: Euro },
  {name: 'import', icon: Publish},
  {name: 'configuration', icon: Settings},
  {name: 'about', icon: Info}
]
