import { SvgIconComponent } from '@mui/icons-material'
import { useTranslation } from 'react-i18next'
import { List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import React from 'react'

export interface MenuListItem {
  name: string
  icon: SvgIconComponent
}
interface MenuListProps {
  menuItems: MenuListItem[]
  onClick: (goTo: string) => void
}

export const MenuList = ({ menuItems, onClick }: MenuListProps): React.JSX.Element => {
  const { t } = useTranslation()
  return (
    <List>
      <div>
        {menuItems.map(item => {
          const Icon = item.icon
          return <ListItemButton key={item.name} onClick={() => { onClick(`/${item.name}`) }}>
            <ListItemIcon>
              <Icon />
            </ListItemIcon>
            <ListItemText primary={t(item.name)} />
          </ListItemButton>
        })}
      </div>
    </List>
  )
}
