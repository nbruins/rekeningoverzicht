import { createTheme, ThemeProvider } from '@mui/material/styles'
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { StoreProvider } from 'easy-peasy'
import { setTestStore } from '../../../utils'
import { MainPage } from '../MainPage'

describe('MainPage tests', () => {

  jest.mock('react-i18next', () => ({ useTranslation: () => ({ t: (key: string) => key }) }))
  const store = setTestStore()
  const theme = createTheme()

  it('should render the MainPage', async () => {

    render(
      <StoreProvider store={store} >
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <MainPage title={'test title'}>
              <p>page contents</p>
            </MainPage>
          </BrowserRouter>
        </ThemeProvider>
      </StoreProvider >
    )
    expect(screen.getByText('test title')).toBeDefined()
    expect(screen.getByText('page contents')).toBeDefined()

    userEvent.click(screen.getByLabelText('language'))
    await waitFor(() => { expect(screen.getByText('nl')).toBeDefined(); })

    userEvent.click(screen.getByLabelText('open drawer'))

  })
})
