import { useMsal } from '@azure/msal-react'
import AccountCircle from '@mui/icons-material/AccountCircle'
import IconButton from '@mui/material/IconButton'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import React, { useState } from 'react'

export const SignOutButton = () => {
  const { instance } = useMsal()

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)

  const handleLogout = () => {
    setAnchorEl(null)
    instance.logoutRedirect()
  }

  return (
    <div>
      <IconButton
        onClick={(event) => { setAnchorEl(event.currentTarget); }}
        color="inherit"
      >
        <AccountCircle />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={() => { setAnchorEl(null); }}
      >
        <MenuItem onClick={() => { handleLogout(); }} key="logout">Logout</MenuItem>
      </Menu>
    </div>
  )
}