import { SxProps } from '@mui/material'

export const bedragPlus: SxProps = {
  color: 'green !important'
}

export const bedragMin: SxProps = {
  color: 'red !important',
}