let store: Record<string, string> = {}

export const localStorageMock = {
  clear: (): void => { store = {} },
  getItem: (key: string): string | null => store[key] || null,
  setItem: (key: string, value: string): void => { store[key] = String(value) },
  // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
  removeItem: (key: string): void => { delete store[key] }
}
