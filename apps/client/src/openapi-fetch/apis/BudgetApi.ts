/* tslint:disable */
/* eslint-disable */
/**
 * server
 * Rekeningoverzicht server V2
 *
 * The version of the OpenAPI document: 2.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  Budget,
  BudgetResponse,
  BudgetUpdateRequestBody,
  PickBudgetExcludeKeyofBudgetId,
  PostsWithOutBudgetResponse,
} from '../models';
import {
    BudgetFromJSON,
    BudgetToJSON,
    BudgetResponseFromJSON,
    BudgetResponseToJSON,
    BudgetUpdateRequestBodyFromJSON,
    BudgetUpdateRequestBodyToJSON,
    PickBudgetExcludeKeyofBudgetIdFromJSON,
    PickBudgetExcludeKeyofBudgetIdToJSON,
    PostsWithOutBudgetResponseFromJSON,
    PostsWithOutBudgetResponseToJSON,
} from '../models';

export interface CreateBudgetRequest {
    body: PickBudgetExcludeKeyofBudgetId;
}

export interface GetAllPostsWithoutBudgetForRequest {
    year: number;
}

export interface GetBudgetsRequest {
    year: number;
}

export interface UpdateBudgetRequest {
    budgetUpdateRequestBody: BudgetUpdateRequestBody;
}

/**
 * 
 */
export class BudgetApi extends runtime.BaseAPI {

    /**
     */
    async createBudgetRaw(requestParameters: CreateBudgetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Budget>> {
        if (requestParameters.body === null || requestParameters.body === undefined) {
            throw new runtime.RequiredError('body','Required parameter requestParameters.body was null or undefined when calling createBudget.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/budgets`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: requestParameters.body as any,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => BudgetFromJSON(jsonValue));
    }

    /**
     */
    async createBudget(requestParameters: CreateBudgetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Budget> {
        const response = await this.createBudgetRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async getAllPostsWithoutBudgetForRaw(requestParameters: GetAllPostsWithoutBudgetForRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<PostsWithOutBudgetResponse>>> {
        if (requestParameters.year === null || requestParameters.year === undefined) {
            throw new runtime.RequiredError('year','Required parameter requestParameters.year was null or undefined when calling getAllPostsWithoutBudgetFor.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/budgets/posts/{year}`.replace(`{${"year"}}`, encodeURIComponent(String(requestParameters.year))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(PostsWithOutBudgetResponseFromJSON));
    }

    /**
     */
    async getAllPostsWithoutBudgetFor(requestParameters: GetAllPostsWithoutBudgetForRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<PostsWithOutBudgetResponse>> {
        const response = await this.getAllPostsWithoutBudgetForRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async getBudgetsRaw(requestParameters: GetBudgetsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<BudgetResponse>>> {
        if (requestParameters.year === null || requestParameters.year === undefined) {
            throw new runtime.RequiredError('year','Required parameter requestParameters.year was null or undefined when calling getBudgets.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/budgets/{year}`.replace(`{${"year"}}`, encodeURIComponent(String(requestParameters.year))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(BudgetResponseFromJSON));
    }

    /**
     */
    async getBudgets(requestParameters: GetBudgetsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<BudgetResponse>> {
        const response = await this.getBudgetsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async updateBudgetRaw(requestParameters: UpdateBudgetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.budgetUpdateRequestBody === null || requestParameters.budgetUpdateRequestBody === undefined) {
            throw new runtime.RequiredError('budgetUpdateRequestBody','Required parameter requestParameters.budgetUpdateRequestBody was null or undefined when calling updateBudget.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/budgets`,
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: BudgetUpdateRequestBodyToJSON(requestParameters.budgetUpdateRequestBody),
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     */
    async updateBudget(requestParameters: UpdateBudgetRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.updateBudgetRaw(requestParameters, initOverrides);
    }

}
