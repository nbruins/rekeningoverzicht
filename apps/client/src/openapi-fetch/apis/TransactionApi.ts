/* tslint:disable */
/* eslint-disable */
/**
 * server
 * Rekeningoverzicht server V2
 *
 * The version of the OpenAPI document: 2.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  PickTransactionExcludeKeyofTransactionId,
  Transaction,
  TransactionUpdatePostAndNote,
  TransactionWithPostAndCategory,
} from '../models';
import {
    PickTransactionExcludeKeyofTransactionIdFromJSON,
    PickTransactionExcludeKeyofTransactionIdToJSON,
    TransactionFromJSON,
    TransactionToJSON,
    TransactionUpdatePostAndNoteFromJSON,
    TransactionUpdatePostAndNoteToJSON,
    TransactionWithPostAndCategoryFromJSON,
    TransactionWithPostAndCategoryToJSON,
} from '../models';

export interface CreateManyTransactionsRequest {
    pickTransactionExcludeKeyofTransactionId: Array<PickTransactionExcludeKeyofTransactionId>;
}

export interface GetTransactionsRequest {
    interestDateGte?: Date;
    interestDateLte?: Date;
    postIds?: Array<number>;
    accountNumbers?: Array<string>;
}

export interface UpdateNoteAndPostForMultipleTransactionsRequest {
    transactionUpdatePostAndNote: TransactionUpdatePostAndNote;
}

/**
 * 
 */
export class TransactionApi extends runtime.BaseAPI {

    /**
     */
    async createManyTransactionsRaw(requestParameters: CreateManyTransactionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<Transaction>>> {
        if (requestParameters.pickTransactionExcludeKeyofTransactionId === null || requestParameters.pickTransactionExcludeKeyofTransactionId === undefined) {
            throw new runtime.RequiredError('pickTransactionExcludeKeyofTransactionId','Required parameter requestParameters.pickTransactionExcludeKeyofTransactionId was null or undefined when calling createManyTransactions.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/transactions`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: requestParameters.pickTransactionExcludeKeyofTransactionId.map(PickTransactionExcludeKeyofTransactionIdToJSON),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(TransactionFromJSON));
    }

    /**
     */
    async createManyTransactions(requestParameters: CreateManyTransactionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<Transaction>> {
        const response = await this.createManyTransactionsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async getTransactionsRaw(requestParameters: GetTransactionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<TransactionWithPostAndCategory>>> {
        const queryParameters: any = {};

        if (requestParameters.interestDateGte !== undefined) {
            queryParameters['interestDateGte'] = (requestParameters.interestDateGte as any).toISOString();
        }

        if (requestParameters.interestDateLte !== undefined) {
            queryParameters['interestDateLte'] = (requestParameters.interestDateLte as any).toISOString();
        }

        if (requestParameters.postIds) {
            queryParameters['postIds'] = requestParameters.postIds;
        }

        if (requestParameters.accountNumbers) {
            queryParameters['accountNumbers'] = requestParameters.accountNumbers;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/transactions`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(TransactionWithPostAndCategoryFromJSON));
    }

    /**
     */
    async getTransactions(requestParameters: GetTransactionsRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<TransactionWithPostAndCategory>> {
        const response = await this.getTransactionsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async updateNoteAndPostForMultipleTransactionsRaw(requestParameters: UpdateNoteAndPostForMultipleTransactionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<TransactionWithPostAndCategory>>> {
        if (requestParameters.transactionUpdatePostAndNote === null || requestParameters.transactionUpdatePostAndNote === undefined) {
            throw new runtime.RequiredError('transactionUpdatePostAndNote','Required parameter requestParameters.transactionUpdatePostAndNote was null or undefined when calling updateNoteAndPostForMultipleTransactions.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/api/transactions`,
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: TransactionUpdatePostAndNoteToJSON(requestParameters.transactionUpdatePostAndNote),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(TransactionWithPostAndCategoryFromJSON));
    }

    /**
     */
    async updateNoteAndPostForMultipleTransactions(requestParameters: UpdateNoteAndPostForMultipleTransactionsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<TransactionWithPostAndCategory>> {
        const response = await this.updateNoteAndPostForMultipleTransactionsRaw(requestParameters, initOverrides);
        return await response.value();
    }

}
