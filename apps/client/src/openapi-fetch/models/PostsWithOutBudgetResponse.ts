/* tslint:disable */
/* eslint-disable */
/**
 * server
 * Rekeningoverzicht server V2
 *
 * The version of the OpenAPI document: 2.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface PostsWithOutBudgetResponse
 */
export interface PostsWithOutBudgetResponse {
    /**
     * 
     * @type {string}
     * @memberof PostsWithOutBudgetResponse
     */
    category: string;
    /**
     * 
     * @type {number}
     * @memberof PostsWithOutBudgetResponse
     */
    postId: number;
    /**
     * 
     * @type {string}
     * @memberof PostsWithOutBudgetResponse
     */
    post: string;
}

/**
 * Check if a given object implements the PostsWithOutBudgetResponse interface.
 */
export function instanceOfPostsWithOutBudgetResponse(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "category" in value;
    isInstance = isInstance && "postId" in value;
    isInstance = isInstance && "post" in value;

    return isInstance;
}

export function PostsWithOutBudgetResponseFromJSON(json: any): PostsWithOutBudgetResponse {
    return PostsWithOutBudgetResponseFromJSONTyped(json, false);
}

export function PostsWithOutBudgetResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PostsWithOutBudgetResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'category': json['category'],
        'postId': json['postId'],
        'post': json['post'],
    };
}

export function PostsWithOutBudgetResponseToJSON(value?: PostsWithOutBudgetResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'category': value.category,
        'postId': value.postId,
        'post': value.post,
    };
}

