/* tslint:disable */
/* eslint-disable */
/**
 * server
 * Rekeningoverzicht server V2
 *
 * The version of the OpenAPI document: 2.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * From T, pick a set of properties whose keys are in the union K
 * @export
 * @interface PickCategoryExcludeKeyofCategoryPost
 */
export interface PickCategoryExcludeKeyofCategoryPost {
    /**
     * 
     * @type {number}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    id: number;
    /**
     * 
     * @type {string}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    icon: string | null;
    /**
     * 
     * @type {string}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    type: string;
    /**
     * 
     * @type {number}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    gemmiddeldbedragNibud: number | null;
    /**
     * 
     * @type {string}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    backgroundColor: string | null;
    /**
     * 
     * @type {string}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    borderColor: string | null;
    /**
     * 
     * @type {string}
     * @memberof PickCategoryExcludeKeyofCategoryPost
     */
    color: string | null;
}

/**
 * Check if a given object implements the PickCategoryExcludeKeyofCategoryPost interface.
 */
export function instanceOfPickCategoryExcludeKeyofCategoryPost(value: object): boolean {
    let isInstance = true;
    isInstance = isInstance && "id" in value;
    isInstance = isInstance && "name" in value;
    isInstance = isInstance && "icon" in value;
    isInstance = isInstance && "type" in value;
    isInstance = isInstance && "gemmiddeldbedragNibud" in value;
    isInstance = isInstance && "backgroundColor" in value;
    isInstance = isInstance && "borderColor" in value;
    isInstance = isInstance && "color" in value;

    return isInstance;
}

export function PickCategoryExcludeKeyofCategoryPostFromJSON(json: any): PickCategoryExcludeKeyofCategoryPost {
    return PickCategoryExcludeKeyofCategoryPostFromJSONTyped(json, false);
}

export function PickCategoryExcludeKeyofCategoryPostFromJSONTyped(json: any, ignoreDiscriminator: boolean): PickCategoryExcludeKeyofCategoryPost {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': json['id'],
        'name': json['name'],
        'icon': json['icon'],
        'type': json['type'],
        'gemmiddeldbedragNibud': json['gemmiddeldbedragNibud'],
        'backgroundColor': json['backgroundColor'],
        'borderColor': json['borderColor'],
        'color': json['color'],
    };
}

export function PickCategoryExcludeKeyofCategoryPostToJSON(value?: PickCategoryExcludeKeyofCategoryPost | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'name': value.name,
        'icon': value.icon,
        'type': value.type,
        'gemmiddeldbedragNibud': value.gemmiddeldbedragNibud,
        'backgroundColor': value.backgroundColor,
        'borderColor': value.borderColor,
        'color': value.color,
    };
}

