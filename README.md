# Rekeningoverzicht

Rekeningoverzicht is an application to import transactions from the dutch Rabobank.
Rekeningoverzicht can be used to categorize transactions and there are some reports available.

## Using Docker to runn the app

copy or rename apps/server/db/transactions.example.db to apps/server/db/transactions.db

```bash
cp apps/server/db/transactions.example.db to apps/server/db/transactions.db
```

Start docker using docker-compose up

```bash
docker-compose up -d
```

## Installation

In the root of the project

```bash
yarn install
```

or 

```bash
npm install
```

## Usage

First start the server in dev mode

```bash
cd .../apps/server
yarn dev
```

Then start client

```bash
cd .../apps/client
yarn start
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://gitlab.com/nbruins/rekeningoverzicht/-/blob/master/LICENSE)
