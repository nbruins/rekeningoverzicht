#!/bin/sh

# Recreate config file
rm -rf ./client/config.js
touch ./client/config.js

# Add assignment 
echo "window.env = {" >> ./client/config.js

# REACT_APP enviroment variabels are written to the config.js file
# Each line represents key=value pairs
printenv | grep REACT_SETTING | \
while read line
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    value=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi
  
  # Append configuration property to JS file
  echo "  \"$varname\": \"$value\"," >> ./client/config.js
done

echo "}" >> ./client/config.js
