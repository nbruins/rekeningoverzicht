################################################
# Build server
################################################
FROM node:lts-jod AS build-stage

WORKDIR /build/app/server

COPY ./apps/server ./

RUN yarn install
RUN yarn build

################################################
# Install
FROM node:lts-jod AS install-stage

WORKDIR /install/app/server

COPY --from=build-stage /build/app/server/buildStage ./

COPY ./apps/server/prisma ./prisma
COPY ./apps/server/prisma/.env.example ./prisma/.env

COPY ./apps/server/package.json ./package.json

RUN yarn install --production
RUN yarn generate:prisma

################################################
# Server container
FROM node:jod-alpine3.21 AS server

WORKDIR /app

# First make sure the client is already build.
COPY ./create_client_config.sh .
COPY ./apps/client/dist ./client
COPY --from=install-stage /install/app/server ./server
COPY ./apps/server/db/transactions.example.db ./server/db/transactions.db

WORKDIR /app/server

EXPOSE 3002
ENV NODE_ENV=production
ENV DATABASE_URL=file:/app/server/db/transactions.db?connection_limit=1&connect_timeout=30

CMD ["/bin/sh", "-c", "cd .. && ./create_client_config.sh && cd server && node server.js"]
